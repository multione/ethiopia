
#%%
import matplotlib.pyplot as plt
import pickle
import pandas
from pathlib import Path
from datetime import datetime, timedelta
import numpy as np
from scipy.stats import linregress
import geopandas


fld = Path(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GEE\modis_median')

#%%
def read_modis():
#%%    
    df_ndvi=pandas.DataFrame()
    df_evi=pandas.DataFrame()
    dff_ndvi=[]; dff_evi=[]
    files = list(fld.glob('*.pickle'))
    for f in files: #f = files[1]
        print(f)
        data = pickle.load(f.open('rb'))

        date, objectid, ndvi, evi = data['date'], data['objectid'], data['ndvi'], data['evi']
        date_str = f'{date:%Y%m%d}'
        dff_ndvi.append(pandas.DataFrame({'objectid':objectid, date_str:ndvi}).set_index('objectid'))       
        dff_evi.append(pandas.DataFrame({'objectid':objectid, date_str:evi}).set_index('objectid'))

    df_ndvi = pandas.concat(dff_ndvi, axis=1)
    df_evi = pandas.concat(dff_evi, axis=1)

    df_ndvi.to_pickle(fld.parent/'modis_median_ndvi.df')
    df_evi.to_pickle(fld.parent/'modis_median_evi.df')


#%%
def drawings():
#%%
    # df = df_evi
    shp = geopandas.read_file(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GIS\SLMP_MWS_shapefiles_2017-05-01.shp')
    shp = shp.set_index('OBJECTID')

    df = pandas.read_pickle(fld.parent/'modis_median_evi.df')
    df.sort_index(inplace=True)

    fd = datetime(2000,1,1)#datetime.strptime(df.columns[0],'%Y%m%d')
    x = np.array([(datetime.strptime(c,'%Y%m%d')-fd).days for c in df.columns])
    dfs = pandas.DataFrame(index=df.index)
    for i in df.index:
        #i = 3
        y = df.loc[i,:].values
        model= linregress(x,y)#np.polyfit(x,y,1)
        '''
        plt.figure(figsize=(10,4))
        plt.plot(x,y,'.')
        xx = np.arange(x.max())
        plt.plot(xx, model[0]*xx+model[1])
        '''

        dfs.loc[i,'slope'] = model.slope
        dfs.loc[i,'intercept'] = model.intercept
        shp.loc[i,'evi_slope'] = model.slope

    shp.to_file(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GIS\MWS_evi_slope.shp')

#%% nans
    ind = dfs.slope.isna()
    dfs_nans = dfs.loc[ind,:]
    dfs_ok = dfs.loc[~ind,:].sort_values('slope')

    sel_ids=dfs_nans.index[:5].tolist() + dfs_ok.index[:5].tolist() + dfs_ok.index[-5:].tolist()

    xx = np.arange(x.max())
    for objid in sel_ids:
        # objid=sel_ids[0]
        slope, intercept = dfs.loc[objid,:]
        y = df.loc[objid,:].values
        plt.figure(figsize=(24,6))
        plt.plot(x,y)
        if not np.isnan(slope):
            plt.plot(xx, slope*xx+intercept)
        plt.xlabel('days from 2000-01-01')
        plt.ylabel('evi')
        plt.title(f'objectid: {objid}, slope: {slope:.3e}')
        outfile = fld.parent/f'imgs/evi_{objid}_{slope*1e5:.2f}.png'
        print(outfile)
        plt.savefig(outfile, dpi=200)


def modis_to_monthly_mean():
#%%
    import rasterio
    from rasterio.warp import calculate_default_transform, reproject, Resampling
    from datetime import datetime, timedelta

    dst_crs = 'EPSG:4326'
    src_crs = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'
    profile = dict(driver='COG', nodata=-9999.0, dtype=rasterio.float32, crs='EPSG:4326', count=1)

    input_folder = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs')
    output_folder = Path(r'D:\DATA\ETHIOPIA\MODIS\ndvi_monthly_mean')
    output_folder.mkdir(parents=True, exist_ok=True)

    files = list(input_folder.glob('modis_*.tif'))
    with rasterio.open(files[0]) as src: 
        src_profile = src.profile
        w = src_profile['width']
        h = src_profile['height']
        t = src_profile['transform']
        transform, width, height = calculate_default_transform(
        src_crs, dst_crs, src.width, src.height, *src.bounds)

    profile['transform'] = transform
    profile['width'] = width
    profile['height'] = height

    dates = [datetime.strptime(f.with_suffix('').name.split('_')[1], '%Y%m%d') for f in files]
    datestr = np.array([(d+timedelta(days=8)).strftime('%Y%m') for d in dates])

    all_months = np.unique(datestr)
    files = np.array(files)
    for m in all_months:
        # m=all_months[0]
        ind = datestr==m
        fs = files[ind]

        ndvi=np.full((h,w,ind.sum()), 0, dtype=np.float32)

        for i,f in enumerate(fs):
            with rasterio.open(f) as src:
                ndvi[:,:,i] = src.read(1)

        ndvi[ndvi<=0]=np.nan
        ndvi = np.nanmean(ndvi,axis=2)/10000.0
        ndvi[np.isnan(ndvi)]=-9999.0

        dst_ndvi = np.zeros((height, width),dtype=np.float32)-9999.0
        dst_ndvi, dst_transform = reproject(
                source=ndvi,
                destination=dst_ndvi,
                src_transform=t,
                src_crs=src_crs,
                dst_transform=transform,
                dst_crs=dst_crs,
                resampling=Resampling.nearest,
                dst_nodata=-9999.0)
        with rasterio.open(output_folder/f'ndvi_250m_{m}.tif', 'w', **profile) as dst:
            dst.write(dst_ndvi, 1)
 



# %%
if __name__=='__main__':
    modis_to_monthly_mean()