#%%
import pandas
from pathlib import Path

fld_out = Path(r'D:\DATA\ETHIOPIA\TEDDY')
#%%
def export_modis_v1():
    fn = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs.pickle')
    

    df = pandas.read_pickle(fn)
    df['date'] = pandas.to_datetime(df.time_id)
    df['year'] = df.date.dt.year
    df['month'] = df.date.dt.month

    dfmy = df.groupby(['object_id','year','month'])['ndvi'].mean()/10000.0
    dfmy.reset_index().to_csv(fld_out/'modis_ndvi_year_month.csv',index=False)

    dfy =  df.groupby(['object_id','year'])['ndvi'].mean()/10000.0
    dfmy.reset_index().to_csv(fld_out/'modis_ndvi_year.csv',index=False)

    dfyy = dfy.groupby(['object_id']).agg(['mean','std'])
    dfyy.reset_index().to_csv(fld_out/'modis_ndvi_mean_std.csv',index=False)

