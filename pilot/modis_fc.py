# H21V07, H21V08, H22V08, H22V07
#%%

from pathlib import Path


#%%
def download():
#%%
    import requests
    from bs4 import BeautifulSoup

    tiles=['h21v07', 'h21v08', 'h22v08', 'h22v07']
    fldout = Path(r'D:\DATA\ETHIOPIA\MODIS\FC')
    fldout = Path('/data/ethiopia/modis_fc')
    #%%
    # download FC
    linkbase = 'https://dapds00.nci.org.au/thredds/catalog/tc43/modis-fc/v310/tiles/monthly/cover/'
    linkbase = 'https://dapds00.nci.org.au/thredds/fileServer/tc43/modis-fc/v310/tiles/monthly/cover/'
    page = requests.get("https://dapds00.nci.org.au/thredds/catalog/tc43/modis-fc/v310/tiles/monthly/cover/catalog.html")
    soup = BeautifulSoup(page.content, 'html.parser')
    links = soup.find_all('a')

    for tile in tiles:
        print(f'{tile}: ', end='')
        # tile='h21v07'
        linkrel = [l['href'] for l in links if tile in l.text]
        print(len(linkrel))
        for l in linkrel:
                # l=linkrel[0]
            filename = Path(l).name
            print(f'{filename}: ', end='')
            r = requests.get(linkbase+filename)
            if r.status_code != 200:
                print(f'ERROR {r.status_code}: {r.reason}')
            else:
                print(f'OK')
            (fldout/filename).open('wb').write(r.content)
# %%
def playground():
#%% 
    from netCDF4 import Dataset
    import rasterio
    from rasterio.features import rasterize
    import geopandas

    fld = Path(r'D:\DATA\ETHIOPIA\MODIS\FC')
    input_shape = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\mws_slopes.shp')
    input_property = 'OBJECTID'

    files = list(fld.glob('*'))

    f= files[0]
    nc = Dataset(f)

    width = nc.dimensions['x'].size
    height = nc.dimensions['y'].size
    #nc.variables['sinusoidal'].ncattrs()
    ncsin = nc.variables['sinusoidal']
    geo_transform = list(map(float, ncsin.getncattr('GeoTransform').strip().split(' ')))
    spatial_ref = ncsin.getncattr('spatial_ref')

    crs = rasterio.crs.CRS.from_string(spatial_ref)
    gt = rasterio.transform.from_origin(geo_transform[0], geo_transform[3], geo_transform[1], -geo_transform[5])
    profile = dict(driver='GTiff', width=width, height=height, count=1, dtype=rasterio.uint8, crs=spatial_ref, transform=gt, nodata=255)
    #with rasterio.open(r'D:\DATA\ETHIOPIA\MODIS\nc_test.tif', 'w', **profile) as dst:
    #    dst.write(nc['bare_soil'][0,:,:],1)

    shp = geopandas.read_file(input_shape)
    shapes = zip(shp.geometry, shp[input_property])
    objectids = rasterize(shapes, out_shape=(height,width), fill=0, transform=gt, dtype=rasterio.int16)
    objectids_ind = objectids>0
    objectids = objectids[objectids_ind]



    # sinusoidal#GeoTransform
    # sinusoidal#spatial_ref
    # odict_keys(['time', 'x', 'y', 'phot_veg', 'nphot_veg', 'bare_soil', 'tot_cov', 'sinusoidal'])

def read_modis_fc():
#%%
    from netCDF4 import Dataset
    import rasterio
    from rasterio.features import rasterize
    import geopandas, pandas
    import numpy as np
    from pathlib import Path
    import pickle

    tiles=['h21v07', 'h21v08', 'h22v08', 'h22v07']
    vars = ['phot_veg', 'nphot_veg', 'bare_soil', 'tot_cov']

    fld = Path(r'D:\DATA\ETHIOPIA\MODIS\FC')
    input_shape = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\mws_slopes.shp')
    input_property = 'OBJECTID'
    output_file = fld.parent/'modis_fc.pickle'

    shp = geopandas.read_file(input_shape)
    shapes = list(zip(shp.geometry, shp[input_property]))
    
    georef={}
    outtiles=[]
    spatial_id=[]
    object_id=[]
    years=[]
    months=[]
    values=dict((v,[]) for v in vars)

    for tid,t in enumerate(tiles): 
        print (tid, t)
        # t='h21v07'
        files = list(fld.glob(f'*.{t}.*'))
        f = files[0]
        
        nc = Dataset(f)
        width = nc.dimensions['x'].size
        height = nc.dimensions['y'].size    
        ncsin = nc.variables['sinusoidal']
        geo_transform = list(map(float, ncsin.getncattr('GeoTransform').strip().split(' ')))
        spatial_ref = ncsin.getncattr('spatial_ref')
        nc.close()

        crs = rasterio.crs.CRS.from_string(spatial_ref)
        gt = rasterio.transform.from_origin(geo_transform[0], geo_transform[3], geo_transform[1], -geo_transform[5])
        georef[t]=dict(crs=spatial_ref, transform=gt)

        objectids = rasterize(shapes, out_shape=(height,width), fill=0, transform=gt, dtype=rasterio.int16)
        objectids_ind = objectids>0
        objectids = objectids[objectids_ind]
        ndata = objectids_ind.sum()

        spatialids = np.arange(height*width).reshape((height,width))
        spatialids = spatialids[objectids_ind]

        for f in files:
            print(f.name)
            fs = f.name.split('.')
            year = int(fs[4])
            if year>2020:
                continue

            nc = Dataset(f)
            for m in range(12): #m=0
                outtiles.append([tid]*ndata)
                object_id.append(objectids)
                spatial_id.append(spatialids)
                years.append(np.zeros(ndata, dtype=int)+year)
                months.append(np.zeros(ndata, dtype=int)+m+1)
                for v in vars:  #v='bare_soil'                    
                    data = nc[v][m,:,:][objectids_ind]
                    #data_ind = ~data.mask
                    #data = data.data[data_ind]
                    #ndata = data_ind.sum()
                    values[v].append(data)                    
            nc.close()

    object_id = np.concatenate(object_id)
    spatial_id = np.concatenate(spatial_id)
    years = np.concatenate(years)   
    months = np.concatenate(months)
    for v in values:
        values[v] = np.concatenate(values[v])
        

#%%

    data = dict(object_id = object_id, spatial_id=spatial_id, year=years, month=months)
    for v in values:
        data[v]=values[v]
    df = pandas.DataFrame(data=data)
    df.to_pickle(output_file)

    fn_georef = output_file.with_name(output_file.with_suffix('').name+'_georef.pickle')
    pickle.dump(georef, fn_georef.open('wb'))

#%%

def modis_fc_slope():
    '''
    fld = Path(r'D:\DATA\ETHIOPIA\MODIS\FC')
    input_shapefile = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\mws_slopes.shp')
    input_property = 'OBJECTID'
    input_file = fld.parent/'modis_fc.pickle'
    output_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\mws_fc_slopes.shp')
    '''
#%%
    import pandas, geopandas
    import numpy as np
    from sklearn.linear_model import LinearRegression
    #from sklearn.preprocessing import scale
#%%
    vars = ['phot_veg', 'nphot_veg', 'bare_soil', 'tot_cov']

    df = pandas.read_pickle(input_file)

    missing_ind = (df[vars]>=254).values.sum(axis=1)>0
    df = df.loc[~missing_ind,:]
    df['month_from_2000'] = (df.year-2000)*12+df.month

    '''
    dfs = df[vars[:-1]].sum(axis=1)
    print(dfs.min(), dfs.max())
    df.loc[dfs>140]
    df.loc[df.spatial_id==1273384]
    '''
#%%
    def agg_slope(dff):
        x = dff.month_from_2000.values.reshape(-1,1)
        y = dff[vars].values
        model = LinearRegression(normalize=True)
        model.fit(x,y)
        return model.coef_.reshape(-1)

    dfg = df.groupby('object_id')
    dfs = dfg.apply(agg_slope)
    dfs = pandas.DataFrame.from_dict(dict(zip(dfs.index, dfs.values))).T
    dfs.columns = [f'{p}_slp' for p in vars]

    dfs.to_pickle(output_file.with_suffix('.pickle'))

    shp = geopandas.read_file(input_shapefile)
    shp = shp.join(dfs, on='OBJECTID')
    shp.to_file(output_file)