#%%
from pathlib import Path
import pandas
import numpy as np

params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']
lc_legend = np.array([
    'No data', 
    'Trees cover areas',
    'Shrubs cover areas',
    'Grassland',
    'Cropland',
    'Vegetation aquatic or regularly flooded',
    'Lichen Mosses / Sparse vegetation',
    'Bare areas',
    'Built up areas',
    'Permanent snow and/or Ice',
    'Open water'])
#%%
def cleanup_score(x):
    if isinstance(x,float):
        return None
    if x.lower()=='yes':
        return 1
    else:
        return 0

def success_prepare_data_v1():

#%%
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.pickle')
    input_data = 'https://docs.google.com/spreadsheets/d/1k020YkZRfRDWuYwBYMSDBzJwoDl6TA3d/export?format=csv&gid=1319881855'
    input_lc =  Path(r'D:\DATA\ETHIOPIA\LC\lc_250m.npz')
    out_fld = Path('D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\stats')

    df = pandas.read_pickle(input_file)
    df['EOS'] = df['SOS'] + df['LOS']

    dfd = pandas.read_csv(input_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','SLMP_MWS_shapefiles.imp_yr','Success_score']]
    dfd.columns = ['objectid','donor','z_name','w_name','int_year','score']
    #dfd = dfd.dropna()    
    #dfd['int_year'] = dfd['int_year'].astype(int)
    dfd['score'] = dfd.score.apply(cleanup_score)


    df = df.join(dfd[['objectid','int_year']].set_index('objectid'), on='object_id')
    df['period']=None
    ind = df[['YEAR','int_year']].isna().sum(axis=1)==0
    df.loc[ind,'period'] = 'BEF'
    ind = ind & (df.YEAR>df.int_year)
    df.loc[ind,'period'] = 'AFT'

    dfg=df.groupby(['NPOINT','spatial_id','object_id', 'period'])
    df = dfg[params].mean().reset_index()

    dff=df.pivot(index=['spatial_id','object_id'], values=params, columns='period' )
    dff.columns=['_'.join(col) for col in dff.columns.values]

    df = dff.reset_index().join(dfd.set_index('objectid'), on='object_id')

    with np.load(input_lc) as data:
        #print(data)
        lc=data['lc']

    df['lc'] = lc[df.spatial_id.values]
    df['lc_name'] = lc_legend[df.lc.values]

    df.to_pickle(input_file.parent/'timesat_season_lc_sucess_v2.df')
    #df.to_csv(input_file.parent/'timesat_season_lc_sucess.csv',sep='\t')



#%%
def success_v1():
#%%
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_season_lc_sucess_v2.df')
    df = pandas.read_pickle(input_file)

    #sklearn.linear_model.LogisticRegressionCV
    # https://www.statsmodels.org/stable/examples/notebooks/generated/discrete_choice_overview.html
#%%
    import numpy as np
    import statsmodels.api as sm
    from patsy import dmatrices

    dff = df.dropna()

    ind_vars = [] #['lc_name']
    for p in params:
        ind_vars.extend([f'{p}_BEF', f'{p}_AFT'])
    
    y, X = dmatrices('score ~ ' + ' + '.join(ind_vars), data=dff, return_type='dataframe')
#%% OLS
    mod = sm.OLS(y, X)
    res = mod.fit()
    print(res.summary())
#%% Logit
    logit_mod = sm.Logit(y, X)
    logit_res = logit_mod.fit(disp=0)
    print(logit_res.summary())
    logit_mod.pred_table()
#%% Probit
    probit_mod = sm.Probit(y, X)
    probit_res = probit_mod.fit()
    print(probit_res.summary())
#%%

def success_v2():
#%%
    from sklearn.utils import class_weight
    from sklearn.metrics import classification_report, confusion_matrix
    from sklearn import preprocessing
    from sklearn.linear_model import LogisticRegressionCV
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_season_lc_sucess_v2.df')
    df = pandas.read_pickle(input_file)
    dff = df.dropna()
#%%
    ind_vars = [] #['lc_name']
    for p in params:
        ind_vars.extend([f'{p}_BEF', f'{p}_AFT'])

    y = dff['score']
    X = dff[ind_vars]
    scaler = preprocessing.StandardScaler().fit(X)
    X_scaled = scaler.transform(X)
    #weight = class_weight.compute_sample_weight('balanced', y)
#%%
    clf = LogisticRegressionCV(class_weight='balanced',max_iter=200).fit(X_scaled,y)
    print(clf.score(X_scaled,y))
    y_prd = clf.predict(X_scaled)
    print(classification_report(y, y_prd))
#%%
    from sklearn.ensemble import GradientBoostingClassifier

    clf_gbc = GradientBoostingClassifier(n_iter_no_change=5).fit(X_scaled, y)
    print(clf_gbc.score(X_scaled, y))
    y_prd = clf_gbc.predict((X_scaled))
    print(classification_report(y, y_prd))
    print(confusion_matrix(y, y_prd))
#%%
