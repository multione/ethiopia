
#%%
from pathlib import Path
import pandas
from sklearn.linear_model import LinearRegression
from sklearn import preprocessing
import numpy as np
import rasterio as rio
import scipy.stats

params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']
lc_legend = np.array([
    'No data', 
    'Trees cover areas',
    'Shrubs cover areas',
    'Grassland',
    'Cropland',
    'Vegetation aquatic or regularly flooded',
    'Lichen Mosses / Sparse vegetation',
    'Bare areas',
    'Built up areas',
    'Permanent snow and/or Ice',
    'Open water'])
#%%
def read_lc():
#%%
    input_file = Path(r'D:\DATA\ETHIOPIA\LC\lc_250m.tif')
    output_file = Path(r'D:\DATA\ETHIOPIA\LC\lc_250m.npz')

    with rio.open(input_file) as src:
        profile = src.profile
        width = profile['width']
        height = profile['height']
        lc = src.read(1)

    # check
    with rio.open(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs\modis_20000218.tif') as src:
        profile_modis = src.profile
        width_modis = profile['width']
        height_modis = profile['height'] 

    assert((width==width_modis)&(height==height_modis))

    # sids = np.arange(height*width).reshape((height,width))
    np.savez_compressed(output_file, lc=lc.reshape(-1))



#%%
def timesat_stats():
#%%
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.pickle')
    input_data = 'https://docs.google.com/spreadsheets/d/1k020YkZRfRDWuYwBYMSDBzJwoDl6TA3d/export?format=csv&gid=1319881855'
    input_lc =  Path(r'D:\DATA\ETHIOPIA\LC\lc_250m.npz')
    out_fld = Path('D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\stats')

    df = pandas.read_pickle(input_file)
    df['EOS'] = df['SOS'] + df['LOS']

    dfd = pandas.read_csv(input_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','SLMP_MWS_shapefiles.imp_yr']]
    dfd.columns = ['objectid','donor','z_name','w_name','int_year']
    dfd = dfd.dropna()    
    dfd['int_year'] = dfd['int_year'].astype(int)
    mwsdict = dfd.set_index('objectid').to_dict('index')

    df = df.join(dfd.set_index('objectid'), on='object_id')
    df = df.loc[~df.int_year.isnull()]

    with np.load(input_lc) as data:
        #print(data)
        lc=data['lc']

    df['lc'] = lc[df.spatial_id.values]
    df['lc_name'] = lc_legend[df.lc.values]
    #points = df.NPOINT.unique()
    #npoints = len(points)

    nparams = len(params)
    # df.to_csv(Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.csv'),index=False)
#%%
    #slope1 = np.full((npoints, nparams), np.nan)
    #slope2 = np.full((npoints, nparams), np.nan)
    out=[]

    dfg = df.groupby(['donor','z_name','w_name', 'lc_name'])
    keys = list(dfg.groups.keys())

    model = LinearRegression()

    for i,(donor,z_name,w_name, lc_name) in enumerate(keys):
        # key=keys[0]; (donor,z_name,w_name)=key;
        # p = 'SOS'
        print(f'{i}/{len(keys)}: {donor}, {z_name}, {w_name}, {lc_name}')

        dff = dfg.get_group((donor,z_name,w_name,lc_name))
        #objectid = dff.object_id.iloc[0]
        #int_year = mwsdict[objectid]['int_year']
        
        data = preprocessing.StandardScaler().fit_transform(dff[params].values)

        ind = dff.YEAR<dff.int_year

        x = dff.YEAR.values[ind].reshape(-1,1)
        y = data[ind,:]
        model.fit(x,y)                
        slope1 = model.coef_*100

        ind = ~ind
        x = dff.YEAR.values[ind].reshape(-1,1)
        y = data[ind,:]
        model.fit(x,y)        
        slope2 = model.coef_*100

        res = dict(donor=donor, z_name=z_name, w_name=w_name,lc_name=lc_name, n=len(dff.spatial_id.unique()))
        for p,s1,s2 in zip(params,slope1,slope2):
            res[f'{p}_BEF'] = s1[0]
            res[f'{p}_AFT'] = s2[0]
            res[f'{p}_DIF'] = s2[0]-s1[0]
        
        out.append(res)

    dfr = pandas.DataFrame(out)
    dfr.to_excel(out_fld/'stats_slopes_v2.xlsx')

def timesat_stats_perpixel():
#%%
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.pickle')
    input_data = 'https://docs.google.com/spreadsheets/d/1k020YkZRfRDWuYwBYMSDBzJwoDl6TA3d/export?format=csv&gid=1319881855'
    input_lc =  Path(r'D:\DATA\ETHIOPIA\LC\lc_250m.npz')
    out_fld = Path('D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\stats')

    df = pandas.read_pickle(input_file)
    df['EOS'] = df['SOS'] + df['LOS']

    dfd = pandas.read_csv(input_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','SLMP_MWS_shapefiles.imp_yr']]
    dfd.columns = ['objectid','donor','z_name','w_name','int_year']
    dfd = dfd.dropna()    
    dfd['int_year'] = dfd['int_year'].astype(int)


    df = df.join(dfd.set_index('objectid'), on='object_id')
    df = df.loc[~df.int_year.isnull()]

    with np.load(input_lc) as data:
        #print(data)
        lc=data['lc']

    df['lc'] = lc[df.spatial_id.values]
    df['lc_name'] = lc_legend[df.lc.values]
    #points = df.NPOINT.unique()
    #npoints = len(points)

    nparams = len(params)
    # df.to_csv(Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.csv'),index=False)
#%%
    out=[]

    dfg = df.groupby(['object_id', 'donor','z_name','w_name', 'lc_name'])
    keys = list(dfg.groups.keys())

    model = LinearRegression()

    for i,(object_id, donor,z_name,w_name, lc_name) in enumerate(keys):
        # i=609; key=keys[i]; (donor,z_name,w_name,lc_name)=key;
        # p = 'SOS'
        print(f'{i}/{len(keys)}: {donor}, {z_name}, {w_name}, {lc_name}')

        dff = dfg.get_group((object_id, donor,z_name,w_name,lc_name))

        npixels = len(dff.spatial_id.unique())
        
        data = preprocessing.StandardScaler().fit_transform(dff[params].values)
        data_sids = dff.spatial_id.values       
        ind_year = dff.YEAR < dff.int_year
        
        slope1 = np.full((npixels,nparams),np.nan)
        slope2 = np.full_like(slope1, np.nan)

        for isid, sid in enumerate(np.unique(data_sids)):
            # sid=data_sids[0]
            ind_sid = data_sids==sid
            ind = ind_sid & ind_year
            if ind.sum()>2:
                x = dff.YEAR.values[ind].reshape(-1,1)
                y = data[ind,:]
                model.fit(x,y)                
                slope1[isid] = model.coef_[:,0]*100

            ind = ind_sid & (~ind_year)
            if ind.sum()>2:
                x = dff.YEAR.values[ind].reshape(-1,1)
                y = data[ind,:]
                model.fit(x,y)        
                slope2[isid] = model.coef_[:,0]*100

        #res = scipy.stats.mannwhitneyu(slope1, slope2)         
        res = dict(object_id=object_id, donor=donor, z_name=z_name, w_name=w_name,int_year=dff.int_year.iloc[0], lc_name=lc_name, n=npixels)
        for ip,p in enumerate(params):
            # ip=0;p=params[ip]
            diff = slope2[:,ip] - slope1[:,ip]
            ws,ps = scipy.stats.wilcoxon(diff)
            res[f'{p}_DIF'] = np.nanmean(diff)
            res[f'{p}_p'] = ps
            
        
        out.append(res)

    dfr = pandas.DataFrame(out)
    dfr.to_excel(out_fld/'stats_slopes_v3.xlsx')
    dfr.to_pickle(out_fld/'stats_slopes_v3.df')
#%%

def table_for_report():
#%%
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_season_lc_sucess_v2.df')
    dfi = pandas.read_pickle(input_file)[['object_id','score']].query("~score.isnull()").drop_duplicates()
    dscore = dfi.set_index('object_id').to_dict()['score']
    for k in dscore:
        if dscore[k]==0.0:
            dscore[k]='no'
        else:
            dscore[k]='yes'

    out_fld = Path('D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\stats')
    df = pandas.read_pickle(out_fld/'stats_slopes_v3.df')
    df = df.query("lc_name=='Cropland'")
    df['int_year'] = df['int_year'].values.astype(int)

    xls = pandas.ExcelWriter(out_fld/'worst_best.xlsx')

    for p in ['LSI','BLV']:
        cd = f'{p}_DIF'
        cp = f'{p}_p'

        dfp = df.query(f"{cp}<0.05 and ~{cd}.isnull()")
        dfp = dfp.sort_values(cd)[['object_id','donor','z_name','w_name','int_year','n',cd,cp]]
        dfp[cd] = dfp[cd].round(2)
        dfp[cp] = dfp[cp].round(4)
        dfq = pandas.concat((dfp.iloc[:10].sort_values(cd, ascending=True), dfp.iloc[-10:].sort_values(cd, ascending=False)))

        dfq['sucess'] = dfq.apply(lambda r: dscore.get(r['object_id'],'NA'), axis=1)
        dfq.to_excel(xls,p)
    xls.close()
#%%




# %%
if __name__=='__main__':
    timesat_stats_perpixel()