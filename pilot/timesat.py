#%% 
from pathlib import Path
import rasterio
from rasterio.features import rasterize
import geopandas
import pandas
import numpy as np
from timesat_util import Timesat_Input, Timesat_Tpa
import pickle

proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

#%%
def gee_to_df(input_folder, input_shape, input_property, output_file):
    # convert images from gee to dataframe sutiable for timesat_util
    '''
    input_folder = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs')
    input_shape = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\SLMP_MWS_2017-05-01_modis.shp')
    input_property = 'OBJECTID'
    output_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs.pickle')
    '''

    shp = geopandas.read_file(input_shape)

    files = list(input_folder.glob('modis_*.tif'))

    with rasterio.open(files[0]) as src: 
        profile = src.profile
        w = profile['width']
        h = profile['height']
        t = profile['transform']
        output_georef = output_file.with_name(output_file.with_suffix('').name+'_georef.pickle')
        pickle.dump(profile, output_georef.open('wb'))

    shapes = zip(shp.geometry, shp[input_property])
    objectids = rasterize(shapes, out_shape=(h,w), fill=0, transform=t, dtype=rasterio.int16)
    objectids_ind = objectids>0
    objectids = objectids[objectids_ind]
    objectids_fn = output_file.with_name(output_file.with_suffix('').name+'_objectids.txt')
    np.savetxt(objectids_fn, objectids,'%d')

    sids = np.arange(h*w).reshape((h,w))
    sids = sids[objectids_ind]

    n = objectids.shape[0]
    
    '''
    with rasterio.open(input_folder.parent/'test.tif', 'w', driver='GTiff', width=w, height=h, count=1, crs=proj4_modis, transform=t, dtype=rasterio.int16) as dst:
        dst.write(ids, 1)
    '''

#%%
    spatial_id=[]
    object_id = []
    time_id=[]
    ndvi=[]
    evi=[]
    viq=[]
    for f in files:
        # f= files[0]
        print(f.name)
        date=f.with_suffix('').name.split('_')[-1]
        with rasterio.open(f) as src:            
            img_ndvi = src.read(1)[objectids_ind]
            img_evi = src.read(2)[objectids_ind]
            img_viq = src.read(3)[objectids_ind]

        spatial_id.append(sids)
        object_id.append(objectids)
        time_id.extend([date]*n)
        ndvi.append(img_ndvi)
        evi.append(img_evi)
        viq.append(img_viq)

    spatial_id = np.concatenate(spatial_id)
    object_id = np.concatenate(object_id)
    ndvi = np.concatenate(ndvi)
    evi = np.concatenate(evi)
    viq = np.concatenate(viq)

    df = pandas.DataFrame(data = dict(object_id = object_id, spatial_id=spatial_id, time_id=time_id,ndvi=ndvi, evi=evi, viq=viq))
    df.to_pickle(output_file)

    dfs = df[['spatial_id','object_id']].drop_duplicates().sort_values('spatial_id')
    dfs.to_pickle(output_file.with_name(output_file.with_suffix('').name+'_sids.pickle'))

#%%

def prepare_timesat(input_file, output_file):
    # Read dataframe and prepare input for timesat
    '''
    input_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs.pickle')
    output_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\timesat_input.txt')
    '''
    df = pandas.read_pickle(input_file)
    df['time_id'] = pandas.to_datetime(df.time_id)
    df = df.loc[df.time_id.dt.year<=2020]
    df['weight'] = 15 - df.viq*5
    del df['viq']

    ti = Timesat_Input()    
    ti.load_from_df(df)

    spatial_ids = df['spatial_id'].unique()

    del df

    ti.write_timesat(output_file, bands=['ndvi','evi'], kind='simple')

    # subsample
    subsample_ids = np.random.choice(spatial_ids, 1000)
    subsample_fn = output_file.with_name(output_file.with_suffix('').name+'_ss.txt')
    ti.write_timesat(subsample_fn, bands=['ndvi','evi'], kind='simple', spatial_ids=subsample_ids)


def timesat_pp(input_file, spatialid_file):
    # Postprocessing of TIMESAT outputs
    '''
    input_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\ethiopia1_STL.tpa')
    spatialid_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs_sids.pickle')
    '''
    tpa = Timesat_Tpa(input_file)

    dfo = tpa.decode_data(first_year=2000, season_offset=1, season_stride=1)
    dfo.NPOINT = dfo.NPOINT-1
    #dfo.to_excel(input_file.with_name('ethiopia1_seasons.xlsx'))    

    dfs = pandas.read_pickle(spatialid_file)
    dfo = dfo.join(dfs, on='NPOINT')
    dfo.to_pickle(input_file.with_name('timesat_seasons.pickle'))


def mws_params_shape(input_file, input_shapefile, output_file):
    # Calculate slope for every mws/seasonal param and write it to shapefile
    '''
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.pickle')
    input_shapefile = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\SLMP_MWS_2017-05-01_modis.shp')
    output_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\mws_slopes.shp')
    '''
    import geopandas
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import scale
    params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

    def agg_slope(dff):
        x = dff.YEAR.values.reshape(-1,1)
        y = scale(dff[params].values)
        model = LinearRegression(normalize=True)
        model.fit(x,y)
        return model.coef_.reshape(-1)


    df = pandas.read_pickle(input_file)
    df['EOS'] = df['SOS'] + df['LOS']

    dfg = df.groupby('object_id')
    # oid=2319; dff=df.loc[dfg.groups[oid]]
    dfs = dfg.apply(agg_slope)
    dfs = pandas.DataFrame.from_dict(dict(zip(dfs.index, dfs.values))).T
    dfs.columns = [f'{p}_slp' for p in params]

    dfs.to_pickle(output_file.with_suffix('.pickle'))

    shp = geopandas.read_file(input_shapefile)
    shp = shp.join(dfs, on='OBJECTID')
    shp.to_file(output_file)


def mws_params_boxplots():
    '''
    fldout = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\boxplots')
    input_file =  Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.pickle')
    input_shapefile = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\mws_slopes.shp')

    '''
#%%
    from pathlib import Path
    import geopandas
    import pandas
    import numpy as np
    import seaborn as sns
    import matplotlib.pyplot as plt

    params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

    df = pandas.read_pickle(input_file)
    df = df.loc[df.YEAR<2021]
    df['EOS'] = df['SOS'] + df['LOS']

    shp = geopandas.read_file(input_shapefile)
    shp = shp.loc[~shp['SSI_slp'].isna()].sort_values('SSI_slp')

    '''
    nn = 10
    inds = np.r_[np.arange(nn),-np.arange(nn,0,-1)]
    objectids = shp['OBJECTID'].values[inds]
    slopes = (shp['SSI_slp'].values[inds]*1000).astype(int)
    '''
    oid_groups = dict(ale_woreda=np.arange(1671,1678),
                      bore_woreda=np.arange(1660,1670),
                      sekela=np.arange(601,614))
#%%
    for grp, objectids in oid_groups.items():
        print(grp)
        #objectids = df.object_id.unique()
        slopes = shp.set_index('OBJECTID').loc[objectids,'SSI_slp']
        n = len(objectids)
        for i,(oid,slp) in enumerate(zip(objectids,slopes)):
            print(f'{i}/{n}: {oid}')
            # oid = objectids[45]
            dff = df.loc[df.object_id==oid]
            plt.close('all')
            fig, axs = plt.subplots(13,1, figsize=(10,30), sharex=True, constrained_layout=True)
            for ax, var in zip(axs, params):
                sns.boxplot(x='YEAR', y=var, data=dff, ax=ax).set(xlabel='', ylabel=var)            
                ax.grid(axis='y', linestyle='--')
            ax.set_xlabel('YEAR')
            #fig.suptitle(f'OBJECTID: {oid}, SLOPE: {slp:04d}', fontsize=16)
            fig.suptitle(f'OBJECTID: {oid}', fontsize=16)

            #dstpath = fldout/grp/f'slp[{slp:04d}]_{oid:04d}_boxplot.png'
            dstpath = fldout/grp/f'{grp}_{oid:04d}_boxplot.png'
            dstpath.parent.mkdir(parents=True, exist_ok=True)
            plt.savefig(dstpath, dpi=200, bbox_inches='tight')

            #sns.boxplot(x='YEAR', y='SOS', data=dff)
            #sns.violinplot(x='YEAR', y='SOS', data=dff)
            
            #sns.swarmplot(x='YEAR', y='SOS', data=dff, color='.25')

#%%
def maps_all_seasons_params():
    '''
    generate layers for every season/parameter from TIMESAT outputs of MODIS
    '''
#%%
    proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

    input_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.pickle')
    georef_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs_georef.pickle')
    output_fld = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\allmaps_timesat')
    output_fld.mkdir(parents=True, exist_ok=True)
    params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

    df = pandas.read_pickle(input_file)
    df['EOS'] = df['SOS']+df['LOS']
    profile = pickle.load(georef_file.open('rb'))

    width, height = profile['width'], profile['height']
    profile['nodata']=-9999.0
    profile['dtype'] = rasterio.float32
    profile['crs'] = proj4_modis
    profile['count']=1
    profile['driver'] = 'COG'
    profile['compress'] = 'DEFLATE'

    years = df['YEAR'].unique()
    data = np.zeros(height*width, dtype=np.float32)-9999.0
    for y in years:
        ind = (df.YEAR==y)
        dff = df.loc[ind]
        spatial_id = dff.spatial_id.values
        for p in params:        
            print(f'{y} - {p}')
        
            data[:]=-9999.0
            data[spatial_id] = dff[p].values

            pdata = data.reshape(height, width)

            with rasterio.open(output_fld/f'modis_{p}_{y}.tif', 'w', **profile) as dst:
                dst.write(pdata, 1)
            

#%%

def maps_of_mean_params_per_pixel():
    pass
#%%
    import pandas, pickle
    from pathlib import Path
    import numpy as np
    import rasterio
    from sklearn.preprocessing import robust_scale
    from sklearn.linear_model import LinearRegression

    proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

    input_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\timesat_seasons.pickle')
    georef_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs_georef.pickle')
    output_fld = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\maps_timesat')
    output_fld.mkdir(parents=True, exist_ok=True)
    params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

    df = pandas.read_pickle(input_file)
    df['EOS'] = df['SOS']+df['LOS']
    profile = pickle.load(georef_file.open('rb'))

    width, height = profile['width'], profile['height']
    profile['nodata']=-9999.0
    profile['dtype'] = rasterio.float32
    profile['crs'] = proj4_modis
    profile['count']=1

    dfg_mean = df.groupby('spatial_id').mean()
    dfg_slope = df.groupby('spatial_id')
    spatial_id = dfg_mean.index.values
    
    

    for p in params:
        # p=params[0]
        print(p)
        data = np.zeros(height*width, dtype=np.float32)-9999.0

        # mean
        data[spatial_id] = dfg_mean[p].values
        data = data.reshape(height, width)

        with rasterio.open(output_fld/f'modis_mean_{p}.tif', 'w', **profile) as dst:
            dst.write(data, 1)

    # slope
    model = LinearRegression()
    data = np.zeros((height*width, len(params))) -9999.0
    for sid, dff in dfg_slope:
        # sid = list(dfg_slope.groups.keys())[0]; dff = dfg_slope.get_group(sid)
        if sid%100==0:
            print(f'{sid}/{max(spatial_id)}')

        x = dff.YEAR.values.reshape(-1,1)
        y = robust_scale(dff[params].values, unit_variance=True) * 100
        model.fit(x,y)

        data[sid,:]=model.coef_[:,0]

    for pi, p in enumerate(params):
        with rasterio.open(output_fld/f'modis_slope_{p}.tif', 'w', **profile) as dst:
            pdata = data[:,pi].reshape(height, width).astype(np.float32)
            dst.write(pdata, 1)


#%%

if __name__=='__main__':
    
    '''
    input_folder = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs')
    input_shape = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\SLMP_MWS_2017-05-01_modis.shp')
    input_property = 'OBJECTID'
    output_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs.pickle')
    gee_to_df(input_folder, input_shape, input_property, output_file)
    '''

    
    input_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs.pickle')
    output_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\timesat_input.txt')
    prepare_timesat(input_file, output_file)
    
    # Dovde je napravljeno za 2022 godinu

    '''
    input_file = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\ethiopia1_STL.tpa')
    spatialid_file = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs_sids.pickle')
    timesat_pp(input_file, spatialid_file)
    '''

    #timesat_pp(Path('D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1ss\ethiopia1_STL.tpa'))
    #timesat_pp(Path('D:\DATA\ETHIOPIA\TIMESAT\ws_allpixs\output_ethiopia1\ethiopia1_STL.tpa'))

    #maps_all_seasons_params()