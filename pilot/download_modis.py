#%%
#import geemap
import geetools
import ee
import numpy as np
import pandas
import datetime
import pickle
from pathlib import Path

ee.Initialize()

#%%
def test():
#%%
    areas = ee.FeatureCollection('users/multione/ethiopia/bounds')
    roi = areas.geometry().bounds()
    collection = ee.ImageCollection("MODIS/006/MOD13Q1").select("EVI")

#%%
    img = collection.first()
    reduced_areas = img.reduceRegions(areas, ee.Reducer.median())

#%%
    date = ee.Date(img.get('system:time_start'))
    print(date.format('YYYY-mm-dd').getInfo())
    datetime.datetime.utcfromtimestamp(date.getInfo()['value']/1000)

#%%
    objectid = pickle.load(open(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GEE\objectid.pickle','rb'))
    modis_list = pickle.load(open(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GEE\modis_median_reduce.pickle','rb'))

#%%  Učitavanje    

    fld = Path(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GEE\modis_median')
    l = fld.glob('*.pickle')
    res=[]
    for f in l:
        d = pickle.load(open(f,'rb'))
        res.append(d)

    n = res[0]['ndvi'].shape[0]
    nd = len(res)
    ndvi = np.zeros((nd, n)) * np.nan
    evi = np.zeros((nd, n)) * np.nan

    for i,d in enumerate(res):
        ndvi[i,:] = d['ndvi']
        evi[i,:] = d['evi']

#%% Crtanje
    from matplotlib import pyplot as plt

    for d in res[:3]:
        dd = d['date']

# %%

def NoneToNan(x):
    if x is None:
        return np.nan
    else:
        return  x

def reduced_modis():
    fld = Path(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GEE\modis_median')

    areas = ee.FeatureCollection('users/multione/ethiopia/bounds')
    roi = areas.geometry().bounds()
    collection = ee.ImageCollection("MODIS/006/MOD13Q1").select(["NDVI","EVI"])

    #objectid = np.array(areas.aggregate_array('OBJECTID').getInfo())
    #pickle.dump(objectid, open(r'C:\MO\PROJEKTI\RAZNO\ETIOPIJA\GEE\objectid.pickle','wb'))

    img_list = collection.toList(collection.size())
    img_count = img_list.size().getInfo()
    print(img_count)
    res=[]
    for i in range(img_count):  #i=0
        print(f'{i}/{img_count}')
        ff = fld/f'modis_{i:03}.pickle'

        if not ff.exists():
            img = ee.Image(img_list.get(i)).clip(roi)
            
            date = datetime.datetime.utcfromtimestamp(img.get('system:time_start').getInfo()/1000)
            reduced_areas = img.reduceRegions(areas, ee.Reducer.median())
            ra = reduced_areas.getInfo()
            
            objectid =  np.array([f['properties']['OBJECTID'] for f in ra['features']]) #np.array(reduced_areas.aggregate_array('OBJECTID').getInfo())
            median_ndvi = np.array([NoneToNan(f['properties']['NDVI']) for f in ra['features']]) * 0.0001  #np.array(reduced_areas.aggregate_array('NDVI').getInfo()) * 0.0001
            median_evi = np.array([NoneToNan(f['properties']['EVI']) for f in ra['features']]) * 0.0001  #np.array(reduced_areas.aggregate_array('EVI').getInfo()) * 0.0001

            d = dict(date=date, objectid=objectid, ndvi=median_ndvi, evi=median_evi)

            pickle.dump(d, open(ff,'wb'))

def pixel_modis():
#%%
    # download all pixels that intersects watersheds, every date - one image with EVI and NDVI 
    fld = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs')

    areas = ee.FeatureCollection('users/multione/ethiopia/bounds_wgs84')
    roi = areas.geometry().bounds()
    collection = ee.ImageCollection("MODIS/006/MOD13Q1").filterDate('2021-07-29','2022-12-31').select(["NDVI","EVI","SummaryQA"])

    img_list = collection.toList(collection.size())
    img_count = img_list.size().getInfo()
    print(img_count)
    res=[]
    for i in  range(img_count):#range(img_count):  #i=0
        print(f'{i}/{img_count}')
        ff = fld/f'modis_{i:03}.pickle'

        #if not ff.exists():
        img = ee.Image(img_list.get(i)).clip(roi)
        date = datetime.datetime.utcfromtimestamp(img.get('system:time_start').getInfo()/1000)
        mask =ee.Image.constant(1).clip(areas.geometry()).mask()
        img_msk = img.updateMask(mask)
        #task=geetools.batch.Export.image.toDrive(img_msk, folder='GEE1', fileNamePrefix='test1')
        sdate = f'{date:%Y%m%d}'
        task = geetools.batch.Export.image.toCloudStorage(img_msk, f'modis_{sdate}', bucket='mo_temp', fileNamePrefix=f'ethiopia_modis/modis_{sdate}')
        task.start()
            #sample = img.sampleRegions(areas, ['OBJECTID'])
            #ss = sample.getInfo()
            #geetools.batch.Export.image.toDriveByFeature(img, areas, folder='GEE', 
            #    namePattern='modis_{OBJECTID}', scale=250, dataType='int', verbose=True)
def pixel_modis_ale_sekele():
#%%
    # download all pixels that intersects watersheds, every date - one image with EVI and NDVI 
    #fld = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_ale_sekele')

    areas = ee.FeatureCollection('users/multione/ethiopia/bounds_wgs84')
    collection = ee.ImageCollection("MODIS/006/MOD13Q1").select(["NDVI","EVI","SummaryQA"])
    img_list = collection.toList(collection.size())
    img_count = img_list.size().getInfo()
    print(img_count)

    for wsname in ['Ale','Sekela']:
        
        ws = areas.filter(ee.Filter.eq('W_NAME',wsname))
        roi = ws.geometry().bounds().buffer(1000).bounds()
                
        res=[]
        for i in  range(img_count):#range(img_count):  #i=0
            print(f'{wsname} {i}/{img_count}')
            #ff = fld/f'modis_{i:03}.pickle'

            #if not ff.exists():
            img = ee.Image(img_list.get(i)).clip(roi)
            date = datetime.datetime.utcfromtimestamp(img.get('system:time_start').getInfo()/1000)
            #mask =ee.Image.constant(1).clip(areas.geometry()).mask()
            #img_msk = img.updateMask(mask)
            #task=geetools.batch.Export.image.toDrive(img_msk, folder='GEE1', fileNamePrefix='test1')
            sdate = f'{date:%Y%m%d}'
            task = geetools.batch.Export.image.toCloudStorage(img, f'modis_{wsname}_{sdate}', bucket='mo_temp', fileNamePrefix=f'ethiopia_modis/modis_{wsname}_{sdate}')
            task.start()
            #sample = img.sampleRegions(areas, ['OBJECTID'])
            #ss = sample.getInfo()
            #geetools.batch.Export.image.toDriveByFeature(img, areas, folder='GEE', 
            #    namePattern='modis_{OBJECTID}', scale=250, dataType='int', verbose=True)

def gee_stop_all_tasks():
#%% 
    tl = ee.batch.Task.list()
    for t in tl:
        s = t.status()
        if s['state'] not in ('COMPLETED', 'CANCELED'):
            print (s['description'])
            t.cancel()
#%%

def read_modic_nc():
#%%
    from netCDF4 import Dataset
    filename = r'D:\DATA\ETHIOPIA\MODIS\test1\MOD13Q1.006_250m_aid0001.nc'
    nc = Dataset(filename)

    proj = nc['crs'].spatial_ref

#%%
if __name__=='__main__':
    #educed_modis()
    pixel_modis_ale_sekele()