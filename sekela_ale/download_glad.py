'''
Download all data from GLAD ARD dataset, tiles:
Sekela: 037E_11N, 037E_10N 
Ale:    035E_08N
'''
#%%
from datetime import timedelta
import geopandas
import requests
from pathlib import Path
from shapely import geometry

from shapely.ops import transform

glad_tiles = ['037E_11N', '037E_10N', '035E_08N']
intervals = list(range(461, 944))   # 2000 - 2020
username = 'krizanjosip'
password = 'SHBsLTmDfskDM6Ol'
fld = Path(r'D:\DATA\ETHIOPIA\GLAD\GLAD_TILES')

#%%
def download_glad_image(tile, year, t16, outfolder):
    # downloads one image
    lat = tile.split('_')[-1]
    interval = (year-1980)*23 + t16 +1
    #year = (interval - 1) // 23 + 1980
    #t16 = interval - (year-1980)*23 - 1

    url = f'https://glad.umd.edu/dataset/landsat_v1.1/{lat}/{tile}/{interval}.tif'
    outfilename = f'glad_{tile}_{year}_{t16:02d}.tif'
    outfile = Path(outfolder)/outfilename

    with requests.get(url, stream=True, auth=(username, password)) as r:
        r.raise_for_status()
        with open(outfile, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024*1024*10):
                print('.', end='', flush=True)
                f.write(chunk)
        print('Done.')

def download_Ale():
    for year in range(2000,2021):
        print(year)
        for t16 in range(23):
            print(f'\t{t16+1} ', end='', flush='True')
            download_glad_image('035E_08N', year, t16, fld)

def download_Sekela():
    for year in range(2000,2021):
        print(year)
        for t16 in range(23):
            print(f'\t{t16+1} ', end='', flush='True')
            download_glad_image('037E_11N', year, t16, fld)
            download_glad_image('037E_10N', year, t16, fld)
#%%
def glad2nc_Ale():
    #%%    
    roi_pilot = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    buffer_m = 500
    outfile = r'D:\DATA\ETHIOPIA\GLAD\glad_Ale.nc'

    import geopandas
    from netCDF4 import Dataset
    import rasterio as rio
    import rasterio.features, rasterio.transform
    import itertools
    import numpy as np

    shp = geopandas.read_file(roi_pilot)
    shp = shp.query("W_NAME=='Ale'")    
    shp_buffer = shp.buffer(buffer_m)

    glad_files = list(fld.glob("glad_035E_08N*.tif"))
    nfiles = len(glad_files)

    with rio.open(glad_files[0]) as src:
        # src= rio.open(glad_files[0])
        profile = src.profile
        #width = profile['width']
        #height= profile['height']

    glad_crs = profile['crs']
    #roi_crs = shp.crs
    
    shp_buffer = shp_buffer.to_crs(glad_crs)
    roi = shp_buffer.unary_union   #buffer od pola km oko WS-a
    roi_bnd = roi.bounds

    window = rio.windows.from_bounds(*roi_bnd, 
            transform=profile['transform'], width=profile['width'], height=profile['height'])
    window = window.round_shape(op='ceil').round_offsets()
    window_transform = rio.windows.transform(window, profile['transform'])

    shp = shp.to_crs(glad_crs)
    shapes = zip(shp.geometry, shp['OBJECTID'])
    objectids = rio.features.rasterize(shapes, 
                        out_shape=(window.height, window.width), 
                        fill=0, 
                        transform=window_transform, 
                        dtype=rio.int16)
#%%
    nc = Dataset(outfile, 'w', format="NETCDF4")

    x = nc.createDimension("x", window.width)
    y = nc.createDimension("y", window.height)
    time = nc.createDimension("time", nfiles)

    lon = nc.createVariable("lon", "f4", ("y","x"))
    lat = nc.createVariable("lat", "f4", ("y","x"))
    year = nc.createVariable("year", "i4", ("time",))
    t16 = nc.createVariable("t16", "i4", ("time",))
    glad_interval = nc.createVariable("glad_interval", "i4", ("time",))

    objectid = nc.createVariable("objectid", "i4", ("y", "x"), zlib=True)
    ndvi = nc.createVariable("ndvi", "i2", ("y", "x", "time"), zlib=True)
    qf = nc.createVariable("qf", "i1", ("y", "x", "time"), zlib=True)

    modis_ndvi_nn = nc.createVariable("modis_ndvi_nn", "i2", ("y", "x", "time"), zlib=True)
    modis_ndvi_bl = nc.createVariable("modis_ndvi_bl", "i2", ("y", "x", "time"), zlib=True)
    modis_ndvi_cb = nc.createVariable("modis_ndvi_cb", "i2", ("y", "x", "time"), zlib=True)
    modis_qa = nc.createVariable("modis_qa", "i1", ("y", "x", "time"), zlib=True)
    modis_sids = nc.createVariable("modis_sids", "i4", ("y", "x"), zlib=True)

    nc.crs = profile['crs'].to_string()
    nc.transform_gdal = window_transform.to_gdal()
    nc.width = window.width
    nc.height = window.height
    # coords
    #row_range, col_range = window.toranges()
    rows, cols= zip(*itertools.product(range(window.height), range(window.width)))
    dlons, dlats = rio.transform.xy(window_transform, rows, cols)
    '''
    tst = geopandas.GeoDataFrame(data={'row':rows, 'col':cols}, geometry=geopandas.points_from_xy(dlons, dlats))
    tst.to_file(r'D:\DATA\ETHIOPIA\GLAD\glad_Ale_coords.shp')
    '''
    lon[:] = dlons
    lat[:] = dlats

    objectid[:] = objectids

    for fn in glad_files:
        # fn = glad_files[0]
        print(fn)
        (_,_,_,yy,tt)=fn.with_suffix('').name.split('_')
        dyear = int(yy); dt16 = int(tt)
        interval = (dyear-1980)*23 + dt16 +1
        t = (dyear - 2000) * 23 + dt16
        year[t] = dyear
        t16[t] = dt16
        glad_interval[t] = interval
        with rasterio.open(fn) as src:
            # src = rasterio.open(fn)

            nir = src.read(4, window = window)
            red = src.read(3, window = window)
            dqf = src.read(8, window = window)

            nrsum = nir + red
            nrdiff = nir - red
            ind = nrsum>0
            dndvi = np.zeros_like(nrsum, dtype=np.int16)-9999
            dndvi[ind] = nrdiff[ind]/nrsum[ind]*10000

            ndvi[:,:,t]=dndvi
            qf[:,:,t]=dqf


    nc.close()    

def glad2nc_Sekela():
#%%    
    roi_pilot = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    buffer_m = 500
    outfile = r'D:\DATA\ETHIOPIA\GLAD\glad_Sekela.nc'

    import geopandas
    from netCDF4 import Dataset
    import rasterio as rio
    import rasterio.features, rasterio.transform
    from rasterio.merge import merge
    import itertools
    import numpy as np

    shp = geopandas.read_file(roi_pilot)
    shp = shp.query("W_NAME=='Sekela'")    
    shp_buffer = shp.buffer(buffer_m)

    glad_files = list(fld.glob("glad_037E*.tif"))
    glad_dates = np.unique(['_'.join(f.with_suffix('').name.split('_')[-2:]) for f in glad_files])
    nfiles = len(glad_files)
    ndates = len(glad_dates)

    with rio.open(fld/'glad_037E_11N_2000_00.tif') as src:
        profile = src.profile

    glad_crs = profile['crs']
    
    shp_buffer = shp_buffer.to_crs(glad_crs)
    roi = shp_buffer.unary_union   #buffer od pola km oko WS-a
    roi_bnd = roi.bounds

    window = rio.windows.from_bounds(*roi_bnd, 
            transform=profile['transform'], width=profile['width'], height=profile['height'])
    window = window.round_shape(op='ceil').round_offsets()
    window_slices = window.toslices()
    window_transform = rio.windows.transform(window, profile['transform'])

    shp = shp.to_crs(glad_crs)
    shapes = zip(shp.geometry, shp['OBJECTID'])
    objectids = rio.features.rasterize(shapes, 
                        out_shape=(window.height, window.width), 
                        fill=0, 
                        transform=window_transform, 
                        dtype=rio.int16)
#%%
    nc = Dataset(outfile, 'w', format="NETCDF4")

    x = nc.createDimension("x", window.width)
    y = nc.createDimension("y", window.height)
    time = nc.createDimension("time", ndates)

    lon = nc.createVariable("lon", "f4", ("y","x"))
    lat = nc.createVariable("lat", "f4", ("y","x"))
    year = nc.createVariable("year", "i4", ("time",))
    t16 = nc.createVariable("t16", "i4", ("time",))
    glad_interval = nc.createVariable("glad_interval", "i4", ("time",))

    objectid = nc.createVariable("objectid", "i4", ("y", "x"), zlib=True)
    ndvi = nc.createVariable("ndvi", "i2", ("y", "x", "time"), zlib=True)
    qf = nc.createVariable("qf", "i1", ("y", "x", "time"), zlib=True)

    modis_ndvi_nn = nc.createVariable("modis_ndvi_nn", "i2", ("y", "x", "time"), zlib=True)
    modis_ndvi_bl = nc.createVariable("modis_ndvi_bl", "i2", ("y", "x", "time"), zlib=True)
    modis_ndvi_cb = nc.createVariable("modis_ndvi_cb", "i2", ("y", "x", "time"), zlib=True)
    modis_qa = nc.createVariable("modis_qa", "i1", ("y", "x", "time"), zlib=True)
    modis_sids = nc.createVariable("modis_sids", "i4", ("y", "x"), zlib=True)

    nc.crs = profile['crs'].to_string()
    nc.transform_gdal = window_transform.to_gdal()
    nc.width = window.width
    nc.height = window.height
    # coords
    #row_range, col_range = window.toranges()
    rows, cols= zip(*itertools.product(range(window.height), range(window.width)))
    dlons, dlats = rio.transform.xy(window_transform, rows, cols)
    '''
    tst = geopandas.GeoDataFrame(data={'row':rows, 'col':cols}, geometry=geopandas.points_from_xy(dlons, dlats))
    tst.to_file(r'D:\DATA\ETHIOPIA\GLAD\glad_Ale_coords.shp')
    '''
    lon[:] = np.array(dlons).reshape(window.height, window.width)
    lat[:] = np.array(dlats).reshape(window.height, window.width)

    objectid[:] = objectids

    for date in glad_dates:
        # date = glad_dates[0]
        (yy,tt)=date.split('_')
        fn1 = fld/f'glad_037E_11N_{date}.tif'
        fn2 = fld/f'glad_037E_10N_{date}.tif'
        print(fn1)
        fns=[fn1, fn2]
        
        
        dyear = int(yy); dt16 = int(tt)
        interval = (dyear-1980)*23 + dt16 +1
        t = (dyear - 2000) * 23 + dt16
        year[t] = dyear
        t16[t] = dt16
        glad_interval[t] = interval
        #with rasterio.open(fn1) as src:
            # src = rasterio.open(fn)
        srcs = [rio.open(fn) for fn in fns]
        data, transform = merge(srcs)
        for src in srcs:
            src.close()
        nir = data[3,window_slices[0],window_slices[1]]
        red = data[2,window_slices[0],window_slices[1]]
        dqf = data[7,window_slices[0],window_slices[1]]

        nrsum = nir + red
        nrdiff = nir - red
        ind = nrsum>0
        dndvi = np.zeros_like(nrsum, dtype=np.int16)-9999
        dndvi[ind] = nrdiff[ind]/nrsum[ind]*10000

        ndvi[:,:,t] = dndvi
        qf[:,:,t] = dqf


    nc.close()    
#%%

# Trebam još pospojiti MODIS pixele sa GLAD pixelima, 
# Bilo bi dobro da i MODIS imam slično složeno tako da mogu od njih uzimati interpolirane vrijednosti na 
# centrima GLAD pixela (možda za sveki GLAD pixel -> 4 najbliža modis pixela i udaljenost do njihovih središta)

def glad_modis_nc(wsname):
    # wsname = 'Ale'
    # wsname = 'Sekela'
#%%
    import pickle
    import pandas
    from netCDF4 import Dataset
    import rasterio as rio
    import rasterio.transform
    import pyproj
    import geopandas
    import scipy.spatial
    from scipy.interpolate import griddata
    import numpy as np
    from datetime import datetime, timedelta
    from pathlib import Path
    import itertools

    proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

    
    fld_modis = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_ale_sekela')

    nc=Dataset(f'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc','a')
    glad_lon = nc['lon'][:].data
    glad_lat = nc['lat'][:].data
    year = nc['year'][:].data
    t16 = nc['t16'][:].data

    modis_ndvi_nn = nc['modis_ndvi_nn']
    modis_ndvi_bl = nc['modis_ndvi_bl']
    modis_ndvi_cb = nc['modis_ndvi_cb']
    modis_qa = nc['modis_qa']
    modis_sids = nc['modis_sids']
    
    crs_glad = pyproj.crs.CRS.from_string(nc.crs)
    crs_modis = pyproj.crs.CRS.from_string(proj4_modis)
    #transform_glad = nc.transform_gdal
    #transform_glad=rio.transform.Affine.from_gdal(*transform_glad)
    #nc.close()

    # reproject glad pixel coordinates to modis projection
    tr = pyproj.Transformer.from_crs(crs_glad, crs_modis, always_xy=True)
    glad_x, glad_y = tr.transform(glad_lon, glad_lat)
    glad_xy = np.c_[glad_x.reshape(-1), glad_y.reshape(-1)]

#%%
    modis_x=None; modis_y=None
    ntimes = nc.dimensions['time'].size
    for i in range(ntimes): #i=0
        # Few first dates in 2000 are replaced with data from 2001
        date = datetime(year[i],1,1) + timedelta(days=16*int(t16[i]))
        if date<datetime(2000,2,18):
            date =datetime(2001,1,1) + timedelta(days=16*int(t16[i]))
        
        #output filename
        modis_fn = fld_modis/f'modis_{wsname}_{date:%Y%m%d}.tif'
        print (modis_fn)

        #read modis raster
        with rio.open(modis_fn) as src:
            # src = rio.open(modis_fn)
            if modis_x is None:
                # coordinates of modix pixels
                rows, cols= zip(*itertools.product(range(src.profile['height']), range(src.profile['width'])))
                modis_x, modis_y = rio.transform.xy(src.profile['transform'], rows, cols)
                modis_xy = np.c_[modis_x, modis_y]
                spatial_ids = np.arange(src.profile['height']*src.profile['width']).reshape((src.profile['height'],src.profile['width']))
                modis_sids[:,:] = griddata(modis_xy, spatial_ids.reshape(-1), glad_xy, 'nearest', -1).reshape(glad_lon.shape)

            modis_data = src.read(1)            

            # interpolate modis data to glad pixel location (nearest, linear and cubic)
            ndvi_nn = griddata(modis_xy, modis_data.reshape(-1), glad_xy, 'nearest', 0)
            modis_ndvi_nn[:,:,i] = ndvi_nn
            ndvi_bl = griddata(modis_xy, modis_data.reshape(-1), glad_xy, 'linear', 0)
            modis_ndvi_bl[:,:,i] = ndvi_bl
            ndvi_cb = griddata(modis_xy, modis_data.reshape(-1), glad_xy, 'cubic', 0)
            modis_ndvi_cb[:,:,i] = ndvi_cb

            # qa data are interpolatred as nearest
            modis_data_qa = src.read(3)
            qa_nn = griddata(modis_xy, modis_data_qa.reshape(-1), glad_xy, 'nearest')
            modis_qa[:,:,i] = qa_nn

            # spatial_ids
            

            '''
            # test to save interpolated data as tiff
            dst_profile = src.profile
            dst_transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
            dst_profile.update({'count':5, 'dtype':'float32', 'width':nc.width, 'height':nc.height, 'transform':dst_transform,'crs':crs_glad})

            with rio.open(r'D:\DATA\ETHIOPIA\GLAD\test_modis_glad.tif','w', **dst_profile) as dst:
                dst.write(ndvi_nn.astype(np.float32).reshape((nc.height,-1)),1)
                dst.write(ndvi_bl.astype(np.float32).reshape((nc.height,-1)),2)
                dst.write(ndvi_cb.astype(np.float32).reshape((nc.height,-1)),3)
                dst.write(nc['ndvi'][:,:,i].astype(np.float32).reshape((nc.height,-1)),4)
                dst.write(qa_nn.astype(np.float32).reshape((nc.height,-1)),5)
            '''
    nc.close()

def test_save_tiff():
#%%
    from netCDF4 import Dataset
    import rasterio as rio

    profile = dict(driver='GTiff', nodata=-9999.0, dtype=rio.int32, crs='EPSG:4326', count=1)
    for wsname in ['Ale', 'Sekela']:
        # wsname = 'Ale'
        nc=Dataset(f'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc')
    
        transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
        profile.update({'width':nc.width, 'height':nc.height, 'transform':transform})

        with rio.open(fr'D:\DATA\ETHIOPIA\GLAD\glad_objectid_{wsname}.tif','w', **profile) as dst:
            dst.write(nc['objectid'][:].data,1)

        nc.close()        

#%%
if __name__ == '__main__':
    #download_glad_image(glad_tiles[0], 2020, 0, fld)
    #download_Ale()
    #download_Sekela()
    #glad2nc_Ale()
    #glad2nc_Sekela()
    glad_modis_nc('Sekela')