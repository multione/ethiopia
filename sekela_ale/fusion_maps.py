#%%
import pandas, pickle, geopandas
from pathlib import Path
import numpy as np
import rasterio as rio
from netCDF4 import Dataset
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import scale, robust_scale
import seaborn as sns
import matplotlib.pyplot as plt

params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']
profile = dict(driver='GTiff', nodata=-9999.0, dtype=rio.float32, crs='EPSG:4326', count=1)
proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

#%%
def agg_slope(years, data):
    #x = years.flatten().reshape(-1,1)
    # samo pixeli sa svim sezonama
    ind = (data.mask.sum(axis=2)==0).flatten()

    x = years.data.reshape((-1,years.shape[2]))[ind,:].mean(axis=0).reshape(-1,1).astype(np.float32) #.flatten()
    y = robust_scale(data.data.reshape(-1,data.shape[2])[ind,:], unit_variance=True).T * 100
    slope = np.ma.masked_all(ind.shape[0], dtype=np.float32) #np.zeros(ind.shape[0],dtype=np.float32)
    
    model = LinearRegression()
    model.fit(x,y)
    slope[ind] = model.coef_.flatten()
    return slope.reshape(*data.shape[:2])

def maps_all(wsname, ver):        
#%%
    # wsname='Ale'; ver='v8'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\allmaps_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)

    var = 'ndvi_cb'    
    input_file_nc = input_folder/f'{wsname}_{var}_STL.nc'

    profile = dict(driver='COG', nodata=-9999.0, dtype=rio.float32, crs='EPSG:4326', count=1)
    nc = Dataset(input_file_nc)
    width = nc.width; height=nc.height
    transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
    profile['transform'] = transform
    profile['width'] = width; profile['height'] = height
    profile['compress'] = 'DEFLATE'
    
    years = nc['YEAR'][:]
    for yi in range(nc.dimensions['season'].size):
        year = yi + 2000
        for p in params:
            print(f'{year} - {p}')
            data = nc[p][:,:,yi].filled(profile['nodata'])
            with rio.open(output_folder/f'{wsname}_{ver}_{p}_{year}.tif','w',**profile) as dst:
                dst.write(data, 1)
#%%
def mean_slope_params(wsname='Ale', ver='v8'):
    '''
    wsname = 'Ale'
    ver = 'v8'
    '''

    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\maps_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)

    for var in ['ndvi_nn', 'ndvi_bl', 'ndvi_cb']:
        print(var)
        input_file_nc = input_folder/f'{wsname}_{var}_STL.nc'

        nc = Dataset(input_file_nc)
        width = nc.width; height=nc.height
        transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
        profile['transform'] = transform
        profile['width'] = width; profile['height'] = height
        

        #data = np.zeros((height, width), dtype=np.float32)
        years = nc['YEAR'][:]
        for p in params:
            # p = 'LOS'
            print(p)
            #data[:] = profile['nodata']
            data = nc[p][:]

            outdata = data.mean(axis=2).filled(profile['nodata']).astype(np.float32)
            with rio.open(output_folder/f'{wsname}_{ver}_{var}_mean_{p}.tif', 'w', **profile) as dst:
                dst.write(outdata, 1)

            outdata=agg_slope(years, data).filled(profile['nodata']).astype(np.float32)
            with rio.open(output_folder/f'{wsname}_{ver}_{var}_slope_{p}.tif', 'w', **profile) as dst:
                dst.write(outdata, 1)        

    nc.close()

def mean_slope_params_modis(wsname='Ale'):
    # wsname='Ale'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\maps_modis')
    output_folder.mkdir(parents=True, exist_ok=True)

    input_file_nc = input_folder/f'modis_timesat_{wsname}_tpa.nc'
    nc = Dataset(input_file_nc)
    width = nc.width; height=nc.height
    transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
    profile['transform'] = transform
    profile['width'] = width; profile['height'] = height
    profile['crs'] = proj4_modis

    years = nc['YEAR'][:]
    for p in params:
        # p = 'LOS'
        print(p)
        #data[:] = profile['nodata']
        data = nc[p][:]

        # outdata = data.mean(axis=2).filled(profile['nodata']).astype(np.float32)
        # with rio.open(output_folder/f'{wsname}_modis_mean_{p}.tif', 'w', **profile) as dst:
        #     dst.write(outdata, 1)

        outdata=agg_slope(years, data).filled(profile['nodata']).astype(np.float32)
        with rio.open(output_folder/f'{wsname}_modis_slope_{p}.tif', 'w', **profile) as dst:
            dst.write(outdata, 1)        

    nc.close()



 #%%
 #sns.boxplot(x=year_ws.flatten(), y=data_ws.flatten()); plt.show()

#%%


if __name__=='__main__':
    pass
    #mean_slope_params(wsname='Sekela',ver='v8')
    #seasonal_params_boxplots(wsname='Sekela', ver='v8')
    
    #mean_slope_params_modis('Ale')
    #mean_slope_params_modis('Sekela')

    maps_all('Ale', 'v8')
    maps_all('Sekela', 'v8')