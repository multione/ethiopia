#%%
from os import replace
from netCDF4 import Dataset
from timesat_util import TimesatTTS
import numpy as np
from pathlib import Path

#%%
def create_copy_fusion_nc(input_file, output_file):

    with Dataset(input_file) as ncin, Dataset(output_file,'w', format='NETCDF4') as nc:
        # copy global attributes all at once via dictionary
        nc.setncatts(ncin.__dict__)
        # copy dimensions
        for name, dimension in ncin.dimensions.items():
            nc.createDimension(
                name, (len(dimension) if not dimension.isunlimited() else None))
        # copy all file data except for the excluded
        for name, variable in ncin.variables.items():
            if name in ['lon','lat','year','t16', 'objectid','modis_sids']:
                v = nc.createVariable(name, variable.datatype, variable.dimensions)
                # copy variable attributes all at once via dictionary
                nc[name].setncatts(ncin[name].__dict__)
                nc[name][:] = ncin[name][:]
                
        
        ndvi_nn = nc.createVariable("ndvi_nn", "i2", ("y", "x", "time"), zlib=True)
        ndvi_bl = nc.createVariable("ndvi_bl", "i2", ("y", "x", "time"), zlib=True)
        ndvi_cb = nc.createVariable("ndvi_cb", "i2", ("y", "x", "time"), zlib=True)

        nc.createVariable("weights_nn", "i2", ("y", "x", "time"), zlib=True)
        nc.createVariable("weights_bl", "i2", ("y", "x", "time"), zlib=True)
        nc.createVariable("weights_cb", "i2", ("y", "x", "time"), zlib=True)

        nc.createVariable("ndvi_score_nn", "i2", ("y", "x"), zlib=True)
        nc.createVariable("ndvi_score_bl", "i2", ("y", "x"), zlib=True)
        nc.createVariable("ndvi_score_cb", "i2", ("y", "x"), zlib=True)


#%%
def fusion_ndvi_v1():
    '''
    Fusion of modis and glad ndvi timeseries
    Simpliest method
    For every glad pixel, residuals to coresponding fitted modis pixel is calcuated.
    This residuals are then averaged over time axis and finaly corrected so that mean 
    of all averaged residuals in one modis pixel is 0.
    This is not good approach as there is siluethes of original modis pixel values in result.
    '''

    wsname='Ale'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_v1.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        

        tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        tts = TimesatTTS(tts_input_file)
        stl_fit_data = tts.decode_data()

        #nseries = stl_fit_data.shape[0]

        with Dataset(input_file,'r') as ncin:
            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            
        fusion_ndvi = np.empty_like(input_ndvi)

        modis_sids = nc['modis_sids'][:].data
        unique_modis_sids = np.unique(modis_sids)
        print(f'modis_sids: {len(unique_modis_sids)}')
        for i,modis_sid in enumerate(unique_modis_sids):            
            # i=34; modis_sid = np.unique(modis_sids)[i]
            #print(f'{i} ',end='')
            ind = modis_sids == modis_sid
            modis_ndvi = stl_fit_data[ind.flat]
            glad_ndvi = input_ndvi[ind]

            res_ndvi = (glad_ndvi - modis_ndvi).mean(axis=1)
            res_ndvi = (res_ndvi - res_ndvi.mean()).astype(np.int16)
            trans_ndvi = modis_ndvi + res_ndvi.reshape((-1,1))

            fusion_ndvi[ind] = trans_ndvi
                
        
        nc[f'ndvi_{interp}'][:] = fusion_ndvi
        print(' FINISHED.')

    nc.close()

def fusion_ndvi_v2():
    '''
    Fusion of modis and glad ndvi timeseries
    v1 + corrections for glad pixels are averaged on whole scene, not only on 
    coresponding modis pixel
    Idea for v3: use scipy.signal.savgol_filter smoothing to smooth residual timeserie
    '''

    wsname='Ale'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_v2.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        

        tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        tts = TimesatTTS(tts_input_file)
        stl_fit_data = tts.decode_data()

        #nseries = stl_fit_data.shape[0]

        with Dataset(input_file,'r') as ncin:
            # ncin = Dataset(input_file)
            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            qf = ncin['qf'][:]
            weight_ndvi = np.ones_like(qf, dtype=float)
            # qf==15 -> clear sky
            weight_ndvi[np.isin(qf,[11,12,13,14,16,17])] = 0.7  #clear-sky data with indication of cloud/shadow proyximity
            weight_ndvi[np.isin(qf,[5,6])] = 0.3  # easonal data quality issues (topographic shadows and snow cover)
            weight_ndvi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows

            
        #fusion_ndvi = np.empty_like(input_ndvi)

        #modis_sids = nc['modis_sids'][:].data
        #unique_modis_sids = np.unique(modis_sids)
        #print(f'modis_sids: {len(unique_modis_sids)}')
        #for i,modis_sid in enumerate(unique_modis_sids):            
            # modis_sid = np.unique(modis_sids)[0]
            #print(f'{i} ',end='')
        #ind = modis_sids == modis_sid
        modis_ndvi = stl_fit_data.reshape(input_ndvi.shape)
        glad_ndvi = input_ndvi

        modis_ndvi_min = np.expand_dims(modis_ndvi.min(axis=2), 2)
        modis_ndvi_max = np.expand_dims(modis_ndvi.max(axis=2),2)
        glad_ndvi_min = np.expand_dims(glad_ndvi.min(axis=2),2)
        glad_ndvi_max = np.expand_dims(glad_ndvi.max(axis=2),2)

        modis_ndvi = (modis_ndvi-modis_ndvi_min)/(modis_ndvi_max-modis_ndvi_min)
        glad_ndvi = (glad_ndvi-glad_ndvi_min)/(glad_ndvi_max-glad_ndvi_min)

        res_ndvi = np.average(glad_ndvi - modis_ndvi, axis=2, weights=weight_ndvi)
        res_ndvi = (res_ndvi - res_ndvi.mean()).astype(np.int16)
        trans_ndvi = modis_ndvi + np.expand_dims(res_ndvi, axis=2)

        # import scipy
        # from scipy.signal import savgol_filter
        # res = (glad_ndvi-modis_ndvi).reshape((-1,glad_ndvi.shape[2]))
        # resf = savgol_filter(res,5,2,deriv=1,axis=1)

        # import rasterio as rio
        # profile = dict(driver='GTiff', nodata=-9999.0, dtype=rio.int16, crs='EPSG:4326', count=1)
        # width = nc.width; height=nc.height
        # transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
        # profile['transform'] = transform
        # profile['width'] = width; profile['height'] = height
        # with rio.open(r'D:\DATA\ETHIOPIA\FUSION\test_tiff/res_ndvi.tif', 'w', **profile) as dst:
        #     dst.write(res_ndvi, 1)
        
        #fusion_ndvi[ind] = trans_ndvi
        
        
        nc[f'ndvi_{interp}'][:] = trans_ndvi
        print(' FINISHED.')

    nc.close()

def fusion_ndvi_v3():
    '''
    Fusion of modis and glad ndvi timeseries
    For every pixel make linear regression smothed modis ndvi + dist from season mid -> glad ndvi
    
    '''
    from sklearn import linear_model
            #from sklearn import ensemble
            #from sklearn.preprocessing import scale, RobustScaler
            #from sklearn.pipeline import make_pipeline
            #from sklearn.preprocessing import PolynomialFeatures


    wsname='Ale'
    ver = 'v3'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        tts = TimesatTTS(tts_input_file)
        stl_fit_data = tts.decode_data()
        
        #nseries = stl_fit_data.shape[0]

        with Dataset(input_file,'r') as ncin:
            # ncin = Dataset(input_file)
            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            qf = ncin['qf'][:]
            weight_ndvi = np.ones_like(qf, dtype=float)
            # qf==15 -> clear sky
            weight_ndvi[np.isin(qf,[11,12,13,14,16,17])] = 0.7  #clear-sky data with indication of cloud/shadow proyximity
            weight_ndvi[np.isin(qf,[5,6])] = 0.3  # easonal data quality issues (topographic shadows and snow cover)
            weight_ndvi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows

            jd = ncin['t16'][:]*16+7
            dist_ms = np.sin(jd/365*np.pi)

            width = ncin.width; height=ncin.height
           
        modis_ndvi = stl_fit_data.reshape((-1,input_ndvi.shape[2]))
        glad_ndvi = input_ndvi.astype(np.float32).filled(np.nan).reshape((-1,input_ndvi.shape[2]))
        weight_ndvi = weight_ndvi.filled(0).reshape((-1,input_ndvi.shape[2]))

        #import matplotlib.pyplot as plt
        #c=1545; plt.figure(figsize=(12,5)); plt.scatter(modis_ndvi[c,:], glad_ndvi[c,:]); plt.show()
        fusion_ndvi = np.empty_like(input_ndvi)
        fusion_ndvi_weights = np.empty_like(input_ndvi)
        for i in range(width*height):
            r = i//width; c=i%width
            if (i%1000==0):
                print(f'{i+1}/{width*height}')

            clf = linear_model.BayesianRidge()
            #clf = linear_model.LinearRegression(fit_intercept=True, copy_X=True)
            #clf = ensemble.RandomForestRegressor()
            #clf = make_pipeline(PolynomialFeatures(degree=2), linear_model.LinearRegression(),)
            #ind = ~glad_ndvi[c,:].mask
            X = np.c_[modis_ndvi[i,:], dist_ms, dist_ms**2, modis_ndvi[i,:]*dist_ms ] # , dist_ms**2, dist_ms*modis_ndvi[c,:]] #modis_ndvi[c,:].reshape(-1,1) #np.c_[modis_ndvi[c,:], dist_ms] #, dist_ms**2, dist_ms*modis_ndvi[c,:]]
            y = glad_ndvi[i,:]
            ind = np.isfinite(y)
            XX = X[ind,:]; yy = y[ind]
            clf = clf.fit(XX,yy, sample_weight=weight_ndvi[i,ind])
            Y, w = clf.predict(X, return_std=True)
            fusion_ndvi[r,c,:] = Y
            fusion_ndvi_weights = w

            #plt.figure(figsize=(12,5)); plt.plot(X[:,0],'k'); plt.plot(Y,'r'); plt.plot(y,'.'); plt.plot(w*10,'g--'); plt.show()
    #%%
            #plt.figure(figsize=(12,5)); plt.plot(modis_ndvi[c,:],'k'); plt.plot(Y,'r'); plt.plot(glad_ndvi[c,:],'.'); plt.show()       
        
        nc[f'ndvi_{interp}'][:] = fusion_ndvi.astype(np.int16)
        nc[f'weights_{interp}'][:] = fusion_ndvi_weights.astype(np.int16)
        print(' FINISHED.')

    nc.close()

def fusion_ndvi_v4():
    '''
    Fusion of modis and glad ndvi timeseries
    For every pixel make linear regression:  (smothed modis ndvi) + (original modis ndvi) -> glad ndvi
    Not good: original modis data have errors that propagate into final ndvi
    '''
    from sklearn import linear_model, metrics
            #from sklearn import ensemble
            #from sklearn.preprocessing import scale, RobustScaler
            #from sklearn.pipeline import make_pipeline
    from sklearn.preprocessing import PolynomialFeatures


    wsname='Ale'
    ver = 'v4'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        tts = TimesatTTS(tts_input_file)
        stl_fit_data = tts.decode_data()
        
        #nseries = stl_fit_data.shape[0]

        with Dataset(input_file,'r') as ncin:
            # ncin = Dataset(input_file)
            modis_ndvi_original = ncin[f'modis_ndvi_{interp}'][:]

            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            qf = ncin['qf'][:]
            weight_ndvi = np.ones_like(qf, dtype=float)
            # qf==15 -> clear sky
            weight_ndvi[np.isin(qf,[11,12,13,14,16,17])] = 0.7  #clear-sky data with indication of cloud/shadow proyximity
            weight_ndvi[np.isin(qf,[5,6])] = 0.3  # easonal data quality issues (topographic shadows and snow cover)
            weight_ndvi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows

            jd = ncin['t16'][:]*16+7
            dist_ms = np.sin(jd/365*np.pi)

            width = ncin.width; height=ncin.height
           
        modis_ndvi = stl_fit_data.reshape((-1,input_ndvi.shape[2]))
        modis_ndvi_original = modis_ndvi_original.astype('float32').filled(np.nan).reshape((-1,input_ndvi.shape[2]))
        glad_ndvi = input_ndvi.astype(np.float32).filled(np.nan).reshape((-1,input_ndvi.shape[2]))
        weight_ndvi = weight_ndvi.filled(0).reshape((-1,input_ndvi.shape[2]))

        #import matplotlib.pyplot as plt
        #c=1545; plt.figure(figsize=(12,5)); plt.scatter(modis_ndvi[c,:], glad_ndvi[c,:]); plt.show()
        fusion_ndvi = np.empty_like(input_ndvi)
        fusion_ndvi_weights = np.empty_like(input_ndvi)
        fusion_score = np.zeros((height,width))
        for i in range(width*height):
            # i= 1545
            r = i//width; c=i%width
            if (i%1000==0):
                print(f'{i+1}/{width*height}')


            clf = linear_model.BayesianRidge()
            #clf = linear_model.LinearRegression(fit_intercept=True, copy_X=True)
            #clf = ensemble.RandomForestRegressor()
            #clf = make_pipeline(PolynomialFeatures(degree=2), linear_model.LinearRegression(),)
            #ind = ~glad_ndvi[c,:].mask
            #X = PolynomialFeatures(degree=2).fit_transform(np.c_[modis_ndvi[i,:], modis_ndvi_original[i,:] ]) #, , dist_ms**2, dist_ms*modis_ndvi[c,:]] #modis_ndvi[c,:].reshape(-1,1) #np.c_[modis_ndvi[c,:], dist_ms] #, dist_ms**2, dist_ms*modis_ndvi[c,:]]
            X = np.c_[modis_ndvi[i,:], modis_ndvi_original[i,:] ]
            y = glad_ndvi[i,:]
            ind = np.isfinite(y)
            XX = X[ind,:]; yy = y[ind]
            clf = clf.fit(XX,yy, sample_weight=weight_ndvi[i,ind])
            Y, w = clf.predict(X, return_std=True)
            fusion_ndvi[r,c,:] = Y
            fusion_ndvi_weights[r,c,:] = w
            fusion_score[r,c] = metrics.r2_score(yy,clf.predict(XX))
            print(fusion_score[r,c])

            # import matplotlib.pyplot as plt
            plt.figure(figsize=(12,5)); plt.plot(X[:,0],'k'); plt.plot(X[:,1],'k--'), plt.plot(Y,'r'); plt.plot(y,'.');# plt.plot(w*10,'g--'); 
            plt.savefig(rf'D:\DATA\ETHIOPIA\FUSION\v4\timeseries_{i}.png',dpi=300); plt.show()
    #%%
            #plt.figure(figsize=(12,5)); plt.plot(modis_ndvi[c,:],'k'); plt.plot(Y,'r'); plt.plot(glad_ndvi[c,:],'.'); plt.show()

            #res_ndvi = np.average(glad_ndvi - modis_ndvi, axis=2, weights=weight_ndvi)
            #res_ndvi = (res_ndvi - res_ndvi.mean()).astype(np.int16)
            #trans_ndvi = modis_ndvi + np.expand_dims(res_ndvi, axis=2)

        # import scipy
        # from scipy.signal import savgol_filter
        # res = (glad_ndvi-modis_ndvi).reshape((-1,glad_ndvi.shape[2]))
        # resf = savgol_filter(res,5,2,deriv=1,axis=1)

        # import rasterio as rio
        # profile = dict(driver='GTiff', nodata=-9999.0, dtype=rio.int16, crs='EPSG:4326', count=1)
        # width = nc.width; height=nc.height
        # transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
        # profile['transform'] = transform
        # profile['width'] = width; profile['height'] = height
        # with rio.open(r'D:\DATA\ETHIOPIA\FUSION\test_tiff/res_ndvi.tif', 'w', **profile) as dst:
        #     dst.write(res_ndvi, 1)
        
        #fusion_ndvi[ind] = trans_ndvi
        
        
        nc[f'ndvi_{interp}'][:] = fusion_ndvi.astype(np.int16)
        nc[f'weights_{interp}'][:] = fusion_ndvi_weights.astype(np.int16)
        print(' FINISHED.')

    nc.close()

def fusion_ndvi_v5():
    '''
    Fusion of modis and glad ndvi timeseries
    For every pixel make linear regression smothed modis ndvi + dist from season mid -> glad ndvi
    
    '''
    from sklearn import linear_model
    from sklearn import metrics
            #from sklearn import ensemble
            #from sklearn.preprocessing import scale, RobustScaler
            #from sklearn.pipeline import make_pipeline
            #from sklearn.preprocessing import PolynomialFeatures


    wsname='Ale'
    ver = 'v5'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        tts = TimesatTTS(tts_input_file)
        stl_fit_data = tts.decode_data()
        
        #nseries = stl_fit_data.shape[0]

        input_tpa_nc = fr"D:\DATA\ETHIOPIA\FUSION\modis_timesat_{wsname}_{interp}.nc"
        with Dataset(input_tpa_nc) as ncin:
            #  ncin = Dataset(input_tpa_nc)
            tpa_year = ncin['YEAR'][:].data
            tpa_mos = ncin['MOS'][:].data

        with Dataset(input_file,'r') as ncin:
            # ncin = Dataset(input_file)
            width = ncin.width; height=ncin.height
            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            qf = ncin['qf'][:]
            weight_ndvi = np.ones_like(qf, dtype=float)
            # qf==15 -> clear sky
            weight_ndvi[np.isin(qf,[11,12,13,14,16,17])] = 0.7  #clear-sky data with indication of cloud/shadow proyximity
            weight_ndvi[np.isin(qf,[5,6])] = 0.3  # easonal data quality issues (topographic shadows and snow cover)
            weight_ndvi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows

            t16 = ncin['t16'][:]
            year= ncin['year'][:]

        doy = (t16*16+8.0).data
        year = year.data
        ddoy = 365*(year-2000)+doy
        # d1 = absolute distance from midseason scaled so begining and end of year is pi/2
        # s1 = 0.5*sin(d1)+0.5
        # s2 = cos
        #s1 = np.sin(jd/365*np.pi)            
           
        modis_ndvi = stl_fit_data.reshape((-1,input_ndvi.shape[2]))
        glad_ndvi = input_ndvi.astype(np.float32).filled(np.nan).reshape((-1,input_ndvi.shape[2]))
        weight_ndvi = weight_ndvi.filled(0).reshape((-1,input_ndvi.shape[2]))

        #def linreg()
        #import matplotlib.pyplot as plt
        #c=1545; plt.figure(figsize=(12,5)); plt.scatter(modis_ndvi[c,:], glad_ndvi[c,:]); plt.show()
        yli = np.zeros_like(year); yri = np.zeros_like(year)
        mosl = np.zeros_like(doy); mosr = np.zeros_like(doy)
        fusion_ndvi = np.empty_like(input_ndvi)
        fusion_ndvi_weights = np.empty_like(input_ndvi)
        fusion_score = np.zeros((height, width))
        for i in range(width*height):

            np.random.seed=42
            rri = np.random.choice(width*height,50, replace=False)
        for i in rri:
                #i=rri[1]
            r = i//width; c=i%width
            if (i%1000==0):
                print(f'{i+1}/{width*height}')

            
            #tpa_yi = tpa_year[r,c,:]
            # let's assume that tpa_yi is always np.arange(2000, 2021)            
            mos = tpa_mos[r,c,year-2000]
            yli[:] = yri[:] = year
            yli[doy<mos] = year[doy<mos]-1; yri[doy>mos] = year[doy>mos]+1
            indl = yli>=2000; indr = yri<=2020
            #yli[yli<indl] = 2000; yri[indr] = 2020
            
            mosl[indl] = tpa_mos[r,c,yli[indl]-2000] + 365*(yli[indl]-2000); mosl[~indl] = -365/2
            mosr[indr] = tpa_mos[r,c,yri[indr]-2000] + 365*(yri[indr]-2000); mosr[~indr] = 21*365+365/2
            
            d1 = 0.5+0.5*np.sin(2*np.pi*(ddoy-mosl)/(mosr-mosl)+np.pi/2)
            d2 = 0.5+0.5*np.cos(2*np.pi*(ddoy-mosl)/(mosr-mosl)+np.pi/2)
            plt.figure(figsize=(12,5)); plt.plot(d1[:100]); plt.plot(d2[:100]);plt.show()


            clf = linear_model.BayesianRidge()
            #clf = linear_model.LinearRegression(fit_intercept=True, copy_X=True)
            #clf = ensemble.RandomForestRegressor()
            #clf = make_pipeline(PolynomialFeatures(degree=2), linear_model.LinearRegression(),)
            #ind = ~glad_ndvi[c,:].mask
            X = np.c_[modis_ndvi[i,:]]#, d1, d1**2, d2, d2**2] # , dist_ms**2, dist_ms*modis_ndvi[c,:]] #modis_ndvi[c,:].reshape(-1,1) #np.c_[modis_ndvi[c,:], dist_ms] #, dist_ms**2, dist_ms*modis_ndvi[c,:]]
            y = glad_ndvi[i,:]
            ind = np.isfinite(y)# & (y>modis_ndvi[i,:].min())
            XX = X[ind,:]; yy = y[ind]
            clf = clf.fit(XX,yy, sample_weight=weight_ndvi[i,ind])
            Y, w = clf.predict(X, return_std=True)
            fusion_ndvi[r,c,:] = Y
            fusion_ndvi_weights = w
            fusion_score[r,c] = metrics.r2_score(yy,clf.predict(XX))
            print(fusion_score[r,c])

            plt.figure(figsize=(12,5)); plt.plot(X[:,0],'k'); plt.plot(Y,'r'); plt.plot(y,'.'); plt.plot(w*10,'g--'); plt.show()
            
    #%% 
            #plt.figure(figsize=(12,5)); plt.plot(modis_ndvi[c,:],'k'); plt.plot(Y,'r'); plt.plot(glad_ndvi[c,:],'.'); plt.show()       
        
        nc[f'ndvi_{interp}'][:] = fusion_ndvi.astype(np.int16)
        nc[f'weights_{interp}'][:] = fusion_ndvi_weights.astype(np.int16)
        print(' FINISHED.')

    nc.close()
# fr"D:\DATA\ETHIOPIA\FUSION\modis_timesat_{wsname}_{interp}.nc"

def fusion_ndvi_v6():
    '''
    Fusion of modis and glad ndvi timeseries
    For every pixel make linear regression smothed modis ndvi + dist from season mid -> glad ndvi
    
    '''
    from sklearn import linear_model
    from sklearn import metrics
            #from sklearn import ensemble
            #from sklearn.preprocessing import scale, RobustScaler
            #from sklearn.pipeline import make_pipeline
            #from sklearn.preprocessing import PolynomialFeatures


    wsname='Ale'
    ver = 'v6'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        tts = TimesatTTS(tts_input_file)
        stl_fit_data = tts.decode_data()
        
        #nseries = stl_fit_data.shape[0]

        input_tpa_nc = fr"D:\DATA\ETHIOPIA\FUSION\modis_timesat_{wsname}_{interp}.nc"
        with Dataset(input_tpa_nc) as ncin:
            #  ncin = Dataset(input_tpa_nc)
            tpa_year = ncin['YEAR'][:].data
            tpa_mos = ncin['MOS'][:].data

        with Dataset(input_file,'r') as ncin:
            # ncin = Dataset(input_file)
            width = ncin.width; height=ncin.height
            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            qf = ncin['qf'][:]
            weight_ndvi = np.ones_like(qf, dtype=float)
            # qf==15 -> clear sky
            weight_ndvi[np.isin(qf,[11,12,13,14,16,17])] = 0.5  #clear-sky data with indication of cloud/shadow proyximity
            weight_ndvi[np.isin(qf,[5,6])] = 0  # seasonal data quality issues (topographic shadows and snow cover)
            weight_ndvi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows

            t16 = ncin['t16'][:]
            year= ncin['year'][:]
            nt = t16.shape[0]

        doy = (t16*16+8.0).data
        year = year.data
        ddoy = 365*(year-2000)+doy
        # d1 = absolute distance from midseason scaled so begining and end of year is pi/2
        # s1 = 0.5*sin(d1)+0.5
        # s2 = cos
        #s1 = np.sin(jd/365*np.pi)            
           
        modis_ndvi = stl_fit_data.reshape((-1,input_ndvi.shape[2]))
        glad_ndvi = input_ndvi.astype(np.float32).filled(np.nan).reshape((-1,input_ndvi.shape[2]))
        weight_ndvi = weight_ndvi.filled(0).reshape((-1,input_ndvi.shape[2]))

        mos = tpa_mos[:,:,year-2000]
        yli = np.zeros_like(doy,dtype=int); yri = np.zeros_like(doy,dtype=int)
        mosl = np.zeros_like(mos); mosr = np.zeros_like(mos)

        for c in range(width):
            for r in range(height):
                mosrc = mos[r,c,:]
                yli[:] = yri[:] = year
                yli[doy<mosrc] = year[doy<mosrc]-1; yri[doy>mosrc] = year[doy>mosrc]+1
                indl = yli>=2000; indr = yri<=2020
                mosl[r,c,indl] = tpa_mos[r,c,yli[indl]-2000] + 365*(yli[indl]-2000); mosl[r,c,~indl] = -365/2
                mosr[r,c,indr] = tpa_mos[r,c,yri[indr]-2000] + 365*(yri[indr]-2000); mosr[r,c,~indr] = 21*365+365/2        

        d1 = 0.5+0.5*np.sin(2*np.pi*(ddoy-mosl)/(mosr-mosl)+np.pi/2)
        d2 = 0.5+0.5*np.cos(2*np.pi*(ddoy-mosl)/(mosr-mosl)+np.pi/2)
        d1[np.isnan(d1)] = 0.5
        d2[np.isnan(d2)] = 0.5

        clf = linear_model.BayesianRidge()

        X = np.c_[modis_ndvi.flatten()]
        y = glad_ndvi.flatten()

        ind = np.isfinite(y)# & (y>modis_ndvi[i,:].min())
        XX = X[ind,:]; yy = y[ind]
        clf = clf.fit(XX,yy, sample_weight=weight_ndvi.flatten()[ind])
        Y, w = clf.predict(X, return_std=True)

        i=rri[0]; r = i//width; c=i%width
        XP = X.reshape(height,width,nt,-1)
        YP = Y.reshape(height,width,nt)
        
        plt.figure(figsize=(12,5)); plt.plot(XP[r,c,:,0],'k'); plt.plot(YP[r,c,:],'r'); plt.plot(glad_ndvi[i,:],'.'); plt.show()
            


            
        fusion_ndvi = np.empty_like(input_ndvi)
        fusion_ndvi_weights = np.empty_like(input_ndvi)
        fusion_score = np.zeros((height, width))
        for i in range(width*height):

            np.random.seed=42
            rri = np.random.choice(width*height,50, replace=False)
        for i in rri:
                #i=rri[1]
            r = i//width; c=i%width
            if (i%1000==0):
                print(f'{i+1}/{width*height}')

            
            #tpa_yi = tpa_year[r,c,:]
            # let's assume that tpa_yi is always np.arange(2000, 2021)            
            
            
            #yli[yli<indl] = 2000; yri[indr] = 2020
            

            
            
            plt.figure(figsize=(12,5)); plt.plot(d1[:100]); plt.plot(d2[:100]);plt.show()


            clf = linear_model.BayesianRidge()
            #clf = linear_model.LinearRegression(fit_intercept=True, copy_X=True)
            #clf = ensemble.RandomForestRegressor()
            #clf = make_pipeline(PolynomialFeatures(degree=2), linear_model.LinearRegression(),)
            #ind = ~glad_ndvi[c,:].mask
            X = np.c_[modis_ndvi[i,:]]#, d1, d1**2, d2, d2**2] # , dist_ms**2, dist_ms*modis_ndvi[c,:]] #modis_ndvi[c,:].reshape(-1,1) #np.c_[modis_ndvi[c,:], dist_ms] #, dist_ms**2, dist_ms*modis_ndvi[c,:]]
            y = glad_ndvi[i,:]
            ind = np.isfinite(y)# & (y>modis_ndvi[i,:].min())
            XX = X[ind,:]; yy = y[ind]
            clf = clf.fit(XX,yy, sample_weight=weight_ndvi[i,ind])
            Y, w = clf.predict(X, return_std=True)
            fusion_ndvi[r,c,:] = Y
            fusion_ndvi_weights = w
            fusion_score[r,c] = metrics.r2_score(yy,clf.predict(XX))
            print(fusion_score[r,c])

            plt.figure(figsize=(12,5)); plt.plot(X[:,0],'k'); plt.plot(Y,'r'); plt.plot(y,'.'); plt.plot(w*10,'g--'); plt.show()
            
    #%% 
            #plt.figure(figsize=(12,5)); plt.plot(modis_ndvi[c,:],'k'); plt.plot(Y,'r'); plt.plot(glad_ndvi[c,:],'.'); plt.show()       
        
        nc[f'ndvi_{interp}'][:] = fusion_ndvi.astype(np.int16)
        nc[f'weights_{interp}'][:] = fusion_ndvi_weights.astype(np.int16)
        print(' FINISHED.')

    nc.close()


def fusion_ndvi_v7():
    '''
    Fusion of modis and glad ndvi timeseries
    For every pixel make linear regression from smothed modis ndvi 
    #Residuals smooth with galinsky filtering # NOT
    
    '''
    from sklearn import linear_model
    from sklearn import metrics
    from utils import SpecialLinReg, savitzky_golay_filtering
    from scipy.signal import savgol_filter
    from matplotlib import pyplot as plt


    wsname='Ale'
    ver = 'v7'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        tts = TimesatTTS(tts_input_file)
        stl_fit_data = tts.decode_data()
        
        with Dataset(input_file,'r') as ncin:
            # ncin = Dataset(input_file)
            width = ncin.width; height=ncin.height
            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            qf = ncin['qf'][:]
            weight_ndvi = np.ones_like(qf, dtype=float)
            # qf==15 -> clear sky
            weight_ndvi[np.isin(qf,[11,12,13,14,16,17])] = 0.5  #clear-sky data with indication of cloud/shadow proyximity
            weight_ndvi[np.isin(qf,[5,6])] = 0  # seasonal data quality issues (topographic shadows and snow cover)
            weight_ndvi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows          
            nt = input_ndvi.shape[2]
            
            
           
        modis_ndvi = stl_fit_data.reshape((-1,input_ndvi.shape[2]))
        glad_ndvi = input_ndvi.astype(np.float32).filled(np.nan).reshape((-1,input_ndvi.shape[2]))
        weight_ndvi = weight_ndvi.filled(0).reshape((-1,input_ndvi.shape[2]))

        # some unrealistic NDVI-s
        #ind = glad_ndvi<5000
        #glad_ndvi[ind] = np.nan

        
        clf = SpecialLinReg()
        res = np.zeros(nt)
        fusion_ndvi = np.empty_like(input_ndvi)
        fusion_ndvi_weights = np.empty_like(input_ndvi)
        fusion_score = np.zeros((height, width))
        for i in range(width*height):

            # np.random.seed=42
            # rri = np.random.choice(width*height,10, replace=False)
            # for i in rri:
                    #i=rri[1]
            r = i//width; c=i%width
            if (i%1000==0):
                print(f'{i+1}/{width*height}')            

            y = glad_ndvi[i,:]

            modis_ndvi_i =modis_ndvi[i,:]
            modis_ndvi_i_min = modis_ndvi_i.min()
            modis_ndvi_i_max = modis_ndvi_i.max()

            y[(y<modis_ndvi_i_min*0.9) | (y>modis_ndvi_i_max*1.1)] = np.nan
            X = np.c_[modis_ndvi_i]#, d1, d1**2, d2, d2**2] # , dist_ms**2, dist_ms*modis_ndvi[c,:]] #modis_ndvi[c,:].reshape(-1,1) #np.c_[modis_ndvi[c,:], dist_ms] #, dist_ms**2, dist_ms*modis_ndvi[c,:]]
            
            ind = np.isfinite(y)# & (y>modis_ndvi[i,:].min())            
            XX = X[ind,:]; yy = y[ind]

            clf.fit(XX,yy, sample_weight=weight_ndvi[i,ind])
            
            Y = clf.predict(X)
            fusion_ndvi[r,c,:] = Y
            
            prd = clf.predict(XX)
            #res[:] = np.nan
            #res[ind] = yy - prd            
            fusion_score[r,c] = metrics.r2_score(yy,yy-prd)
            #print(fusion_score[r,c])

            #sres = savitzky_golay_filtering(res, wnds=[23*5, 23*3], orders=[2,4], debug=False)
            #plt.close('all')
            #plt.figure(figsize=(12,5)); plt.plot(X[:,0],'k'); plt.plot(Y,'r'); plt.plot(sres+X[:,0].mean(),'.'); plt.plot(sres+Y,'g--'); plt.plot(y,'k.'); #plt.show()
            #plt.savefig(fr'D:\DATA\ETHIOPIA\FUSION\{ver}\sg_{interp}_{i}.png',dpi=200,bbox_inches='tight')

    #%% 
            #plt.figure(figsize=(12,5)); plt.plot(modis_ndvi[c,:],'k'); plt.plot(Y,'r'); plt.plot(glad_ndvi[c,:],'.'); plt.show()       
        
        nc[f'ndvi_{interp}'][:] = fusion_ndvi.astype(np.int16)
        #nc[f'weights_{interp}'][:] = fusion_ndvi_weights.astype(np.int16)
        nc[f'ndvi_score_{interp}'][:] = fusion_score
        print(' FINISHED.')

    nc.close()

def fusion_ndvi_v8(wsname):
    '''
    Fusion of modis and glad ndvi timeseries
    Modis is now interpolated after TIMESAT smoothing
    For every pixel make linear regression from smothed modis ndvi 
    #Residuals smooth with galinsky filtering # NOT
    
    '''
    from sklearn import linear_model
    from sklearn import metrics
    from utils import SpecialLinReg, savitzky_golay_filtering
    from scipy.signal import savgol_filter
    from matplotlib import pyplot as plt


    # wsname='Ale'
    ver = 'v8'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    input_modis = fr'D:\DATA\ETHIOPIA\FUSION\modis_timesat_{wsname}_raw.nc'
    output_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    for interp in ['nn','bl','cb']:
        # interp = 'cb'        
        print(interp)

        #tts_input_file = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STLfit.tts"
        #tts = TimesatTTS(tts_input_file)
        #stl_fit_data = tts.decode_data()
        
        with Dataset(input_file,'r') as ncin:
            # ncin = Dataset(input_file)
            width = ncin.width; height=ncin.height
            input_ndvi = ncin['ndvi'][:].data
            input_ndvi = np.ma.masked_less_equal(input_ndvi, -5000)
            qf = ncin['qf'][:]
            weight_ndvi = np.ones_like(qf, dtype=float)
            # qf==15 -> clear sky
            weight_ndvi[np.isin(qf,[11,12,13,14,16,17])] = 0.5  #clear-sky data with indication of cloud/shadow proyximity
            weight_ndvi[np.isin(qf,[5,6])] = 0  # seasonal data quality issues (topographic shadows and snow cover)
            weight_ndvi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows          
            nt = input_ndvi.shape[2]
            
            
        with Dataset(input_modis) as ncmodis:
            # ncmodis = Dataset(input_modis)
            modis_ndvi = ncmodis[f'modis_ndvi_{interp}'][:].reshape((-1,nt))

        glad_ndvi = input_ndvi.astype(np.float32).filled(np.nan).reshape((-1,input_ndvi.shape[2]))
        weight_ndvi = weight_ndvi.filled(0).reshape((-1,input_ndvi.shape[2]))

        # some unrealistic NDVI-s
        #ind = glad_ndvi<5000
        #glad_ndvi[ind] = np.nan

        
        clf = SpecialLinReg()
        res = np.zeros(nt)
        fusion_ndvi = np.empty_like(input_ndvi)
        fusion_ndvi_weights = np.empty_like(input_ndvi)
        fusion_score = np.zeros((height, width))
        for i in range(width*height):

            # np.random.seed=42
            # rri = np.random.choice(width*height,10, replace=False)
            # for i in rri:
                    #i=rri[1]
            r = i//width; c=i%width
            if (i%1000==0):
                print(f'{i+1}/{width*height}')            

            y = glad_ndvi[i,:]

            modis_ndvi_i =modis_ndvi[i,:]
            modis_ndvi_i_min = modis_ndvi_i.min()
            modis_ndvi_i_max = modis_ndvi_i.max()

            y[(y<modis_ndvi_i_min*0.9) | (y>modis_ndvi_i_max*1.1)] = np.nan
            X = np.c_[modis_ndvi_i]#, d1, d1**2, d2, d2**2] # , dist_ms**2, dist_ms*modis_ndvi[c,:]] #modis_ndvi[c,:].reshape(-1,1) #np.c_[modis_ndvi[c,:], dist_ms] #, dist_ms**2, dist_ms*modis_ndvi[c,:]]
            
            ind = np.isfinite(y)# & (y>modis_ndvi[i,:].min())            
            XX = X[ind,:]; yy = y[ind]

            clf.fit(XX,yy, sample_weight=weight_ndvi[i,ind])
            
            Y = clf.predict(X)
            fusion_ndvi[r,c,:] = Y
            
            prd = clf.predict(XX)
            #res[:] = np.nan
            #res[ind] = yy - prd            
            fusion_score[r,c] = metrics.r2_score(yy,prd)
            #print(fusion_score[r,c])

            #sres = savitzky_golay_filtering(res, wnds=[23*5, 23*3], orders=[2,4], debug=False)
            #plt.close('all')
            #plt.figure(figsize=(12,5)); plt.plot(X[:,0],'k'); plt.plot(Y,'r'); plt.plot(sres+X[:,0].mean(),'.'); plt.plot(sres+Y,'g--'); plt.plot(y,'k.'); plt.show()
            #plt.savefig(fr'D:\DATA\ETHIOPIA\FUSION\{ver}\sg_{interp}_{i}.png',dpi=200,bbox_inches='tight')

    #%% 
            #plt.figure(figsize=(12,5)); plt.plot(modis_ndvi[c,:],'k'); plt.plot(Y,'r'); plt.plot(glad_ndvi[c,:],'.'); plt.show()       
        
        nc[f'ndvi_{interp}'][:] = fusion_ndvi.astype(np.int16)
        #nc[f'weights_{interp}'][:] = fusion_ndvi_weights.astype(np.int16)
        nc[f'ndvi_score_{interp}'][:] = fusion_score
        print(' FINISHED.')

    nc.close()

def test_glad_ndvi():
    import rasterio as rio
    import rasterio.transform
    from datetime import datetime, timedelta

    wsname = 'Ale'
    input_file = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_fld = Path(r'D:\DATA\ETHIOPIA\FUSION\tiffs')

    nc = Dataset(input_file)
    
    ncyear = nc['year'][:]
    nct16 = nc['t16'][:]
    dates = np.array([f'{datetime(y,1,1)+timedelta(days=int(t)*16):%Y%m%d}' for y,t in zip(ncyear, nct16)])

    transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
    crs = nc.crs
    profile =dict(driver='GTiff', count=1, width=nc.width, height=nc.height, crs=crs, transform=transform, dtype='int16')     
    

    years=[2020, 2010, 2000]

    ndvi = nc[f'ndvi'][:]

    for y in years:
        # y= 2020
        print('\t',y)

        ind = ncyear==y
        ydates = dates[ind]
        yndvi = ndvi[:,:,ind]

        for i in range(ind.sum()):
            # i=0
            data = yndvi[:,:,i]
            with rio.open(output_fld/f'{wsname}_ndvi_glad_{ydates[i]}.tif', 'w',**profile) as dst:
                dst.write(data, 1)

    nc.close()


def test_fusion_ndvi(ver):
    import rasterio as rio
    import rasterio.transform
    from datetime import datetime, timedelta

    wsname = 'Ale'
    input_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'
    output_fld = Path(fr'D:\DATA\ETHIOPIA\FUSION\{ver}\tiffs')
    

    nc = Dataset(input_file)
    
    ncyear = nc['year'][:]
    nct16 = nc['t16'][:]
    dates = np.array([f'{datetime(y,1,1)+timedelta(days=int(t)*16):%Y%m%d}' for y,t in zip(ncyear, nct16)])

    transform = rio.transform.Affine.from_gdal(*nc.transform_gdal)
    crs = nc.crs
    profile =dict(driver='GTiff', count=1, width=nc.width, height=nc.height, crs=crs, transform=transform, dtype='int16')     
    

    years=[2020, 2010, 2000]
    for interp in ['nn','bl','cb']:
        # interp='nn'
        print(interp)
        ndvi = nc[f'ndvi_{interp}'][:]

        for y in years:
            # y= 2020
            print('\t',y)

            ind = ncyear==y
            ydates = dates[ind]
            yndvi = ndvi[:,:,ind]

            for i in range(ind.sum()):
                # i=0
                data = yndvi[:,:,i]
                with rio.open(output_fld/f'{wsname}_ndvi_{interp}_{ver}_{ydates[i]}.tif', 'w',**profile) as dst:
                    dst.write(data, 1)

    nc.close()



  
if __name__=='__main__':
    #fusion_ndvi_v1()
    #fusion_ndvi_v2()
    #fusion_ndvi_v3()
    #test_fusion_ndvi('v7')
    #test_glad_ndvi()
    #fusion_ndvi_v7()
    fusion_ndvi_v8('Sekela')