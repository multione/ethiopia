#%%
from pathlib import Path
from timesat_util import Timesat_Tpa
import rasterio
from rasterio.features import rasterize
import geopandas
import pandas
import numpy as np
import pickle
from netCDF4 import Dataset
from datetime import datetime, timedelta

from utils import params, create_copy_fusion_nc

#%%

def fusion2timesat(wsname='Ale', ver='v7'):
    #wsname='Ale'
    #ver='v2'
    input_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'
    output_folder =  Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)

    nc= Dataset(input_file)

    year = nc['year'][:].data
    t16 = nc['t16'][:].data
    times = [datetime(y,1,1) + timedelta(days=int(t)*16) for y,t in zip(year,t16)]
    timess = [f"{d:%Y%m%d}" for d in times]

    ny = nc.dimensions['y'].size
    nx = nc.dimensions['x'].size
    nt = len(timess)
    nt16 = 23
    nyears = len(np.unique(year))

    
    for var in ['ndvi_nn', 'ndvi_bl', 'ndvi_cb']:
        print(var)
        all_values = nc[var][:].data

        fid = open(output_folder/f"tsinput_{wsname}_{var}.txt", 'wt')
        fid.write(f'{nyears+2} {nt16} {ny*nx}\n')
        for iy in range(ny):
            print(f'{iy}/{ny}')
            for ix in range(nx):
                values = all_values[iy, ix, :] #nc[var][iy,ix,:].data
                values = np.r_[values[:nt16],values,values[-nt16:]] #dodajemo prvu i zadnju godinu
                fid.write(' '.join(values.astype(str)))
                fid.write('\n')
        fid.close()
    nc.close()


def timesat2nc(wsname = 'Ale', ver = 'v7'):
    '''
    wsname = 'Ale'
    ver = 'v7'
    '''
    ns = 21
    input_folder =  Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')

    for var in ['ndvi_nn', 'ndvi_bl', 'ndvi_cb']:
        # var = 'ndvi_cb'
        print(wsname, ver, var)
        input_file_tpa = input_folder/f'{wsname}_{var}_STL.tpa'
        input_file_nc = rf'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'
        output_file_nc = Path(input_file_tpa).with_suffix('.nc')

        create_copy_fusion_nc(input_file_nc, output_file_nc, ns)
        tpa=Timesat_Tpa(input_file_tpa)
        tpa.decode2nc(output_file_nc)

        #make_nc_from_tpa(input_file_tpa, input_file_nc, output_file_nc)



# def make_nc_from_tpa(input_file_tpa, input_file_nc, output_file_nc):
#     # input_file_tpa = r'D:/DATA/ETHIOPIA/FUSION/timesat_v2/Ale_ndvi_cb_STL.tpa'
#     # input_file_nc = r'D:\DATA\ETHIOPIA\FUSION\fusion_Ale_v2.nc'
#     # output_file_nc = Path(input_file_tpa).with_suffix('.nc')
#     from timesat_util import Timesat_Tpa
#     tpa = Timesat_Tpa(input_file_tpa)

#     first_year=2000
#     season_offset = 1
#     season_stride = 1
#     ns = (tpa.nyears-season_offset)//season_stride-season_offset
#     ndpt = 365//tpa.nptperyear+1 
    
#     create_copy_fusion_nc(input_file_nc, output_file_nc, ns)

#     nc = Dataset(output_file_nc, 'a')
#     #nc.first_year=first_year
#     width=nc.width
#     height=nc.height
#     nseason = nc.dimensions['season'].size

#     ind_season = np.zeros((tpa.nseries, tpa.nyears), dtype=bool)
#     for i, nsi in enumerate(tpa.nseasons):
#         # i= 0; nsi=tpa.nseasons[i]        
#         ind_season[i, season_offset:season_offset+nsi-season_offset]=True
    
#     #y = (tpa.rows-1)//nc.width
#     #x = (tpa.rows-1)%nc.width       

#     for ip, p in enumerate(params):
#         print(p)
#         # ip=0; p=params[ip]
#      #   out_data = np.zeros((height,width,nseason))
#         data = tpa.data[ind_season,ip].reshape((height,width,-1))
#         if p=='SOS':
#             season = data//tpa.nptperyear - season_offset
#             year = first_year + season
#             nc['YEAR'][:] = year.reshape(height,width,nseason)
        
#         if p in ['SOS','EOS','MOS']:
#             data = data - (season_offset+season)*tpa.nptperyear + 8
#         if p=='LOS':
#             data = data*ndpt

#     #    out_data[y,x,:]=data.reshape(height,width,)
#         nc[p][:] = data.reshape(height,width,nseason)

#     nc.close()
            

def fusion_ndvi_tiff(wsname, ver='v8'):
#%%
    '''
    wsname='Ale'
    ver='v8'
    '''
    input_file = fr'D:\DATA\ETHIOPIA\FUSION\fusion_{wsname}_{ver}.nc'
    output_folder =  Path(fr'D:\DATA\ETHIOPIA\FUSION\ndvi_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)
        
    profile = dict(driver='COG', nodata=-9999.0, dtype=rasterio.float32, crs='EPSG:4326', count=1)

    # nc = Dataset(input_file)
    with Dataset(input_file) as nc:
        year = nc['year'][:].data
        ny = nc.dimensions['y'].size
        nx = nc.dimensions['x'].size
        nt = nc.dimensions['time'].size
        transform = rasterio.transform.Affine.from_gdal(*nc.transform_gdal)
        ndvi = nc['ndvi_cb'][:]
        t16 = nc['t16'][:]
        date = [datetime(year[i],1,1) + timedelta(days=int(t16[i])*16+8) for i in range(nt)]
        datestr = np.array([f'{d:%Y%m}' for d in date])
    
    profile['width'] = nx
    profile['height'] = ny
    profile['transform']=transform
    all_months = np.unique(datestr)
    for m in datestr:
        # y=2000
        ind = datestr == m
        ndvi_year = (ndvi[:,:,ind].mean(axis=2)/10000).astype(np.float32).filled(-9999)
        with rasterio.open(output_folder/f'n{wsname}_ndvi30m_{m}.tif', 'w', **profile) as dst:
            dst.write(ndvi_year,1)



if __name__=='__main__':
    #fusion2timesat('Sekela', 'v8')
    #timesat2nc(wsname = 'Ale', ver = 'v8')
    #timesat2nc(wsname = 'Sekela', ver = 'v8')
    fusion_ndvi_tiff('Ale')
    fusion_ndvi_tiff('Sekela')