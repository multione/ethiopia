#%%
import pandas, pickle, geopandas
from pathlib import Path
import numpy as np
import rasterio as rio
from netCDF4 import Dataset
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import scale, robust_scale, StandardScaler
import seaborn as sns
import matplotlib.pyplot as plt
import scipy

params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

lc_legend = np.array([
    'No data', 
    'Trees cover areas',
    'Shrubs cover areas',
    'Grassland',
    'Cropland',
    'Vegetation aquatic or regularly flooded',
    'Lichen Mosses / Sparse vegetation',
    'Bare areas',
    'Built up areas',
    'Permanent snow and/or Ice',
    'Open water'])

#%%
def bare_soil_stats():
#%%
    wsnames = ['Ale','Sekela']
    input_data = r'D:\DATA\ETHIOPIA\SLMP_MWS_shapefiles.xlsx'
    input_folder = Path(fr'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\DATA')
    input_folder_lc = Path('D:\DATA\ETHIOPIA\LC')
    input_folder_nc = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_v8')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FC')
    output_folder.mkdir(parents=True, exist_ok=True)

    dfd = pandas.read_excel(input_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','mws_nm','SLMP_MWS_shapefiles.imp_yr']]
    dfd.columns = ['objectid','donor','z_name','w_name','mws_name','int_year']
    dfd = dfd.dropna()    
    dfd['int_year'] = dfd['int_year'].astype(int)    

    nparams = len(params)
    out=[]
    model = LinearRegression()
#%%
    for wsname in wsnames: 
        # wsname = wsnames[0]
        with rio.open(input_folder_lc/f'lc_30m_{wsname}.tif') as src:
            lc = src.read(1)

        files = list((input_folder/f'bare_soil_{wsname}/{wsname}_bare_soil_mask').glob('*.tif'))
        #C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\DATA\bare_soil_Ale\ale_bare_soil_frac
        files = sorted(files, key=lambda x: x.name)
        bsall = np.zeros(lc.shape+(12*len(files),))
        years = np.arange(2001,2021,1./12)
        for i, f in enumerate(files):
            # f= files[0]
            with rio.open(f) as src:
                bsm = src.read()
            bsall[:,:,i*12:(i+1)*12] = np.moveaxis(bsm, 0, -1)
            

        input_file_nc = input_folder_nc/f'{wsname}_ndvi_cb_STL.nc'
        with Dataset(input_file_nc) as nc:
            nc_objectids = nc['objectid'][:]

        df = dfd.query(f"w_name=='{wsname}'")

        for iind, oid, mwsname, int_year in df[['objectid','mws_name','int_year']].itertuples():
            print (oid, mwsname)

            ind = nc_objectids==oid
            lc_oid = lc[ind]

            #year_oid = nc['YEAR'][:][ind]

            for lc_pix in np.unique(lc_oid):
                # lc_pix = np.unique(lc_oid)[0]
                lcname = lc_legend[lc_pix]

                ind_lc = lc_oid==lc_pix                
                npixels = ind_lc.sum()

                res=dict(wsname=wsname, mwsname=mwsname, lcname=lcname, n=npixels)

                pdata = bsall[ind][ind_lc].sum(axis=0)/npixels*100

                ind_year = years<int_year

                x = years[ind_year].reshape(-1,1)
                y = pdata[ind_year].reshape(-1,1)
                model.fit(x,y)
                slope1 = model.coef_[0][0]

                x = years[~ind_year].reshape(-1,1)
                y = pdata[~ind_year].reshape(-1,1)
                model.fit(x,y)
                slope2 = model.coef_[0][0]

                res['BS_BEF']=slope1
                res['BS_AFT']=slope2
                res['DIF']=slope2-slope1

                out.append(res)
    
    dfr = pandas.DataFrame(out)
    dfr.to_excel(output_folder/'stats_baresoil_slopes_30m.xlsx')


def seasonal_params_stats(ver):
    '''
    wsname='Ale'
    ver='v8'    
    '''
    
#%%
    wsnames = ['Ale','Sekela']
    input_data = 'https://docs.google.com/spreadsheets/d/1k020YkZRfRDWuYwBYMSDBzJwoDl6TA3d/export?format=csv&gid=1319881855'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    input_folder_lc = Path('D:\DATA\ETHIOPIA\LC')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\stats_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)
    
    dfd = pandas.read_csv(input_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','mws_nm','SLMP_MWS_shapefiles.imp_yr']]
    dfd.columns = ['objectid','donor','z_name','w_name','mws_name','int_year']
    dfd = dfd.dropna()    
    dfd['int_year'] = dfd['int_year'].astype(int)    

    var = 'ndvi_cb'
    nparams = len(params)
    out=[]
    model = LinearRegression()

    for wsname in wsnames:                
        # wsname = wsnames[0]

        #LC
        with rio.open(input_folder_lc/f'lc_30m_{wsname}.tif') as src:
            lc = src.read(1)

        input_file_nc = input_folder/f'{wsname}_{var}_STL.nc'
        nc = Dataset(input_file_nc)
        nc_objectids = nc['objectid'][:]
        #years = nc['YEAR'][:]

        data={}
        for p in params:
            data[p] = nc[p][:]

        df = dfd.query(f"w_name=='{wsname}'")

        #dfg = df.groupby(['donor','z_name','w_name', 'lc_name'])
        #keys = list(dfg.groups.keys())

        for iind, oid, donor, mwsname, int_year in df[['objectid','donor','mws_name','int_year']].itertuples():
            print (oid, mwsname)

            ind = nc_objectids==oid
            lc_oid = lc[ind]

            year_oid = nc['YEAR'][:][ind]

            for lc_pix in np.unique(lc_oid):
                # lc_pix = np.unique(lc_oid)[0]
                lcname = lc_legend[lc_pix]

                ind_lc = lc_oid==lc_pix                
                npixels = ind_lc.sum()

                #slope1 = np.full((npixels,nparams),np.nan)
                #slope2 = np.full_like(slope1, np.nan)

                year_lc = year_oid[ind_lc].filled(np.nan)
                
                ind_year = year_lc < int_year
                #res=dict(donor=donor, wsname=wsname, mwsname=mwsname, lcname=lcname, n=npixels)

                slope1 = np.full((npixels,nparams),np.nan)
                slope2 = np.full_like(slope1, np.nan)
                for ip,p in enumerate(params):
                    pdata = StandardScaler().fit_transform(data[p][ind][ind_lc].filled(np.nan).reshape(-1,1)) #scale(data[p][ind][ind_lc].filled(np.nan))
                    
                    x = year_lc[ind_year].reshape(-1,1)
                    y = pdata[ind_year.reshape(-1,1)]
                    model.fit(x,y) 
                    slope1 = model.coef_[0]

                    x = year_lc[~ind_year].reshape(-1,1)
                    y = pdata[(~ind_year).reshape(-1,1)]
                    model.fit(x,y) 
                    slope2 = model.coef_[0]

                    #diff = slope2-slope1
                    #ws,ps = scipy.stats.wilcoxon(diff)

                    #res[f'{p}_DIF'] = np.nanmean(diff)
                    #res[f'{p}_p'] = ps

                    res[f'{p}_BEF']=slope1
                    res[f'{p}_AFT']=slope2
                    res[f'{p}_DIF']=diff

                out.append(res)

    dfr = pandas.DataFrame(out)
    dfr.to_excel(output_folder/'stats_slopes_30m.xlsx')

def seasonal_params_stats_perpixel(ver):
    '''
    # Treba prepraviti kod da radi slope na svaki pixel posebno
    # i onda iz razlike računati značajnost razlike 
    wsname='Ale'
    ver='v8'    
    '''
    
#%%
    wsnames = ['Ale','Sekela']
    input_data = 'https://docs.google.com/spreadsheets/d/1k020YkZRfRDWuYwBYMSDBzJwoDl6TA3d/export?format=csv&gid=1319881855'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    input_folder_lc = Path('D:\DATA\ETHIOPIA\LC')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\stats_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)
    
    dfd = pandas.read_csv(input_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','mws_nm','SLMP_MWS_shapefiles.imp_yr']]
    dfd.columns = ['objectid','donor','z_name','w_name','mws_name','int_year']
    dfd = dfd.dropna()    
    dfd['int_year'] = dfd['int_year'].astype(int)    

    var = 'ndvi_cb'
    nparams = len(params)
    out=[]
    model = LinearRegression()
    for wsname in wsnames:                
        # wsname = wsnames[0]

        #LC
        with rio.open(input_folder_lc/f'lc_30m_{wsname}.tif') as src:
            lc = src.read(1)

        input_file_nc = input_folder/f'{wsname}_{var}_STL.nc'
        nc = Dataset(input_file_nc)
        nc_objectids = nc['objectid'][:]
        #years = nc['YEAR'][:]

        data={}
        for p in params:
            data[p] = nc[p][:]

        df = dfd.query(f"w_name=='{wsname}'")


        for iind, oid, mwsname, int_year in df[['objectid','mws_name','int_year']].itertuples():
            print (oid, mwsname)

            ind = nc_objectids==oid
            lc_oid = lc[ind]

            year_oid = nc['YEAR'][:][ind]

            for lc_pix in np.unique(lc_oid):
                # lc_pix = np.unique(lc_oid)[0]
                lcname = lc_legend[lc_pix]

                ind_lc = lc_oid==lc_pix                
                npixels = ind_lc.sum()

                #slope1 = np.full((npixels,nparams),np.nan)
                #slope2 = np.full_like(slope1, np.nan)

                year_lc = year_oid[ind_lc].filled(np.nan)
                
                ind_year = year_lc < int_year
                res=dict(wsname=wsname, mwsname=mwsname, lcname=lcname, n=npixels)
                for ip,p in enumerate(params):
                    pdata = StandardScaler().fit_transform(data[p][ind][ind_lc].filled(np.nan).reshape(-1,1)) #scale(data[p][ind][ind_lc].filled(np.nan))
                    
                    x = year_lc[ind_year].reshape(-1,1)
                    y = pdata[ind_year.reshape(-1,1)]
                    model.fit(x,y) 
                    slope1 = model.coef_[0]

                    x = year_lc[~ind_year].reshape(-1,1)
                    y = pdata[(~ind_year).reshape(-1,1)]
                    model.fit(x,y) 
                    slope2 = model.coef_[0]

                    #diff = slope2-slope1
                    #ws,ps = scipy.stats.wilcoxon(diff)

                    #res[f'{p}_DIF'] = np.nanmean(diff)
                    #res[f'{p}_p'] = ps

                    res[f'{p}_BEF']=slope1
                    res[f'{p}_AFT']=slope2
                    res[f'{p}_DIF']=diff

                out.append(res)

    dfr = pandas.DataFrame(out)
    dfr.to_excel(output_folder/'stats_slopes_30m.xlsx')

if __name__ == '__main__':
    seasonal_params_stats('v8')
    #bare_soil_stats()