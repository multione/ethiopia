#%%
import pandas, pickle, geopandas
from pathlib import Path
import numpy as np
import rasterio as rio
from netCDF4 import Dataset
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import scale, robust_scale, StandardScaler
import seaborn as sns
import matplotlib.pyplot as plt

params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

lc_legend = np.array([
    'No data', 
    'Trees cover areas',
    'Shrubs cover areas',
    'Grassland',
    'Cropland',
    'Vegetation aquatic or regularly flooded',
    'Lichen Mosses / Sparse vegetation',
    'Bare areas',
    'Built up areas',
    'Permanent snow and/or Ice',
    'Open water'])

#%%
def agg_slope_boxplot(year_ws, data_ws):
    #x = years.flatten().reshape(-1,1)
    # samo pixeli sa svim sezonama
    #dd = data[ind,:]
    #yy = years[ind,:]
    indy = (data_ws.mask.sum(axis=1)==0)

    #x = year_ws.data[indy,:].mean(axis=0).reshape(-1,1).astype(np.float32) #.flatten()
    #y = robust_scale(data_ws.data[indy,:], unit_variance=True).T * 100
    x = year_ws.data[indy,:].reshape(-1,1)
    y = data_ws.data[indy,:].flatten()
    #slope = np.ma.masked_all((indy.shape[0],2), dtype=np.float32) #np.zeros(ind.shape[0],dtype=np.float32)
    
    model = LinearRegression()
    
    indx = (x<2012).flatten()
    model.fit(x[indx],y[indx])
    slope1 = model.coef_[0]
    inter1 = model.intercept_

    indx = (x>=2012).flatten()
    model.fit(x[indx],y[indx])
    slope2 = model.coef_[0]
    inter2 = model.intercept_

    return slope1,inter1,slope2,inter2

#%%
def seasonal_params_modis_boxplot(wsname='Sekela'):
    
    input_shapefile = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\boxplot_modis')
    output_folder.mkdir(parents=True, exist_ok=True)

    shp = geopandas.read_file(input_shapefile)
    shp = shp.query(f"W_NAME=='{wsname}'")    
    df = shp[['OBJECTID','mws_nm']]
    df.columns=['objectid','name']
    wslu = df.set_index('objectid').to_dict()['name']
    objectids = df.objectid.values 

    input_file_nc = input_folder/f'modis_timesat_{wsname}_tpa.nc'
    nc = Dataset(input_file_nc)
    nc_objectids = nc['objectid'][:]
    years = nc['YEAR'][:]

    bp_data={}
    for p in params:
        # p = params[0]
        data = nc[p][:]
        if p in ['BLV','LEF','SAM','LSI','SSI','SSV','ESV']:
            data = data/10000.0
        for oid in objectids:                
            # oid = objectids[0]
            ind = nc_objectids==oid
            data_ws = data[ind,:]
            year_ws = years[ind,::]
            bp_data[oid,p] = {'linreg':agg_slope_boxplot(year_ws, data_ws), 'data':data_ws, 'year':year_ws}
        
    for oid in objectids:
        wssname = wslu[oid]
        print(wssname)

        plt.close('all')
        fig, axs = plt.subplots(13,1, figsize=(10,30), sharex=True, constrained_layout=True)
        for ax, p in zip(axs, params):
                d = bp_data[oid,p]
                data_ws, year_ws = d['data'], d['year']
                sns.boxplot(x=year_ws.flatten(), y=data_ws.flatten(), ax=ax).set(xlabel='', ylabel=p)            
                ax.grid(axis='y', linestyle='--')

                slope1,inter1,slope2,inter2 = d['linreg']
                lx1=1999.5; ly1 = inter1+slope1*lx1
                lx2=2011.5; ly2 = inter1+slope1*lx2
                ax.plot([lx1-2000, lx2-2000],[ly1,ly2], 'r--', lw=2)

                lx1=2011.5; ly1 = inter2+slope2*lx1
                lx2=2020.5; ly2 = inter2+slope2*lx2
                ax.plot([lx1-2000, lx2-2000],[ly1,ly2], 'r--', lw=2)
                
        #fig.suptitle(f'OBJECTID: {oid}, SLOPE: {slp:04d}', fontsize=16)
        ax.set_xlabel('YEAR')
        fig.suptitle(f'{wssname}', fontsize=16)
        #plt.show()
        plt.savefig(output_folder/f'{wsname}_{wssname}_{oid:04d}_modis_boxplot.png', dpi=200, bbox_inches='tight', facecolor='w')

def seasonal_params_boxplots_lc(ver='v8'):
    '''
    wsname='Ale'
    ver='v8'
    '''
    wsnames = ['Ale','Sekela']
    input_data = 'https://docs.google.com/spreadsheets/d/1k020YkZRfRDWuYwBYMSDBzJwoDl6TA3d/export?format=csv&gid=1319881855'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    input_folder_lc = Path('D:\DATA\ETHIOPIA\LC')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\boxplot_lc_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)
    
    dfd = pandas.read_csv(input_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','mws_nm','SLMP_MWS_shapefiles.imp_yr']]
    dfd.columns = ['objectid','donor','z_name','w_name','mws_name','int_year']
    dfd = dfd.dropna()    
    dfd['int_year'] = dfd['int_year'].astype(int)    

    var = 'ndvi_cb'
    nparams = len(params)
    out=[]
    model = LinearRegression()
    for wsname in wsnames:                
        # wsname = wsnames[0]

        #LC
        with rio.open(input_folder_lc/f'lc_30m_{wsname}.tif') as src:
            lc = src.read(1)

        input_file_nc = input_folder/f'{wsname}_{var}_STL.nc'
        nc = Dataset(input_file_nc)
        nc_objectids = nc['objectid'][:]
        years = nc['YEAR'][:]

        data={}
        for p in params:
            data[p] = nc[p][:]
            

        df = dfd.query(f"w_name=='{wsname}'")

        for iind, oid, mwsname, int_year in df[['objectid','mws_name','int_year']].itertuples():
            print (oid, mwsname)

            ind = nc_objectids==oid
            lc_oid = lc[ind]

            year_oid = nc['YEAR'][:][ind]

            for lc_pix in np.unique(lc_oid):
                # lc_pix = np.unique(lc_oid)[0]
                lcname = lc_legend[lc_pix]

                ind_lc = lc_oid==lc_pix                
                npixels = ind_lc.sum()

                plt.close('all')
                fig, axs = plt.subplots(13,1, figsize=(10,30), sharex=True, constrained_layout=True)

                for ax, p in zip(axs, params):
                    # ax=axs[0]; p=params[0]
                    data_ws = data[p][ind][ind_lc].filled(np.nan)
                    year_ws = years[ind][ind_lc].filled(np.nan)
                    sns.boxplot(x=year_ws.flatten(), y=data_ws.flatten(), ax=ax).set(xlabel='', ylabel=p)            
                    ax.grid(axis='y', linestyle='--')

                    ind_year = year_ws < int_year

                    x = year_ws[ind_year].reshape(-1,1)
                    y = data_ws[ind_year].reshape(-1,1)
                    model.fit(x,y) 
                    slope1 = model.coef_[0]
                    inter1 = model.intercept_

                    x = year_ws[~ind_year].reshape(-1,1)
                    y = data_ws[~ind_year].reshape(-1,1)
                    model.fit(x,y) 
                    slope2 = model.coef_[0]
                    inter2 = model.intercept_

                    lx1=1999.5; ly1 = inter1+slope1*lx1
                    lx2=int_year; ly2 = inter1+slope1*lx2
                    ax.plot([lx1-2000, lx2-2000],[ly1,ly2], 'r-', lw=2)

                    lx1=int_year; ly1 = inter2+slope2*lx1
                    lx2=2020.5; ly2 = inter2+slope2*lx2
                    ax.plot([lx1-2000, lx2-2000],[ly1,ly2], 'r-', lw=2)

                ax.set_xlabel('YEAR')
                fig.suptitle(f'{mwsname}, {lcname}', fontsize=16)
                #plt.show()
                plt.savefig(output_folder/f'{wsname}_{mwsname}_{oid:04d}_lc{lc_pix}_boxplot.png', dpi=200, bbox_inches='tight')


                

#%%
def seasonal_params_boxplots(wsname='Ale', ver='v8'):
    '''
    wsname='Ale'
    ver='v8'
    '''
    input_shapefile = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\boxplot_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)
    
    shp = geopandas.read_file(input_shapefile)
    shp = shp.query(f"W_NAME=='{wsname}'")    

    df = shp[['OBJECTID','mws_nm']]
    df.columns=['objectid','name']
    wslu = df.set_index('objectid').to_dict()['name']

    objectids = df.objectid.values    

    #for var in ['ndvi_cb']:
    var = 'ndvi_cb'
    print(var)

    input_file_nc = input_folder/f'{wsname}_{var}_STL.nc'
    nc = Dataset(input_file_nc)
    nc_objectids = nc['objectid'][:]
    years = nc['YEAR'][:]

    bp_data={}
    for p in params:
        # p = params[0]
        data = nc[p][:]
        if p in ['BLV','LEF','SAM','LSI','SSI','SSV','ESV']:
            data = data/10000.0
        for oid in objectids:                
            # oid = objectids[0]
            ind = nc_objectids==oid
            data_ws = data[ind,:]
            year_ws = years[ind,::]
            bp_data[oid,p] = {'linreg':agg_slope_boxplot(year_ws, data_ws), 'data':data_ws, 'year':year_ws}
        

    for oid in objectids:
        wssname = wslu[oid]
        print(wssname)

        plt.close('all')
        fig, axs = plt.subplots(13,1, figsize=(10,30), sharex=True, constrained_layout=True)
        for ax, p in zip(axs, params):
                d = bp_data[oid,p]
                data_ws, year_ws = d['data'], d['year']
                sns.boxplot(x=year_ws.flatten(), y=data_ws.flatten(), ax=ax).set(xlabel='', ylabel=p)            
                ax.grid(axis='y', linestyle='--')

                slope1,inter1,slope2,inter2 = d['linreg']
                lx1=1999.5; ly1 = inter1+slope1*lx1
                lx2=2011.5; ly2 = inter1+slope1*lx2
                ax.plot([lx1-2000, lx2-2000],[ly1,ly2], 'r--', lw=2)

                lx1=2011.5; ly1 = inter2+slope2*lx1
                lx2=2020.5; ly2 = inter2+slope2*lx2
                ax.plot([lx1-2000, lx2-2000],[ly1,ly2], 'r--', lw=2)
                
        #fig.suptitle(f'OBJECTID: {oid}, SLOPE: {slp:04d}', fontsize=16)
        ax.set_xlabel('YEAR')
        fig.suptitle(f'{wssname}', fontsize=16)
        #plt.show()
        plt.savefig(output_folder/f'{wsname}_{wssname}_{oid:04d}_boxplot.png', dpi=200, bbox_inches='tight')


def seasonal_params_modis_shape(wsname='Ale'):
    input_shapefile = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\shapes_modis')
    output_folder.mkdir(parents=True, exist_ok=True)

    shp = geopandas.read_file(input_shapefile)
    shp = shp.query(f"W_NAME=='{wsname}'")    
    df = shp[['OBJECTID','mws_nm']]
    df.columns=['objectid','name']
    wslu = df.set_index('objectid').to_dict()['name']
    objectids = df.objectid.values 

    input_file_nc = input_folder/f'modis_timesat_{wsname}_tpa.nc'
    nc = Dataset(input_file_nc)
    nc_objectids = nc['objectid'][:]
    years = nc['YEAR'][:]

    bp_data={}
    for p in params:
        # p = params[0]
        data = nc[p][:]
        if p in ['BLV','LEF','SAM','LSI','SSI','SSV','ESV']:
            data = data/10000.0
        for oid in objectids:                
            # oid = objectids[0]
            ind = nc_objectids==oid
            data_ws = data[ind,:]
            year_ws = years[ind,::]
            bp_data[oid,p] = {'linreg':agg_slope_boxplot(year_ws, data_ws), 'mean':data_ws.flatten().mean()}
    nc.close()

    dff=pandas.DataFrame({'objectids':objectids})
    for p in params:
        dff[f'{p}_AVG'] = [bp_data[oid,p]['mean'] for oid in objectids]
        dff[f'{p}_SLP1'] = [bp_data[oid,p]['linreg'][0]  for oid in objectids]
        dff[f'{p}_SLP2'] = [bp_data[oid,p]['linreg'][2]  for oid in objectids]
        dff[f'{p}_SLPD'] = dff[f'{p}_SLP2']-dff[f'{p}_SLP1']
    dff=dff.set_index('objectids')

    shp_out = shp.join(dff,on='OBJECTID')

    shp_out.to_file(output_folder/f'{wsname}_avgslp.shp')

#%%
def seasonal_params_shape(wsname='Ale', ver='v8'):
    '''
    wsname='Ale'
    ver='v8'
    '''
    input_shapefile = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    input_folder = Path(fr'D:\DATA\ETHIOPIA\FUSION\timesat_{ver}')
    output_folder= Path(fr'D:\DATA\ETHIOPIA\FUSION\shapes_{ver}')
    output_folder.mkdir(parents=True, exist_ok=True)
    
    shp = geopandas.read_file(input_shapefile)
    shp = shp.query(f"W_NAME=='{wsname}'") 
    shp = shp[list(shp.columns[:17])+['geometry']]
    objectids = shp.OBJECTID.values   

    df = shp[['OBJECTID','mws_nm']]
    df.columns=['objectid','name']
    wslu = df.set_index('objectid').to_dict()['name']

    #for var in ['ndvi_cb']:
    var = 'ndvi_cb'
    print(var)

    input_file_nc = input_folder/f'{wsname}_{var}_STL.nc'
    nc = Dataset(input_file_nc)
    nc_objectids = nc['objectid'][:]
    years = nc['YEAR'][:]
    

    bp_data={}
    for p in params:
        # p = params[0]
        data = nc[p][:]
        if p in ['BLV','LEF','SAM','LSI','SSI','SSV','ESV']:
            data = data/10000.0
        for oid in objectids:                
            # oid = objectids[0]
            ind = nc_objectids==oid
            data_ws = data[ind,:]
            year_ws = years[ind,::]
            bp_data[oid,p] = {'linreg':agg_slope_boxplot(year_ws, data_ws), 'mean':data_ws.flatten().mean()}
    nc.close()

    dff=pandas.DataFrame({'objectids':objectids})
    for p in params:
        dff[f'{p}_AVG'] = [bp_data[oid,p]['mean'] for oid in objectids]
        dff[f'{p}_SLP1'] = [bp_data[oid,p]['linreg'][0]  for oid in objectids]
        dff[f'{p}_SLP2'] = [bp_data[oid,p]['linreg'][2]  for oid in objectids]
        dff[f'{p}_SLPD'] = dff[f'{p}_SLP2']-dff[f'{p}_SLP1']
    dff=dff.set_index('objectids')

    shp_out = shp.join(dff,on='OBJECTID')

    shp_out.to_file(output_folder/f'{wsname}_{ver}_avgslp.shp')
    


        
#%%
if __name__=='__main__':
    #mean_slope_params(wsname='Sekela',ver='v8')
    #seasonal_params_boxplots(wsname='Sekela', ver='v8')
    #seasonal_params_shape('Sekela','v8')
    #seasonal_params_modis_boxplot('Ale')

    #seasonal_params_modis_shape('Ale')
    #seasonal_params_modis_shape('Sekela')
    seasonal_params_boxplots_lc('v8')