#%%
from pathlib import Path

glad_tiles = ['037E_11N', '037E_10N', '035E_08N']
fld = Path(r'D:\DATA\ETHIOPIA\GLAD')
modis_data = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs.pickle')

#%%
tile = glad_tiles[-1]
for year in range(2000, 2021):
    print (year)
    for t16 in range(23):
        print(f'\t{t16+1} ', end='', flush='True')

        filename = f'glad_{tile}_{year}_{t16:02d}.tif'
#%%
# Crtanje modis i pripadajućih glad pixela

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt

nc = Dataset(r'D:\DATA\ETHIOPIA\GLAD\glad_Ale.nc')

nx = nc.dimensions['x'].size
ny = nc.dimensions['y'].size
glad_int = nc['glad_interval'][:]
msids = nc['modis_sids'][:].data
ndvi = nc['ndvi'][:, :, :].data     #fill_value=-32767
modis_ndvi = nc['modis_ndvi_nn'][:,:,:].data

#%%
rsid = np.random.choice(msids.reshape(-1))

ind = msids==rsid
rind, cind = ind.nonzero()

rndvi = ndvi[rind, cind, :]
rmndvi = modis_ndvi[rind, cind, :]

#%%
fig = plt.figure(figsize=(15,5))
plt.plot(glad_int,rmndvi[0,:])

tind = rndvi[0,:]>-9999
plt.plot(glad_int[tind],rndvi[0,tind],'r.')

tind = rndvi[1,:]>-9999
plt.plot(glad_int[tind],rndvi[1,tind],'g.')

# %%
