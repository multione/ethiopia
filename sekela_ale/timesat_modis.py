#%%
from pathlib import Path

from timesat_util import TimesatTTS
import rasterio as rio
from rasterio.features import rasterize
import geopandas
import pandas
import numpy as np
from timesat_util import Timesat_Input, Timesat_Tpa
import pickle
from netCDF4 import Dataset
from datetime import datetime, timedelta
import itertools, pyproj
from scipy.interpolate import griddata

from utils import params, create_copy_fusion_nc
#%%

def prepare_timesat_raw(wsname='Ale'):
    '''
    Timesat from original modis NDVI
    '''
    '''
    wsname = 'Ale'
    '''
    fld_modis = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_ale_sekela')
    input_file = f'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_folder = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ale_sekela')

    with Dataset(input_file) as nc:
        year = nc['year'][:].data
        t16 = nc['t16'][:].data
        #times = [datetime(y,1,1) + timedelta(days=int(t)*16) for y,t in zip(year,t16)]
        #timess = [f"{d:%Y%m%d}" for d in times]
        nyears = len(np.unique(year))
        nt16 = 23

    ntimes = len(year)
    modis_ndvi = None
    for i in range(ntimes):
        date = datetime(year[i],1,1) + timedelta(days=16*int(t16[i]))
        if date<datetime(2000,2,18):
            date =datetime(2001,1,1) + timedelta(days=16*int(t16[i]))

        modis_fn = fld_modis/f'modis_{wsname}_{date:%Y%m%d}.tif'
        print (modis_fn)
        with rio.open(modis_fn) as src:

            if modis_ndvi is None:
                height = src.profile['height']
                width = src.profile['width']
                modis_ndvi = np.zeros((height, width, ntimes), dtype=np.int16)
                modis_qa = np.zeros_like(modis_ndvi)

            modis_data = src.read(1)   
            modis_ndvi[:,:,i] = modis_data

            modis_data_qa = src.read(3)
            modis_qa[:,:,i] = modis_data_qa

    input_data = dict(modis_raw = modis_ndvi, modis_raw_qa=modis_qa)
    for var in ['modis_raw','modis_raw_qa']:
        # var = 'modis_raw'
        print(var)
        data = input_data[var]
        fid = open(output_folder/f"{wsname}_tsinput_{var}.txt", 'wt')        
        fid.write(f'{nyears+2} {nt16} {width*height}\n')
        for iy in range(height):
            print(f'{iy}/{height}')
            for ix in range(width):
                values = data[iy, ix, :] #nc[var][iy,ix,:].data
                values = np.r_[values[:nt16],values,values[-nt16:]] #dodajemo prvu i zadnju godinu
                fid.write(' '.join(values.astype(str)))            
                fid.write('\n')
                
        fid.close()    




def prepare_timesat_interpolated():
    '''
    Timesat from interpolated MODIS
    '''
#%%
    wsname = 'Ale'
    input_file = f'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_folder = Path(r'D:\DATA\ETHIOPIA\TIMESAT\ale_sekela')

    nc = Dataset(input_file)

    objectid = nc['objectid'][:,:]
    year = nc['year'][:].data
    t16 = nc['t16'][:].data
    times = [datetime(y,1,1) + timedelta(days=int(t)*16) for y,t in zip(year,t16)]
    timess = [f"{d:%Y%m%d}" for d in times]

    ny = nc.dimensions['y'].size
    nx = nc.dimensions['x'].size
    nt = len(timess)
    nt16 = 23
    nyears = len(np.unique(year))

    for var in ['modis_ndvi_nn', 'modis_qa', 'modis_ndvi_bl', 'modis_ndvi_cb']:
        print(var)
        all_values = nc[var][:].data
        fid = open(output_folder/f"tsinput_{var}.txt", 'wt')
        fid.write(f'{nyears+2} {nt16} {ny*nx}\n')
        for iy in range(ny):
            print(f'{iy}/{ny}')
            for ix in range(nx):
                values = all_values[iy, ix, :] #nc[var][iy,ix,:].data
                values = np.r_[values[:nt16],values,values[-nt16:]] #dodajemo prvu i zadnju godinu
                fid.write(' '.join(values.astype(str)))
                #fid.write(' '.join(map(str,values)))
                #fid.write(np.array_str(values, max_line_width=100000)[1:-1])
                fid.write('\n')
        fid.close()
    nc.close()

def timesat2nc():
    from timesat_util import Timesat_Tpa

    interp='cb'
    wsname='Ale'
    ns=21

    input_file_tpa = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_{interp}_STL.tpa"
    input_file_nc = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_file_nc = fr"D:\DATA\ETHIOPIA\FUSION\modis_timesat_{wsname}_{interp}.nc"

    create_copy_fusion_nc(input_file_nc, output_file_nc, ns)

    tpa=Timesat_Tpa(input_file_tpa)
    tpa.decode2nc(output_file_nc)

def timesat_modis_tpa_to_nc(wsname):
    # wsname='Sekela'
    from timesat_util import Timesat_Tpa
    import rasterio.features

    proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

    input_shapefile = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    input_file_tpa = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\{wsname}_modis_ndvi_raw_STL.tpa"
    fld_modis = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_ale_sekela')
    output_file_nc = fr'D:\DATA\ETHIOPIA\FUSION\modis_timesat_{wsname}_tpa.nc'

    ns=21

    fn_modis=next(fld_modis.glob(f'*_{wsname}_*.tif'))
    with rio.open(fn_modis) as src:
        width_raw = src.profile['width']
        height_raw = src.profile['height']
        transform_raw = src.profile['transform']
        

    nc = Dataset(output_file_nc,'w',format='NETCDF4')
    nc.width=width_raw
    nc.height=height_raw
    nc.crs = proj4_modis
    nc.transform_gdal = transform_raw.to_gdal()

    x = nc.createDimension("x", width_raw)
    y = nc.createDimension("y", height_raw)
    season = nc.createDimension("season", ns)
    
    objectid = nc.createVariable("objectid", "i4", ("y", "x"), zlib=True)

    for p in params:
        nc.createVariable(p, 'f4', ('y','x','season'))
    nc.createVariable('YEAR','i2', ('y','x','season'))
    nc.close()

    # OBJECTIDS
    with Dataset(output_file_nc,'a') as nc:
        shp = geopandas.read_file(input_shapefile)
        shp = shp.query(f"W_NAME=='{wsname}'")  
        shp = shp.to_crs(proj4_modis)
        shapes = zip(shp.geometry, shp['OBJECTID'])
        objectids = rio.features.rasterize(shapes, 
                            out_shape=(height_raw, width_raw), 
                            fill=0, 
                            transform=transform_raw, 
                            dtype=rio.int16)
        nc['objectid'][:] = objectids

    tpa=Timesat_Tpa(input_file_tpa)
    tpa.decode2nc(output_file_nc)

def timesat_modis_raw_to_nc(wsname='Ale'):
    
    fld_modis = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_ale_sekela')
    input_file_tts = fr"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_raw_STLfit.tts"
    input_file_nc = fr'D:\DATA\ETHIOPIA\GLAD\glad_{wsname}.nc'
    output_file_nc = fr'D:\DATA\ETHIOPIA\FUSION\modis_timesat_{wsname}_raw.nc'
    ntpy = 23
    ns = 21

    proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

    fn_modis=next(fld_modis.glob(f'*_{wsname}_*.tif'))
    with rio.open(fn_modis) as src:
        width_raw = src.profile['width']
        height_raw = src.profile['height']
        transform_raw = src.profile['transform']

    tts = TimesatTTS(input_file_tts)
    tts_data = tts.decode_data()
    nt = tts_data.shape[1]

    modis_raw_ndvi = np.zeros((height_raw*width_raw,nt)) + np.nan

    modis_raw_ndvi[tts.rows-1,:] = tts_data
    modis_raw_ndvi = modis_raw_ndvi.reshape((height_raw,width_raw,nt))

    rows, cols= zip(*itertools.product(range(height_raw), range(width_raw)))
    modis_x, modis_y = rio.transform.xy(transform_raw, rows, cols)
    modis_xy = np.c_[modis_x, modis_y]

    create_copy_fusion_nc(input_file_nc, output_file_nc, ns, seasonal_params=False)
    nc = Dataset(output_file_nc,'a')

    glad_lon = nc['lon'][:].data
    glad_lat = nc['lat'][:].data
    crs_glad = pyproj.crs.CRS.from_string(nc.crs)
    crs_modis = pyproj.crs.CRS.from_string(proj4_modis)
    tr = pyproj.Transformer.from_crs(crs_glad, crs_modis, always_xy=True)
    glad_x, glad_y = tr.transform(glad_lon, glad_lat)
    glad_xy = np.c_[glad_x.reshape(-1), glad_y.reshape(-1)]

    
    modis_qa = nc['modis_qa']
    modis_sids = nc['modis_sids']

    spatial_ids = np.arange(height_raw * width_raw).reshape((height_raw,width_raw))
    modis_sids[:,:] = griddata(modis_xy, spatial_ids.reshape(-1), glad_xy, 'nearest', -1).reshape((nc.height,nc.width))

    modis_ndvi_nn = np.full((nc.height,nc.width,nt),np.nan,dtype=np.float32)
    modis_ndvi_bl = np.full((nc.height,nc.width,nt),np.nan,dtype=np.float32)
    modis_ndvi_cb = np.full((nc.height,nc.width,nt),np.nan,dtype=np.float32) 

    for i in range(modis_raw_ndvi.shape[2]):
        # i=0
        print(i)
        mrn = modis_raw_ndvi[:,:,i]
        ind = np.isfinite(mrn)
        modis_xy_i = modis_xy[ind.reshape(-1),:]
        modis_ndvi_nn[:,:,i] = griddata(modis_xy_i, mrn[ind], glad_xy, 'nearest').reshape(nc.height,nc.width)
        modis_ndvi_bl[:,:,i] = griddata(modis_xy_i, mrn[ind], glad_xy, 'linear').reshape(nc.height,nc.width)
        modis_ndvi_cb[:,:,i] = griddata(modis_xy_i, mrn[ind], glad_xy, 'cubic').reshape(nc.height,nc.width)
        
    nc['modis_ndvi_nn'][:] = modis_ndvi_nn
    nc['modis_ndvi_bl'][:] = modis_ndvi_bl
    nc['modis_ndvi_cb'][:] = modis_ndvi_cb

    nc.close()
    '''
    import matplotlib.pyplot as plt
    plt.imshow(modis_ndvi_cb[:,:,100]); plt.show()
    '''
    

def read_tts_test():
    input_file = r"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_cb_STLfit.tts"
    tts = TimesatTTS(input_file)
    stl_fit_data = tts.decode_data()

    input_file = r"D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\modis_ndvi_cb_STLtrend.tts"
    tts = TimesatTTS(input_file)
    stl_trend_data = tts.decode_data()

    import matplotlib.pyplot as plt

    nc = Dataset(f'D:\DATA\ETHIOPIA\GLAD\glad_Ale.nc')
    width = nc.width
    modis_sids = nc['modis_sids'][:].data
    ndvi = nc['ndvi'][:].data
    ndvi = np.ma.masked_less_equal(ndvi, -5000)
#%%
    # scipy.signal.savgol_filter
    
    nseries = stl_fit_data.shape[0]
    r = np.random.choice(nseries)

    ix = r % width
    iy = r// width

    modis_sid = modis_sids[iy,ix]
    ind = modis_sids == modis_sid

    modis_ndvi = stl_fit_data[ind.flat]

    glad_ndvi = ndvi[ind]
    glad_ndvi_m = glad_ndvi.mean(axis=1)
    glad_ndvi_d = glad_ndvi.std(axis=1)

    '''
    glad_ndvi_t = (glad_ndvi-modis_ndvi)/glad_ndvi_d.reshape((-1,1))*modis_ndvi.std() + modis_ndvi.mean()
    glad_res_t = (glad_ndvi_t - modis_ndvi).mean(axis=1)
    glad_res_t = glad_res_t - glad_res_t.mean()
    glad_ndvi_t = glad_res_t.reshape((-1,1)) + modis_ndvi
    '''
    glad_res_t = (glad_ndvi - modis_ndvi).mean(axis=1)
    glad_res_t = glad_res_t - glad_res_t.mean().astype(np.int16)
    glad_ndvi_t = modis_ndvi + glad_res_t.reshape((-1,1))
    #glad_res_t = (glad_res_t - glad_res_t.mean(axis=1).reshape((-1,1)))/glad_res_t.std(axis=1).reshape((-1,1))
    #glad_res_t = glad_res_t.mean(axis=1)
    #r=52348
    fig = plt.figure(figsize=(15,5))
    #plt.plot(stl_trend_data[r,:])
    #plt.plot(glad_res_t)
    plt.plot(modis_ndvi.mean(axis=0),)
    plt.plot(glad_ndvi_t[1,:],'r.')
    plt.plot(glad_ndvi_t[30,:],'g.')

    #plt.savefig('D:\DATA\ETHIOPIA\TIMESAT\ale_sekela\')


#%%
if __name__=='__main__':
    #prepare_timesat()
    #timesat2nc()

    #prepare_timesat_raw(wsname='Ale')
    #timesat_modis_raw_to_nc(wsname='Sekela')
    timesat_modis_tpa_to_nc('Ale')