import geopandas as gp
from pathlib import Path
from rasterio.mask import mask
import rasterio as rio
from shapely import speedups
if speedups.available:
    speedups.enable()
import shapely.geometry as g
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import cartopy.feature as cfeature
from itertools import product
import multiprocessing as mp
from datetime import datetime, timedelta

plt.style.use('seaborn')

from eumap.misc import ttprint

data_dir = Path('/data/work/etiopija/data')
fc_dir = data_dir/'deafrica_fc'
shp_dir = Path('/data/work/etiopija/GIS')
plot_dir = Path('/data/work/etiopija/plots')
shp = shp_dir/'MWS_pilot.shp'
bs_files = sorted(fc_dir.glob('*.tif'))
# bs_files[0]

# ws_name = 'Ale'
ws_name = 'Sekela'

bs_out_dir = data_dir/f'{ws_name.lower()}_bare_soil_frac'
# pix_area = -ref.transform[0] * ref.transform[4]

mws_df = gp.read_file(shp)
mws_df = mws_df[mws_df.W_NAME==ws_name]
# ws_geom = mws_df.cascaded_union
# bounds = ws_geom.bounds

#%%

# !!! DEA fc_ls tiles are LANDSAT tiles reprojected into UTM,
# they have a bounding box much larger than the data, so
# the below makes no sense, filtering by region ID instead...

# def intersects(df):
#     def wrapped(rpath):
#         with rio.open(rpath) as src:
#             geom = df.geometry.to_crs(src.crs).cascaded_union
#             bbox = g.box(*src.bounds)
#             return geom.intersects(bbox)
#     return wrapped

# region_code = '170054' # Ale
region_code = '170052' # Sekela
bs_files = [*filter(lambda rpath: region_code in rpath.name, bs_files)]

months = sorted({*map(
    lambda fp: (lambda dt: f'{dt.year}-{dt.month:02}')(datetime.fromisoformat(fp.name[-17:-7])),
    bs_files,
)})

years = sorted({*map(
    lambda m: m[:4],
    months,
)})

#%%

ref = rio.open(bs_files[0])
# coords = g.mapping(mws_df.to_crs(ref.crs).cascaded_union)
coords = g.mapping(g.box(*mws_df.to_crs(ref.crs).cascaded_union.bounds))

def read_one_bs(bsf):
    with rio.open(bsf) as src:
        with rio.vrt.WarpedVRT(
            src,
            transform=ref.transform,
            width=ref.width,
            height=ref.height,
        ) as vrt:
            data, transform = mask(
                vrt, [coords],
                crop=True,
                all_touched=True,
            )
        data = data.astype(np.float16)
        data[data==src.nodata] = np.nan
    return data, transform

# month = months[0]

def agg_year(year):
    data_ref, transform = read_one_bs(bs_files[0])
    with rio.open(
        bs_out_dir/f'{ws_name.lower()}_bare_soil_frac_{year}.tif', 'w',
        **{
            **ref.profile,
            **{
                'transform': transform,
                'height': data_ref.shape[-2],
                'width': data_ref.shape[-1],
                'count': 12,
            },
        }
    ) as dst:
        for i_month in range(12):

            month = f'{year}-{i_month+1:02}'
            bsf_month = [*filter(
                lambda bsf: month in bsf.stem,
                bs_files,
            )]

            if bsf_month:
                # bs_month, transforms = [*zip(*map(
                bs_month, __ = [*zip(*map(
                    read_one_bs,
                    bsf_month,
                ))]

                # transform = transforms[0]

                bs_month = np.concatenate(bs_month, axis=0)
                bs_month = np.nanmean(bs_month, axis=0)
                bs_month[np.isnan(bs_month)] = ref.nodata
                bs_month = bs_month.round(0).astype(np.uint8)
            else:
                bs_month = np.full(data_ref.shape[-2:], ref.nodata, dtype=np.uint8)

            dst.write(bs_month, i_month+1)

#%%

# for i, __ in enumerate(map(
#     agg_year,
#     years,
# )):
#     ttprint(f'month {i+1} of {len(months)}')

with mp.Pool(mp.cpu_count()) as pool:
    for i, __ in enumerate(pool.imap_unordered(
        agg_year,
        years,
    )):
        ttprint(f'year {i+1} of {len(years)}')
