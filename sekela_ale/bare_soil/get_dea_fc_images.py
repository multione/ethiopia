import geopandas as gp
from pathlib import Path
from rasterio.mask import mask
import rasterio as rio
import shapely.geometry as g
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import cartopy.feature as cfeature
from itertools import product
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
from datetime import datetime, timedelta
import requests

plt.style.use('seaborn')

from eumap.misc import ttprint
from eumap import plotter

data_dir = Path('/data/work/etiopija/data')
fc_dir = data_dir/'deafrica_fc'
shp_dir = Path('/data/work/etiopija/GIS')
shp = shp_dir/'MWS_pilot.shp'

# ws_name = 'Ale'
ws_name = 'Sekela'

gdf = gp.read_file(shp)
# gdf = gdf[gdf.W_NAME==ws_name].to_crs(ref.crs)
gdf = gdf[gdf.W_NAME==ws_name].to_crs('EPSG:4326')
ws_geom = gdf.cascaded_union
bounds = ws_geom.bounds

from pystac_client import Client

cat = Client.open('https://explorer.digitalearth.africa/stac/')

search = cat.search(collections=['fc_ls'], bbox=bounds)
items = search.get_items()
bs_images = map(
    lambda it: it.assets['bs'].href.replace(
        's3://deafrica-services/',
        'https://deafrica-services.s3.af-south-1.amazonaws.com/',
    ),
    items,
)

# region_code = '170054' # other region is 170055, borders with 171054
# bs_images = filter(
#     lambda url: region_code in Path(url).stem,
#     bs_images,
# )
# next(bs_images)
#%%

# img = next(bs_images)

def get_image(url):
    out_path = fc_dir/Path(url).name
    if not out_path.exists():
        resp = requests.get(url)
        with open(out_path, 'wb') as dst:
            dst.write(resp.content)

with ThreadPool(8) as pool:
    for i, __ in enumerate(pool.imap_unordered(
        get_image,
        bs_images,
    )):
        ttprint(f'downloaded image {i+1}')
