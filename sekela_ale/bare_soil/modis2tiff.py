from pathlib import Path
from netCDF4 import Dataset
import rasterio as rio
from rasterio import warp
from functools import reduce
from operator import mul
from eumap import plotter
import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
import os
# %matplotlib inline

#%%
data_dir = Path('/data/work/etiopija/data')

ws_name = 'Ale'
tile_id = 'h21v08' # Ale
# ws_name = 'Sekela'
# tile_id = 'h21v07' # Sekela
# tile_id = 'h22v07'
# tile_id = 'h22v08'

out_dir = data_dir/f'modis_fc_tifs'
os.makedirs(out_dir, exist_ok=True)

mfc_files = sorted((data_dir/'modis_fc').glob(f'*{tile_id}*.nc'))

ref = rio.open(f'NETCDF:"{mfc_files[0].as_posix()}":bare_soil')
bounds = rio.open(next(data_dir.glob(f'*/{ws_name.lower()}_bare_soil_mask_2001.tif'))).bounds

#%%
# mfc_file = mfc_files[0]
def write_tiffs(mfc_file):
    mfc = Dataset(mfc_file)
    mfc_date = pd.Series(mfc.variables['time'][:]).apply(datetime.fromtimestamp)

    mfc_src = rio.open(f'NETCDF:"{mfc_file.as_posix()}":bare_soil')
    mfc_vrt = rio.vrt.WarpedVRT(
        mfc_src,
        crs='EPSG:4326',
        resampling=rio.enums.Resampling.nearest,
    )
    window = rio.windows.from_bounds(*bounds, mfc_vrt.transform)
    transform = rio.windows.transform(window, mfc_vrt.transform)

    for date_idx, ts in enumerate(mfc_date):
        data = mfc_vrt.read(date_idx+1, window=window)
        height, width = data.shape

        out_file = out_dir/f'{ws_name.lower()}_modis_bare_soil_frac_{ts.strftime("%Y%m")}.tif'

        with rio.open(
            out_file, 'w',
            **{
                **ref.profile,
                **{
                    'driver': 'GTiff',
                    'count': 1,
                    'crs': mfc_vrt.crs,
                    'width': width,
                    'height': height,
                    'transform': transform,
                },
            },
        ) as dst:
            dst.write(
                data,
                1,
            )

def worker(mfc_file):
    # try:
    write_tiffs(mfc_file)
    # except Exception as e:
        # return f'ERROR for {mfc_file}: {e}'

with mp.Pool(mp.cpu_count()//2) as pool:
    for i, err in enumerate(pool.imap_unordered(
        worker,
        mfc_files,
    )):
        if err is None:
            print(f'finished {i+1} of {len(mfc_files)}')
        else:
            print(err)
