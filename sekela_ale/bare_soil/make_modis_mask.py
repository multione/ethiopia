from pathlib import Path
from netCDF4 import Dataset
import rasterio as rio
from rasterio import warp
from functools import reduce
from operator import mul
from eumap import plotter
import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import multiprocessing as mp
from multiprocessing.pool import ThreadPool

# %matplotlib inline

# ws_name = 'Ale'
ws_name = 'Sekela'

data_dir = Path('/data/work/etiopija/data')
out_dir = data_dir/f'{ws_name.lower()}_bare_soil_mask'
# ndvi_file = [*data_dir.glob('*v8.nc')][0]
ndvi_file = data_dir/f'fusion_{ws_name}_v8.nc'
ndvi_src = Dataset(ndvi_file)
ndvi = ndvi_src.variables['ndvi_cb'][:]
ndvi = np.moveaxis(ndvi, -1, 0)
ndvi_year = pd.Series(ndvi_src.variables['year'][:])
ndvi_jan1st = ndvi_year.apply(lambda y: datetime(y, 1, 1))
ndvi_doy = pd.Series(ndvi_src.variables['t16'][:]).apply(lambda t: timedelta(days=t*16+7-1))
ndvi_date = ndvi_jan1st + ndvi_doy
ndvi_month = ndvi_date.apply(lambda dt: dt.month)

crs = ndvi_src.crs
transform = rio.transform.Affine.from_gdal(*ndvi_src.transform_gdal)
width = ndvi_src.width
height = ndvi_src.height
bounds = rio.transform.array_bounds(height, width, transform)

#%%
#
# with rio.open(
#     'tmp.tif', 'w',
#     transform=transform,
#     width=width,
#     height=height,
#     driver='GTiff',
#     crs=crs,
#     count=1,
#     dtype=rio.int16,
# ) as dst:
#     dst.write(ndvi[...,0], 1)

#%%

# mfc_files = sorted((data_dir/'modis_fc').glob('*h21v08*.nc')) # Ale
# mfc_files = sorted((data_dir/'modis_fc').glob('*h21v07*.nc')) # Sekela
# ref = rio.open(f'NETCDF:"{mfc_files[0].as_posix()}":bare_soil')

mfc_files = sorted(filter(
    lambda fp: not fp.stem.endswith('_flag'),
    (data_dir/'modis_fc_tifs_gapfilled').glob(f'{ws_name.lower()}*.tif'),
))

ref = rio.open(mfc_files[0])

idx_profile = {
    **ref.profile,
    **{
        'driver': 'GTiff',
        'dtype': 'uint32',
        'nodata': 0,
        'count': 1,
    },
}
idx_file = 'tmp_fc_pidx.tif'

with rio.open(
    idx_file, 'w',
    **idx_profile,
) as dst:
    dst.write(np.arange(1, ref.width*ref.height+1, dtype=np.uint32).reshape(ref.shape), 1)

with rio.open(idx_file) as idx_src:
    with rio.vrt.WarpedVRT(
        idx_src,
        crs=crs,
        transform=transform,
        width=width,
        height=height,
        resampling=rio.enums.Resampling.nearest,
    ) as vrt:
        window = vrt.window(*bounds)
        idx = vrt.read(1, window=window)
pix_indices = np.unique(idx)

#%%
# mfc_file = mfc_files[0]

def bare_soil_30m(year):
    # mfc = Dataset(mfc_file)
    # mfc_date = pd.Series(mfc.variables['time'][:]).apply(datetime.fromtimestamp)
    # mfc_src = rio.open(f'NETCDF:"{mfc_file.as_posix()}":bare_soil')

    mfc_files_year = [*filter(
        lambda fp: f'_{year}' in fp.stem,
        mfc_files,
    )]

    mfc_date = np.array([*map(
        lambda fp: datetime.strptime(fp.stem[-6:], '%Y%m'),
        mfc_files_year,
    )])

    out_file = out_dir/f'{ws_name.lower()}_bare_soil_mask_{mfc_date[0].year}.tif'

    with rio.open(
        out_file, 'w',
        crs=crs,
        width=width,
        height=height,
        transform=transform,
        count=mfc_date.size,
        dtype=rio.uint8,
        nodata=0,
        tiled=True,
        blockxsize=256,
        blockysize=256,
        compress='LZW'
    ) as dst:

        for date_idx in range(mfc_date.size):

            with rio.open(mfc_files_year[date_idx]) as mfc_src:
                with rio.vrt.WarpedVRT(
                    mfc_src,
                    crs=crs,
                    transform=transform,
                    width=width,
                    height=height,
                    resampling=rio.enums.Resampling.nearest,
                ) as vrt:
                    bare_soil_frac = vrt.read(1, window=window).astype(np.float16)
                    bfc_mask = vrt.read_masks(1, window=window).astype(bool)

            bare_soil_frac[~bfc_mask] = np.nan
            date = mfc_date[date_idx]

            ndvi_tidx = (ndvi_year == date.year) & (ndvi_month == date.month)
            ndvi_ = ndvi[ndvi_tidx].mean(axis=0)

            bare_soil = np.zeros_like(ndvi_, dtype=np.uint8)

            for pi in pix_indices:
                win_idx = idx == pi
                win_ndvi = ndvi_[win_idx]
                win_bs_frac = np.nanmedian(bare_soil_frac[win_idx])
                if not np.isnan(win_bs_frac):
                    ndvi_thresh = np.nanpercentile(win_ndvi, win_bs_frac)
                    win_bs = (win_ndvi < ndvi_thresh).astype(np.uint8)
                    bare_soil[win_idx] = win_bs

                dst.write(bare_soil, date_idx+1)

def worker(year):
    try:
        bare_soil_30m(year)
        return None
    except Exception as e:
        return f'ERROR for {mfc_file}: {e}'

with mp.Pool(mp.cpu_count()//2) as pool:
    for i, err in enumerate(pool.imap_unordered(
        worker,
        range(2001, 2021),
    )):
        if err is None:
            print(f'finished {i+1} of {len(mfc_files)}')
        else:
            print(err)
