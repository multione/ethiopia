import geopandas as gp
from pathlib import Path
from rasterio.mask import mask
import rasterio as rio
import shapely.geometry as g
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import cartopy.feature as cfeature
from itertools import product
import multiprocessing as mp

plt.style.use('seaborn')

from eumap import plotter

data_dir = Path('/data/work/etiopija/data')
shp_dir = Path('/data/work/etiopija/GIS')
plot_dir = Path('/data/work/etiopija/plots')
shp = shp_dir/'MWS_pilot.shp'

ws_name = 'Ale'

bs_mask_dir = data_dir/f'{ws_name.lower()}_bare_soil_mask'
bs_mask_files = sorted(bs_mask_dir.glob('*.tif'))
ref_mask = rio.open(bs_mask_files[0])
pix_area_mask = -ref_mask.transform[0] * ref_mask.transform[4]

gdf = gp.read_file(shp)
gdf = gdf[gdf.W_NAME==ws_name].to_crs(ref_mask.crs)
ws_geom = gdf.cascaded_union
bounds = ws_geom.bounds

years = [
    int(fp.stem[-4:])
    for fp in bs_mask_files
]
months = [*range(1, 13)]
N = len(years) * len(months)
# data = ref_mask.read(1)
# plotter.plot_rasters(data, cmaps='Greys', figsize=3)
mlabels = np.array([*map(
    lambda ym: f'{ym[1]:02} / {ym[0]}',
    product(years, months)
)])

#%%

def read_bs_file(feature, crs):
    df_ = gp.GeoDataFrame([feature], crs=crs)
    def wrapped(fp):
        with rio.open(fp) as src:
            geom_ = df_.to_crs(src.crs).iloc[0].geometry
            geom_ = g.mapping(geom_)
            data, __ = mask(src, [geom_], crop=True)
        return data
    return wrapped

#%%
feature = gdf.iloc[0]
#%%
def aggregate(feature):
#%%
    geom = feature.geometry
    mws_name = feature.mws_nm

    reader = read_bs_file(feature, gdf.crs)

    cropped = np.concatenate([*map(
        reader,
        bs_mask_files,
    )], axis=0)

    bs_frac_monthly = pix_area_mask * cropped.sum(axis=1).sum(axis=1) / geom.area
    bs_frac_yearly = bs_frac_monthly.reshape(-1, 12).mean(axis=-1).repeat(12)

    # fig, ax = plt.subplots(1, 2, figsize=(20, 5))
    fig = plt.figure(figsize=(26, 4))
    gs = fig.add_gridspec(1, 6)

    m_idx = np.arange(N)
    reg = stats.linregress(m_idx, bs_frac_monthly)
    trend = m_idx * reg[0] + reg[1]

    yearly_diff = (reg[0] * 1200).round(3)

    ax1 = fig.add_subplot(gs[1:])
    ax1.plot(bs_frac_monthly*100, 'green', label='Monthly values')
    ax1.plot(bs_frac_yearly*100, 'k', label='Yearly mean')
    ax1.set_ylabel('% bare soil')
    ax1.plot(trend*100, 'r--', label=f'20 year trend ({yearly_diff}% / year)')
    ax1.legend()
    ticks = np.arange(0, mlabels.size, 6)
    ax1.set_xticks(ticks)
    ax1.set_xticklabels(mlabels[ticks], rotation=45)

    stamen_terrain = cimgt.Stamen('terrain-background')
    # ax[1].plot(trend[6::12])

    ax2 = fig.add_subplot(gs[0], projection=ccrs.PlateCarree())
    ax2.set_extent([*bounds[0::2], *bounds[1::2]], crs=ccrs.PlateCarree())
    ax2.add_image(stamen_terrain, 11)
    cfeat = cfeature.ShapelyFeature([geom], ccrs.PlateCarree())
    cws = cfeature.ShapelyFeature([ws_geom], ccrs.PlateCarree())
    ax2.add_feature(cws, edgecolor='k', facecolor='#00000000', linewidth=1)
    ax2.add_feature(cfeat, edgecolor='green', facecolor='#00000000', linewidth=4)
    ax2.set_title(f'{ws_name.upper()} - {mws_name.upper()}')


    plt.savefig(plot_dir/f'bare_soil_{ws_name.lower()}-{mws_name.lower().replace(" ", "_")}', bbox_inches='tight', dpi=100)

#%%

with mp.Pool(mp.cpu_count()) as pool:
    __ = pool.map(
        aggregate,
        gdf.iloc,
    )
