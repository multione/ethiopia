import geopandas as gp
from pathlib import Path
from rasterio.mask import mask
import rasterio as rio
import shapely.geometry as g
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import cartopy.feature as cfeature
from itertools import product
import multiprocessing as mp

plt.style.use('seaborn')

from eumap import plotter

data_dir = Path('/data/work/etiopija/data')
shp_dir = Path('/data/work/etiopija/GIS')
plot_dir = Path('/data/work/etiopija/plots')
shp = shp_dir/'MWS_pilot.shp'

# ws_name = 'Ale'
ws_name = 'Sekela'

bs_mask_dir = data_dir/f'{ws_name.lower()}_bare_soil_mask'
bs_mask_files = sorted(bs_mask_dir.glob('*.tif'))
ref_mask = rio.open(bs_mask_files[0])
pix_area_mask = -ref_mask.transform[0] * ref_mask.transform[4]

bs_frac_dir = data_dir/f'{ws_name.lower()}_bare_soil_frac'
bs_frac_files = sorted(bs_frac_dir.glob('*.tif'))[-21:-1]
ref_frac = rio.open(bs_frac_files[0])
pix_area_frac = -ref_frac.transform[0] * ref_frac.transform[4]

gdf = gp.read_file(shp)
gdf = gdf[gdf.W_NAME==ws_name].to_crs(ref_mask.crs)
ws_geom = gdf.cascaded_union
bounds = ws_geom.bounds

months = [*range(1, 13)]

years_modis = [
    int(fp.stem[-4:])
    for fp in bs_mask_files
]
N_modis = len(years_modis) * len(months)
mlabels_modis = np.array([*map(
    lambda ym: f'{ym[1]:02} / {ym[0]}',
    product(years_modis, months)
)])

years_dea = [
    int(fp.stem[-4:])
    for fp in bs_frac_files
]
N_dea = len(years_dea) * len(months)
mlabels_dea = np.array([*map(
    lambda ym: f'{ym[1]:02} / {ym[0]}',
    product(years_dea, months)
)])

def read_bs_file(feature, crs):
    df_ = gp.GeoDataFrame([feature], crs=crs)
    def wrapped(fp):
        with rio.open(fp) as src:
            geom_ = df_.to_crs(src.crs).iloc[0].geometry
            geom_ = g.mapping(geom_)
            data, __ = mask(src, [geom_], crop=True)
            data = data.astype(np.float64)
            data[data==src.nodata] = np.nan
        return data
    return wrapped

#%%
feature = gdf.iloc[0]
#%%
def aggregate(feature):
#%%
    geom = feature.geometry
    mws_name = feature.mws_nm

    reader = read_bs_file(feature, gdf.crs)

    cropped_bs_modis = np.concatenate([*map(
        reader,
        bs_mask_files,
    )], axis=0).reshape(len(bs_mask_files)*12, -1)

    bs_frac_monthly_modis = pix_area_mask * np.nansum(cropped_bs_modis, axis=1) / geom.area
    bs_frac_yearly_modis = np.nanmean(bs_frac_monthly_modis.reshape(-1, 12), axis=-1)

    m_idx = np.arange(N_modis)
    reg_modis = stats.linregress(m_idx[:11*12], bs_frac_monthly_modis[:11*12])
    trend_modis_before = m_idx * reg_modis[0] + reg_modis[1]
    trend_modis_before[11*12:] = np.nan
    slope_modis_before = (reg_modis[0] * 1200).round(3)

    reg_modis = stats.linregress(m_idx[11*12:], bs_frac_monthly_modis[11*12:])
    trend_modis_after = m_idx * reg_modis[0] + reg_modis[1]
    trend_modis_after[:11*12] = np.nan
    trend_modis_after += trend_modis_before[11*12-1] - trend_modis_after[11*12]
    slope_modis_after = (reg_modis[0] * 1200).round(3)

    slope_modis = (reg_modis[0] * 1200).round(3)
    yearly_diff_modis = bs_frac_yearly_modis - bs_frac_yearly_modis[0]

    cropped_bs_dea = np.concatenate([*map(
        reader,
        bs_frac_files,
    )], axis=0).reshape(len(bs_frac_files)*12, -1)[-20*12:]
    # )], axis=0).reshape(len(bs_frac_files), -1)

    bs_dea_denand = [
        a[~np.isnan(a)]
        for a in cropped_bs_dea
    ]

    bs_frac_monthly_dea = np.nanmean(cropped_bs_dea, axis=1)
    bs_frac_yearly_dea = np.nanmean(bs_frac_monthly_dea.reshape(-1, 12), axis=-1)
    # bs_frac_yearly_dea = np.nanmean(cropped_bs_dea.reshape((len(bs_frac_files), -1)), axis=-1)

    notnan = ~np.isnan(bs_frac_monthly_dea)
    reg_dea = stats.linregress(m_idx[:11*12][notnan[:11*12]], bs_frac_monthly_dea[:11*12][notnan[:11*12]])
    trend_dea_before = m_idx * reg_dea[0] + reg_dea[1]
    trend_dea_before[11*12:] = np.nan
    slope_dea_before = (reg_dea[0] * 12).round(3)

    reg_dea = stats.linregress(m_idx[11*12:][notnan[11*12:]], bs_frac_monthly_dea[11*12:][notnan[11*12:]])
    trend_dea_after = m_idx * reg_dea[0] + reg_dea[1]
    trend_dea_after[:11*12] = np.nan
    trend_dea_after += trend_dea_before[11*12-1] - trend_dea_after[11*12]
    slope_dea_after = (reg_dea[0] * 12).round(3)

    # m_idx = np.arange(N_dea)
    # reg_dea = stats.linregress(m_idx, bs_frac_monthly_dea)
    # trend_dea = m_idx * reg_dea[0] + reg_dea[1]

    # slope_dea = (reg_dea[0] * 1200).round(3)
    yearly_diff_dea = bs_frac_yearly_dea - bs_frac_yearly_dea[0]

    fig = plt.figure(figsize=(26, 8))
    gs = fig.add_gridspec(2, 6, hspace=.4)

    stamen_terrain = cimgt.Stamen('terrain-background')
    ax0 = fig.add_subplot(gs[:1,:1], projection=ccrs.PlateCarree())
    ax0.set_extent([*bounds[0::2], *bounds[1::2]], crs=ccrs.PlateCarree())
    ax0.add_image(stamen_terrain, 11)
    cfeat = cfeature.ShapelyFeature([geom], ccrs.PlateCarree())
    cws = cfeature.ShapelyFeature([ws_geom], ccrs.PlateCarree())
    ax0.add_feature(cws, edgecolor='k', facecolor='#00000000', linewidth=1)
    ax0.add_feature(cfeat, edgecolor='green', facecolor='#00000000', linewidth=4)
    ax0.set_title(f'{ws_name.upper()} - {mws_name.upper()}')

    ax1 = fig.add_subplot(gs[:1,1:])
    ax1.plot(bs_frac_monthly_modis*100, 'darkgreen', label='Monthly values')
    # ax1.plot(bs_frac_yearly_modis.repeat(12)*100, 'k', label='Yearly mean')
    ax1.plot(trend_modis_before*100, 'k--', label=f'Pre-intervention trend ({slope_modis_before}% / year)')
    ax1.plot(trend_modis_after*100, 'k', label=f'Post-intervention trend ({slope_modis_after}% / year)')
    ax1.plot(yearly_diff_modis.repeat(12)*100, 'r--', label='Yearly median difference to first year')
    ax1.set_ylabel('% bare soil')
    ax1.legend()
    ticks = np.arange(0, mlabels_modis.size, 6)
    ax1.set_xticks(ticks)
    ax1.set_xticklabels(mlabels_modis[ticks], rotation=60)
    ax1.set_title('MODIS - derived 30m bare soil mask')

    ax2 = fig.add_subplot(gs[1:,1:], sharex=ax1)
    # ax2.violinplot(bs_dea_denand)#, label='Monthly values')
    ax2.boxplot(bs_dea_denand, showfliers=False)#, label='Monthly values')
    # ax2.plot(bs_frac_monthly_dea, 'green', label='Monthly values')
    # ax2.plot(bs_frac_yearly_dea.repeat(12), 'k', label='Yearly mean')
    # ax2.plot(trend_dea, 'r--', label=f'20 year trend ({slope_dea}% / year)')
    ax2.plot(trend_dea_before, 'k--', label=f'Pre-intervention trend ({slope_dea_before}% / year)')
    ax2.plot(trend_dea_after, 'k', label=f'Post-intervention trend ({slope_dea_after}% / year)')
    ax2.plot(yearly_diff_dea.repeat(12), 'r--', label='Yearly median difference to first year')
    ax2.set_ylabel('% bare soil')
    ax2.legend()
    ticks = np.arange(0, mlabels_dea.size, 6)
    ax2.set_xticks(ticks)
    ax2.set_xticklabels(mlabels_dea[ticks], rotation=60)
    ax2.set_title('DEA 30m bare soil fraction')

    plt.savefig(plot_dir/f'bare_soil_{ws_name.lower()}-{mws_name.lower().replace(" ", "_")}', bbox_inches='tight', dpi=100)
#%%

with mp.Pool(mp.cpu_count()) as pool:
    __ = pool.map(
        aggregate,
        gdf.iloc,
    )
