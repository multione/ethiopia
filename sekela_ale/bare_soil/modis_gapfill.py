from pathlib import Path
from netCDF4 import Dataset
import rasterio as rio
from rasterio import warp
from functools import reduce
from operator import mul
from eumap import plotter
import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
import os
# %matplotlib inline
from eumap.misc import find_files
from eumap import gapfiller

#%%

# ws_name = 'Ale'
ws_name = 'Sekela'

# tile_id = 'h21v08' # Ale
# tile_id = 'h21v07' # Sekela

data_dir = Path('/data/work/etiopija/data')
in_dir = data_dir/f'modis_fc_tifs'
out_dir = data_dir/f'modis_fc_tifs_gapfilled'

months = [
    f'{m:02}'
    for m in range(1, 13)
]


fn_rasters = []
for year in range(2001, 2021):
    for month in months:
        fn_rasters += find_files(dir_list=in_dir, pattern=f'{ws_name.lower()}*{year}{month}*.tif')


#%%

gf = gapfiller.TMWM(
    fn_files=fn_rasters,
    season_size=12,
    time_win_size=9,
    cpu_max_workers=8,
    # outlier_remover=gapfiller.OutlierRemover.Std,
    std_env=2,
    n_jobs_io=8,
    verbose=True,
)

# gf = gapfiller.TLI(
#     fn_files=fn_rasters,
# )

data_gapfilled = gf.run()

fn_rasters_gf = gf.save_rasters(
    out_dir,
    out_mantain_subdirs=False,
    save_flag=True,
)

# print(f'Total number of files: {len(fn_rasters)}')
# print(f'- First file: {fn_rasters[0]}')
# print(f'- Last file: {fn_rasters[len(fn_rasters)-1]}')

# return fn_rasters

# def worker(mfc_file):
#     try:
#         write_tiffs(mfc_file)
#         return None
#     except Exception as e:
#         return f'ERROR for {mfc_file}: {e}'
#
# with mp.Pool(mp.cpu_count()//2) as pool:
#     for i, err in enumerate(pool.imap_unordered(
#         worker,
#         mfc_files,
#     )):
#         if err is None:
#             print(f'finished {i+1} of {len(mfc_files)}')
#         else:
#             print(err)
