# Ethiopia

## pilot
MOD13Q1 modis dataset download, preparing for timesat, reading timesat outputs
and some analysis on 250m
Also modis dataset with fractional cover on 500m

- download_modis.py
- modis.py
- modis_fc.py
- timesat_util.py
  - util functions for writing and reading timesat files
- timesat.py

## sekela_ale
Two choisen watersheds - Sekela and Ale. First only Ale becouse it's in single GLAD 
tile so it was easier. 
Fusion of modis MOD13Q1 dataset with GLAD 
- download_glad.py
  - download glad, ....
  - - timesat_modis

  1. download_glad.py: download_Ale(), download_Sekela()
  2. download_glad.py: glad2nc_Ale(), glad2nc_Sekela()
  3. download_glad.py: glad_modis_nc('Ale'), glad_modis_nc('Sekela')
  4. timesat_modis.py: prepare_timesat_raw(wsname='Sekela')
  5. run timesat: c:\utils\timesat33\timesat_fortran\main\TSF_process.exe set_modis_ndvi_raw.set 10 #number of processors doesn't work on windows
  6. timesat_modis.py: timesat_modis_raw_to_nc(wsname='Ale')
  7. fusion.py: fusion_ndvi_v8('Sekela')
  8. fusion_timesat.py: fusion2timesat('Sekela', 'v8')
  9. run timesat (parallel): 
    - c:\utils\timesat33\timesat_fortran\main\TSF_process.exe set_Sekela_ndvi_nn.set 10 
    - c:\utils\timesat33\timesat_fortran\main\TSF_process.exe set_Sekela_ndvi_bl.set 10 
    - c:\utils\timesat33\timesat_fortran\main\TSF_process.exe set_Sekela_ndvi_cb.set 10 
  10. fusion_timesat.py: timesat2nc(wsname = 'Ale', ver = 'v8')
  11. fusion_maps.py: mean_slope_params(wsname='Sekela',ver='v8')
  12. fusion_maps.py: seasonal_params_boxplots('Sekela','v8')