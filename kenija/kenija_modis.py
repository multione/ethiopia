#%%
import geetools
import ee
import numpy as np
import pandas
import datetime
import pickle
from pathlib import Path

ee.Initialize()


#%%
def pixel_modis():
#%%
    # download all pixels that intersects watersheds, every date - one image with EVI and NDVI 
    fld = Path(r'D:\DATA\KENIJA\MODIS\ws_allpixs')

    areas = ee.FeatureCollection('projects/ee-multione/assets/kenija/bounds')
    roi = areas.geometry().bounds()
    collection = ee.ImageCollection("MODIS/006/MOD13Q1").select(["NDVI","EVI","SummaryQA"])

    img_list = collection.toList(collection.size())
    img_count = img_list.size().getInfo()
    print(img_count)
    res=[]
    for i in  range(img_count):#range(img_count):  #i=0
        print(f'{i}/{img_count}')
        ff = fld/f'modis_{i:03}.pickle'

        #if not ff.exists():
        img = ee.Image(img_list.get(i)).clip(roi)
        date = datetime.datetime.utcfromtimestamp(img.get('system:time_start').getInfo()/1000)
        mask =ee.Image.constant(1).clip(areas.geometry()).mask()
        img_msk = img.updateMask(mask)
        #task=geetools.batch.Export.image.toDrive(img_msk, folder='GEE1', fileNamePrefix='test1')
        sdate = f'{date:%Y%m%d}'
        task = geetools.batch.Export.image.toCloudStorage(img_msk, f'modis_{sdate}', bucket='mo_temp', fileNamePrefix=f'kenija_modis/modis_{sdate}')
        task.start()



if __name__=='__main__':
    pixel_modis()