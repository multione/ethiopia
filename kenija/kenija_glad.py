#%%
from datetime import timedelta
import geopandas
import requests
from pathlib import Path
from shapely import geometry

from shapely.ops import transform

# 037E_02S
# 036E_06S
glad_tiles = ['037E_02S', '036E_06S']
intervals = list(range(461, 967))   # 2000 - 2021
username = 'krizanjosip'
password = 'SHBsLTmDfskDM6Ol'
fld = Path(r'D:\DATA\KENIJA\GLAD\GLAD_TILES')

#%%
def download_glad_image(tile, year, t16, outfolder):
    # downloads one image
    lat = tile.split('_')[-1]
    interval = (year-1980)*23 + t16 +1
    #year = (interval - 1) // 23 + 1980
    #t16 = interval - (year-1980)*23 - 1

    url = f'https://glad.umd.edu/dataset/landsat_v1.1/{lat}/{tile}/{interval}.tif'
    outfilename = f'glad_{tile}_{year}_{t16:02d}.tif'
    outfile = Path(outfolder)/outfilename
    if outfile.exists():
        print('Exists.')
        return

    with requests.get(url, stream=True, auth=(username, password)) as r:
        r.raise_for_status()
        with open(outfile, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024*1024*10):
                print('.', end='', flush=True)
                f.write(chunk)
        print('Done.')

def download_all():
    for year in range(2000,2022):
        print(year)
        for t16 in range(23):
            print(f'\t{t16+1} ', end='', flush='True')
            for tile in glad_tiles:
                download_glad_image(tile, year, t16, fld)
        
if __name__ == '__main__':
    download_all()