#%%
from pathlib import Path
import rasterio
from rasterio.features import rasterize
import geopandas
import pandas
import numpy as np
from timesat_util import Timesat_Input, Timesat_Tpa
import pickle

proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

#%%
def gee_to_df(input_folder, input_shape, input_property, output_file):
#%%
    # convert images from gee to dataframe sutiable for timesat_util
    '''
    input_folder = Path(r'D:\DATA\KENIJA\MODIS\allpixs')
    input_shape = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\KENIJA\kenija_bound_plots.shp')
    input_property = 'id'
    output_file = Path(r'D:\DATA\KENIJA\MODIS\allpixs.df')
    '''

    shp = geopandas.read_file(input_shape).to_crs(proj4_modis)

    files = list(input_folder.glob('modis_*.tif'))

    with rasterio.open(files[0]) as src:
        profile = src.profile
        w = profile['width']
        h = profile['height']
        t = profile['transform']
        output_georef = output_file.with_name(output_file.with_suffix('').name+'_georef.pickle')
        pickle.dump(profile, output_georef.open('wb'))

    shapes = list(zip(shp.geometry, shp[input_property].values))
    objectids = rasterize(shapes, out_shape=(h,w), fill=0, transform=t, dtype=rasterio.int16)
    objectids_ind = objectids>0
    objectids = objectids[objectids_ind]
    objectids_fn = output_file.with_name(output_file.with_suffix('').name+'_objectids.txt')
    np.savetxt(objectids_fn, objectids,'%d')

    sids = np.arange(h*w).reshape((h,w))
    sids = sids[objectids_ind]

    n = objectids.shape[0]

    '''
    with rasterio.open(input_folder.parent/'test.tif', 'w', driver='GTiff', width=w, height=h, count=1, crs=proj4_modis, transform=t, dtype=rasterio.int16) as dst:
        dst.write(ids, 1)
    '''

#%%
    spatial_id=[]
    object_id = []
    time_id=[]
    ndvi=[]
    evi=[]
    viq=[]
    for f in files:
        # f= files[0]
        print(f.name)
        date=f.with_suffix('').name.split('_')[-1]
        with rasterio.open(f) as src:
            img_ndvi = src.read(1)[objectids_ind]
            img_evi = src.read(2)[objectids_ind]
            img_viq = src.read(3)[objectids_ind]

        spatial_id.append(sids)
        object_id.append(objectids)
        time_id.extend([date]*n)
        ndvi.append(img_ndvi)
        evi.append(img_evi)
        viq.append(img_viq)

    spatial_id = np.concatenate(spatial_id)
    object_id = np.concatenate(object_id)
    ndvi = np.concatenate(ndvi)
    evi = np.concatenate(evi)
    viq = np.concatenate(viq)

    df = pandas.DataFrame(data = dict(object_id = object_id, spatial_id=spatial_id, time_id=time_id,ndvi=ndvi, evi=evi, viq=viq))
    df.to_pickle(output_file)

    dfs = df[['spatial_id','object_id']].drop_duplicates().sort_values('spatial_id')
    dfs.to_pickle(output_file.with_name(output_file.with_suffix('').name+'_sids.pickle'))

#%%
def prepare_timesat(input_file, output_file):
    # Read dataframe and prepare input for timesat
    '''
    input_file = Path(r'D:\DATA\KENIJA\MODIS\allpixs.df')
    output_file = Path(r'D:\DATA\KENIJA\TIMESAT\allpixs\timesat_input.txt')
    '''
    df = pandas.read_pickle(input_file)
    df['time_id'] = pandas.to_datetime(df.time_id)
    df = df.loc[df.time_id.dt.year<=2021]
    df['weight'] = 15 - df.viq*5
    del df['viq']

    ti = Timesat_Input()
    ti.load_from_df(df)

    spatial_ids = df['spatial_id'].unique()

    del df

    ti.write_timesat(output_file, bands=['ndvi','evi'], kind='simple')

    # subsample
    subsample_ids = np.random.choice(spatial_ids, 1000)
    subsample_fn = output_file.with_name(output_file.with_suffix('').name+'_ss.txt')
    ti.write_timesat(subsample_fn, bands=['ndvi','evi'], kind='simple', spatial_ids=subsample_ids)

# %%
def timesat_pp(input_file, spatialid_file):
    # Postprocessing of TIMESAT outputs
    '''
    input_file = Path(r'D:\DATA\KENIJA\TIMESAT\allpixs\output_kenija1\kenija1_STL.tpa')
    spatialid_file = Path(r'D:\DATA\KENIJA\MODIS\allpixs_sids.pickle')
    '''
    tpa = Timesat_Tpa(input_file)

    dfo = tpa.decode_data(first_year=2000, season_offset=1, season_stride=1)
    dfo.NPOINT = dfo.NPOINT-1
    #dfo.to_excel(input_file.with_name('ethiopia1_seasons.xlsx'))

    dfs = pandas.read_pickle(spatialid_file)
    dfo = dfo.join(dfs, on='NPOINT')
    dfo.to_pickle(input_file.with_name('timesat_seasons.pickle'))


def mws_params_shape(input_file, input_shapefile, output_file):
    # Calculate slope for every mws/seasonal param and write it to shapefile
    '''
    input_file =  Path(r'D:\DATA\KENIJA\TIMESAT\allpixs\output_kenija1\timesat_seasons.pickle')
    input_shapefile = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\KENIJA\kenija_bound_plots.shp')
    output_file = Path(r'D:\DATA\KENIJA\TIMESAT\allpixs\output_kenija1\kenija_slopes.shp')
    '''
    import geopandas
    from sklearn.linear_model import LinearRegression
    from sklearn.pipeline import make_pipeline
    from sklearn.preprocessing import scale, StandardScaler
    params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

    def agg_slope(dff):
        x = dff.YEAR.values.reshape(-1,1)
        y = scale(dff[params].values)
        model = LinearRegression()
        model.fit(x,y)
        return model.coef_.reshape(-1)


    df = pandas.read_pickle(input_file)
    df['EOS'] = df['SOS'] + df['LOS']

    dfg = df.groupby('object_id')
    # oid=2319; dff=df.loc[dfg.groups[oid]]
    dfs = dfg.apply(agg_slope)
    dfs = pandas.DataFrame.from_dict(dict(zip(dfs.index, dfs.values))).T
    dfs.columns = [f'{p}_slp' for p in params]

    dfs.to_pickle(output_file.with_suffix('.pickle'))

    shp = geopandas.read_file(input_shapefile)
    shp = shp.join(dfs, on='id')
    shp.to_file(output_file)

def params_boxplots():
    '''
    fldout = Path(r'D:\DATA\KENIJA\TIMESAT\allpixs\boxplots')
    input_file =  Path(r'D:\DATA\KENIJA\TIMESAT\allpixs\output_kenija1\timesat_seasons.pickle')
    input_shapefile = Path(r'D:\DATA\KENIJA\TIMESAT\allpixs\output_kenija1\kenija_slopes.shp')

    '''
#%%
    from pathlib import Path
    import geopandas
    import pandas
    import numpy as np
    import seaborn as sns
    import matplotlib.pyplot as plt

    params = ['SOS', 'EOS', 'LOS', 'BLV', 'MOS', 'LEF', 'SAM', 'ROI', 'ROD', 'LSI', 'SSI', 'SSV', 'ESV']

    df = pandas.read_pickle(input_file)
    df = df.loc[df.YEAR<2022]
    df['EOS'] = df['SOS'] + df['LOS']

    shp = geopandas.read_file(input_shapefile)
    shp = shp.loc[~shp['SSI_slp'].isna()].sort_values('SSI_slp')

    '''
    nn = 10
    inds = np.r_[np.arange(nn),-np.arange(nn,0,-1)]
    objectids = shp['OBJECTID'].values[inds]
    slopes = (shp['SSI_slp'].values[inds]*1000).astype(int)
    '''
    # oid_groups = dict(ale_woreda=np.arange(1671,1678),
    #                   bore_woreda=np.arange(1660,1670),
    #                   sekela=np.arange(601,614))
    oid_groups = dict(zip(shp['Name'],shp['id']))
#%%

    for i,r in shp.iterrows():
        # i,r = next(shp.iterrows())
        #print(f'{i}/{n}: {oid}')
        # oid = objectids[45]
        oid=r['id']
        name=r['Name']
        dff = df.loc[df.object_id==r['id']]
        plt.close('all')
        fig, axs = plt.subplots(13,1, figsize=(10,30), sharex=True, constrained_layout=True)
        for ax, var in zip(axs, params):
            sns.boxplot(x='YEAR', y=var, data=dff, ax=ax).set(xlabel='', ylabel=var)
            # if var=='SOS':
            #     ax.set_ylim(0,150)
            # if var=='EOS':
            #     ax.set_ylim(150,300)
            ax.grid(axis='y', linestyle='--')
        ax.set_xlabel('YEAR')
        #fig.suptitle(f'OBJECTID: {oid}, SLOPE: {slp:04d}', fontsize=16)
        fig.suptitle(f'{r["Name"]}, id: {r["id"]}', fontsize=16)

        #dstpath = fldout/grp/f'slp[{slp:04d}]_{oid:04d}_boxplot.png'
        dstpath = fldout/f'{oid}_{name}_boxplot.png'
        dstpath.parent.mkdir(parents=True, exist_ok=True)
        plt.savefig(dstpath, dpi=200, bbox_inches='tight', facecolor='w')

        #sns.boxplot(x='YEAR', y='SOS', data=dff)
        #sns.violinplot(x='YEAR', y='SOS', data=dff)

        #sns.swarmplot(x='YEAR', y='SOS', data=dff, color='.25')
