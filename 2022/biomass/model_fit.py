#%%
from cmath import isfinite
import pandas
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import LinearRegression
from pathlib import Path
import geopandas

# Working folder
fld_ml = Path('D:\DATA\ETHIOPIA2022\ML')
# TIMESAT working folder
fld_timesat = Path(r'D:\DATA\ETHIOPIA2022\TIMESAT\work')
# Dataset with prepared data
fn_dataset = r'D:\DATA\ETHIOPIA2022\TIMESAT\work\modis_agb.df'
# Yearly EVI dataset
fn_timesat_yearly = fld_timesat/'evi_yearly.parquet'
# Shape file of watersheds
shp_file = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\SLMP_MWS_2017-05-01_modis.shp')
# Data in google spreadsheet
fn_data = 'https://docs.google.com/spreadsheets/d/1k020YkZRfRDWuYwBYMSDBzJwoDl6TA3d/export?format=csv&gid=1319881855'
#%%
# Function that are fitted to data
def f(x, a,b,c,d):
    return a*np.arctan(b*(x-c))+d

# Fitting model and calculating AGB predictions
def modis_fit_predictions():
    df = pandas.read_pickle(fn_dataset)

    # Extracting EVI and AGB values
    evi = df.evi.values
    agb = df.agb.values

    # only finite values
    ind = np.isfinite(agb)
    x=evi[ind]
    y=agb[ind]

    # fit model
    popt,pcov = curve_fit(f, x, y, (75,1,10,132))
    print(f'Fitted values for a,b,c,d: {popt}')
    yprd = f(x,*popt)
    r2 = r2_score(y,yprd)
    rmse = np.sqrt(mean_squared_error(y,yprd))

    # Plotting of fitted function
    plt.rcParams.update({'font.size': 18})
    fig,ax = plt.subplots(1,1,figsize=(16,8))
    ax.plot(x,y,'k.', label='AGB')
    xx = np.linspace(x.min(),x.max(),50)
    ax.plot(xx,f(xx, *popt),'r-', label='y=a*arctan(b(x-c))+d\na={:f}\nb={:f}\nc={:f}\nd={:f}'.format(*popt))
    ax.set_ylabel('Aboveground biomass $[Mg/ha]$')
    ax.set_xlabel('Yearly sum of EVI')
    ax.annotate(f'R^2 = {r2:.3f}\nRMSE = {rmse:.3f}',(0.01, 0.5),xycoords='axes fraction')
    plt.legend()
    plt.savefig(fld_ml/'abg_fit.png',dpi=200, facecolor='white')

    # Calculating mean AGB predictions on level of microwatersheds
    dfye = pandas.read_parquet(fn_timesat_yearly)
    dfye.columns=['spatial_id','object_id','year','evi']
    dfo = dfye.groupby(['object_id','year'])['evi'].mean().reset_index()
    dfo['agb'] = f(dfo.evi.values, *popt)

    dfp = dfo.pivot('object_id', 'year', 'agb')
    dfp.columns=[str(c) for c in dfp.columns]

    shp = geopandas.read_file(shp_file)

    # Saving predictions per year to shape file
    shp = shp.join(dfp,on='OBJECTID')
    shp.to_file(fld_ml/'mws_agb.gpkg',driver='GPKG')

#%% Calculating of slopes of yearly AGB per microwatershed, before intervention year and after intervention
def modis_slopes():
    
    # Reading data from google spreadsheet (int_year == year of intervention)
    dfd = pandas.read_csv(fn_data)
    dfd = dfd[['OBJECTID','donor','Z_NAME','W_NAME','SLMP_MWS_shapefiles.imp_yr']]
    dfd.columns = ['objectid','donor','z_name','w_name','int_year']
    dfd = dfd.dropna()    
    dfd['int_year'] = dfd['int_year'].astype(int)
    #mwsdict = dfd.set_index('objectid').to_dict('index')

    # Reading data with predicted AGB
    shp = geopandas.read_file(fld_ml/'mws_agb.gpkg')
    df = shp.join(dfd[['objectid','int_year']].set_index('objectid'), on='OBJECTID')
    df['slope_before'] = np.nan
    df['slope_after'] = np.nan

    lm = LinearRegression()
    for ir,r in enumerate(df.to_dict('records')):
        # calculating slopes for every microwatershed
        intyear = r['int_year']
        if not np.isfinite(intyear):
            intyear=2021
        else:
            intyear=int(intyear)

        # Before intervention
        x=np.arange(2000,intyear+1).reshape(-1,1)
        y=np.array([r[str(year)] for year in x[:,0]])

        if np.isfinite(y).all():
            lm = lm.fit(x,y)
            slope = lm.coef_[0]
            df.loc[ir,'slope_before']=slope

        # After intervention
        if intyear<2020:
            x = np.arange(intyear,2022).reshape(-1,1)
            y = np.array([r[str(year)] for year in x[:,0]])

            if np.isfinite(y).all():
                lm = lm.fit(x,y)
                slope = lm.coef_[0]
                df.loc[ir,'slope_after']=slope

    # Saving slopes to shape
    df.to_file(fld_ml/'mws_agb_slopes.gpkg',driver='GPKG')

if __name__ == '__main__':
    modis_fit_predictions()
    modis_slopes()
