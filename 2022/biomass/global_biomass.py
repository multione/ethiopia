#%%
from turtle import shape
from netCDF4 import Dataset
from pathlib import Path
import numpy as np
import rasterio
import shapely
import shapely.geometry
import geopandas

# defining projections
crs_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'
crs_wgs84 = 'EPSG:4326'

# Folder with input data
fld = Path(r'D:\DATA\ETHIOPIA2022\GLOBAL_AB')
#%% Testing code for one year. Not important
def testing():
    
    year = 2001
    
    nc = Dataset(fld/'Global_annual_mean_ABC_lc2001_1993_2012_20150331.nc','r')

    years = nc['time'][:].data
    lon = nc['longitude'][:].data
    lat = nc['latitude'][:].data
    abc = nc['Aboveground Biomass Carbon'][:]

    iyear = years==year
    dlon = 0.25 #abs(lon[1]-lon[0])
    dlat = 0.25 #abs(lat[1]-lat[0])

    data = (abc[iyear, :, :].data).squeeze().T * 2 # Convert biomas C to biomass


    transform = rasterio.transform.from_origin(lon[0],lat[0],dlon,dlat)
    profile={'driver':'GTiff','dtype':'float32','nodata':abc.fill_value * 2, 'tiled':True, 
            'width':data.shape[1],'height':data.shape[0],'count':1,'crs':crs_wgs84, 'transform':transform}

    with rasterio.open(fld/f'agb_global_{year}.tif', 'w', **profile) as dst:
        dst.write(data,1)

    nc.close()
#%% Main function for reading netCDF file and writing to .gpkg

def main():
    years = range(2001, 2012+1)
    nc = Dataset(fld/'Global_annual_mean_ABC_lc2001_1993_2012_20150331.nc','r')

    years = nc['time'][:].data
    lon0 = nc['longitude'][:].data[0]
    lat0 = nc['latitude'][:].data[0]
    dlon = 0.25 #abs(lon[1]-lon[0])
    dlat = -0.25 #abs(lat[1]-lat[0])

    abc = nc['Aboveground Biomass Carbon'][:]
    ntimes, nlon, nlat = abc.shape
    mask = nc['Map of grid cells with valid ABC values'][:].data
    # Možda bi trebalo drugačije napraviti masku jer ovako gubim dosta podataka
    # Trebala bi maska biti True gdje ima barem jedan valid podatak u biranim godinama

    polys = []
    pid=0
    pids=[]
    for ilon in range(nlon):
        lon = lon0 + ilon*dlon
        for ilat in range(nlat):
            lat = lat0 + ilat*dlat

            if mask[ilon,ilat]>0:
                box = shapely.geometry.box(lon,lat+dlat,lon+dlon,lat)
                polys.append(box)
                pids.append(pid)

            pid += 1

    df = geopandas.GeoDataFrame(data={'pid':pids}, geometry=polys, crs=crs_wgs84)

    for year in years:
        iyear = years==year
        data = abc.data[iyear,:].flatten()[pids] * 2    # Multiplzing with 2 to get AGB from AGBc
        df[f'Y{int(year)}'] = data

    df.to_file(fld/'global_agb.gpkg', driver='GPKG')

    nc.close()      


if __name__=='__main__':
    main()