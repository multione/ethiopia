#%%
from pathlib import Path
import rasterio
from rasterio.features import rasterize
import geopandas
import pandas
import numpy as np
# These are mine wrappers for TIMESAT input and output files
from timesat_util import Timesat_Input, Timesat_Tpa, Timesat_tts
import pickle

proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

# Folder with modis data 
fld_modis_input =  Path(r'D:\DATA\ETHIOPIA\MODIS\ws_allpixs')
# Folder for TIMESAT output
fld_timesat_output = Path(r'D:\DATA\ETHIOPIA2022\TIMESAT')
# File with temporary data
fn_output = fld_timesat_output/'modis4timesat.df'
# File with pixel ID's 
fn_output_sids = fn_output.with_name(fn_output.with_suffix('').name+'_sids.pickle')
# Working folder for temporary files
fld_timesat_work = fld_timesat_output/'work'
# Shape file with all watersheds
shp_file = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\SLMP_MWS_2017-05-01_modis.shp')
# TIMESAT output file
fn_timesat_tts = fld_timesat_work/'eth2022_v2_fit.tts'
# TIMESAT output file converted to parquet format
fn_dataframe_tts = fn_timesat_tts.with_suffix('.parquet')
#%%
# convert MODIS images from gee to dataframe suitable for timesat_util    
def modis_to_df():
    
    shp = geopandas.read_file(shp_file)
    # Every file is MODIS NDVI,EVI,VIQ for one 16 days period in all watersheds
    files = list(fld_modis_input.glob('modis_*.tif'))

    # Find out metadata about modis tiffs
    with rasterio.open(files[0]) as src: 
        profile = src.profile
        w = profile['width']
        h = profile['height']
        t = profile['transform']
        output_georef = fn_output.with_name(fn_output.with_suffix('').name+'_georef.pickle')
        # Save georeference data for later
        pickle.dump(profile, output_georef.open('wb'))

    # This block of code rasterizes OBJECTID field in shapefile and saves it for later
    shapes = zip(shp.geometry, shp['OBJECTID'])
    objectids = rasterize(shapes, out_shape=(h,w), fill=0, transform=t, dtype=rasterio.int16)
    objectids_ind = objectids>0
    objectids = objectids[objectids_ind]
    objectids_fn = fn_output.with_name(fn_output.with_suffix('').name+'_objectids.txt')
    np.savetxt(objectids_fn, objectids,'%d')

    # sids represents uniqe pixel id in input MODIS tiffs
    sids = np.arange(h*w).reshape((h,w))
    # we are using only pixels that are in some of watershed
    sids = sids[objectids_ind]

    # number of pixels
    n = objectids.shape[0]

    
    spatial_id=[] # id of pixels
    object_id = [] # id of watershed
    time_id=[] # date of modis values
    ndvi=[] # ndvi values
    evi=[] # evi values
    viq=[] # pixel quality index values
    for f in files:        
        print(f.name)
        # extracting of date from filename
        date=f.with_suffix('').name.split('_')[-1]
        # Reading data from MODIS file
        with rasterio.open(f) as src:            
            img_ndvi = src.read(1)[objectids_ind]
            img_evi = src.read(2)[objectids_ind]
            img_viq = src.read(3)[objectids_ind]

        spatial_id.append(sids)
        object_id.append(objectids)
        time_id.extend([date]*n)
        ndvi.append(img_ndvi)
        evi.append(img_evi)
        viq.append(img_viq)

    spatial_id = np.concatenate(spatial_id)
    object_id = np.concatenate(object_id)
    ndvi = np.concatenate(ndvi)
    evi = np.concatenate(evi)
    viq = np.concatenate(viq)

    # Putting all data to DataFrame and saving it
    df = pandas.DataFrame(data = dict(object_id = object_id, spatial_id=spatial_id, time_id=time_id,ndvi=ndvi, evi=evi, viq=viq))
    df.to_pickle(fn_output)

    # extracting relations of pixel id and shape id and saving it for later
    dfs = df[['spatial_id','object_id']].drop_duplicates().sort_values('spatial_id')
    dfs.to_pickle(fn_output_sids)

#%% Preparing files to TIMESAT input format
def prepare_timesat():
    # Input dataset we prepared in modis_to_df()
    fn_input = fld_timesat_output/'modis4timesat.df'
    # Output file
    fn_output = fld_timesat_work/'timesat_input.txt'

    df = pandas.read_pickle(fn_input)
    df['time_id'] = pandas.to_datetime(df.time_id)
    # Only to 2021
    df = df.loc[df.time_id.dt.year<=2021]
    # Weight is level of significance of pixel in TIMESAT calculations
    df['weight'] = 15 - df.viq*5
    del df['viq']

    ti = Timesat_Input()    
    ti.load_from_df(df)

    spatial_ids = df['spatial_id'].unique()
    del df

    ti.write_timesat(fn_output, bands=['ndvi','evi'], kind='simple')

    # subsample small number of random pixels for testing 
    subsample_ids = np.random.choice(spatial_ids, 1000)
    subsample_fn = fn_output.with_name(fn_output.with_suffix('').name+'_ss.txt')
    ti.write_timesat(subsample_fn, bands=['ndvi','evi'], kind='simple', spatial_ids=subsample_ids)

# Decode data from TIMESAT output
def timesat_pp():
    tts = Timesat_tts(fn_timesat_tts)
    dfo = tts.decode_data(first_year=2000, season_offset=1)
    dfo.NPOINT = dfo.NPOINT-1
    dfs = pandas.read_pickle(fn_output_sids)
    dfo = dfo.join(dfs, on='NPOINT')
    dfo.to_parquet(fn_dataframe_tts)

if __name__=='__main__':
    modis_to_df()
    prepare_timesat()
    # run TIMESAT application
    timesat_pp()