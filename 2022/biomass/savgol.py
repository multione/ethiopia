
#%%

import pandas as pd
import numpy as np
from scipy.signal import savgol_filter

def savitzky_golay_filtering_original1(timeseries, wnds=[11, 7], orders=[2, 4], debug=True):                                     
    '''
    A simple method for reconstructing a high quality NDVI time-series data set based on the Savitzky-Golay filter", Jin Chen et al. 2004
    '''
    interp_ts = pd.Series(timeseries)
    interp_ts = interp_ts.interpolate(method='linear', limit=14)
    smooth_ts = interp_ts                                                                                              
    wnd, order = wnds[0], orders[0]
    F = 1e8 
    W = None
    it = 0                                                                                                             
    while True:
        smoother_ts = savgol_filter(smooth_ts, window_length=wnd, polyorder=order)                                     
        diff = smoother_ts - interp_ts
        sign = diff > 0                                                                                                                       
        if W is None:
            W = 1 - np.abs(diff) / np.max(np.abs(diff)) * sign                                                         
            wnd, order = wnds[1], orders[1]                                                                            
        fitting_score = np.sum(np.abs(diff) * W)                                                                       
        #print it, ' : ', fitting_score
        if fitting_score > F:
            break
        else:
            F = fitting_score
            it += 1        
        smooth_ts = smoother_ts * sign + interp_ts * (1 - sign)
    if debug:
        return smooth_ts, interp_ts
    return smooth_ts

def savitzky_golay_filtering_original2(timeseries, wnds=[11, 7], orders=[2, 4], debug=True):                                     
    '''
    Popravak
    https://gis.stackexchange.com/questions/173721/reconstructing-modis-time-series-applying-savitzky-golay-filter-with-python-nump

    '''
    interp_ts = pd.Series(timeseries).interpolate(method='linear', limit=9999)
    smooth_ts = interp_ts                                                                                              
    wnd, order = wnds[0], orders[0]
    F = 1e8 
    it = 0                                                                                                             
    while True:
        smoother_ts = savgol_filter(smooth_ts, window_length=wnd, polyorder=order)                                     
        diff = (smoother_ts - interp_ts).clip(0)
        sign = diff > 0                                                                                                                       
        W = 1 - diff / np.max(diff)                                                         
        wnd, order = wnds[1], orders[1]                                                                            
        fitting_score = np.sum(diff * W)                                                                       
        #print it, ' : ', fitting_score
        if fitting_score > F:
            break
        else:
            F = fitting_score
            it += 1        
        smooth_ts = smoother_ts * sign + interp_ts * (1 - sign)
    if debug:
        return smooth_ts, interp_ts
    return smooth_ts

def savitzky_golay_filtering(timeseries, wnds=[11,7], orders=[2,4]):
    '''
    Moja implementacija
    '''
    pass
    #%%