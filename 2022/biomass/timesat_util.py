#%%
from os import path
import numpy as np
import pandas
from pathlib import Path

#%%
class Timesat_tts:
    '''
    Čitanje .tts fileova
    '''

    def __init__(self, filename):
        fn_ndx = Path(filename).with_suffix('.ndx')   
        index = np.fromfile(fn_ndx.open('rb'), dtype=np.int64)
        self.index = index.reshape(-1, 3) 
        self.nseries = self.index.shape[0] 

        with open(filename,'rb') as fid:
            # fid = open(filename,'rb')
            d=np.fromfile(fid,dtype=np.int32,count=6)
            self.nyears,self.nptperyear,self.rowstart,self.rowstop,self.colstart,self.colstop=[int(i) for i in d]
            self.nptperseries = int(self.index[1,2]-self.index[0,2])//4 - 2# Broj podataka po jednoj seriji

            self.data = np.zeros((self.nseries, self.nptperseries), dtype=np.float32)
            self.rows = np.zeros(self.nseries, dtype=np.int32)
            self.cols = np.zeros(self.nseries, dtype=np.int32)

            for i in range(self.nseries):
                fid.seek(self.index[i,2]-1)
                rr = np.fromfile(fid, dtype=np.int32, count=2)
                self.rows[i], self.cols[i] = rr
                data = np.fromfile(fid, dtype=np.float32, count=self.nptperseries)
                self.data[i,:] = data

    def decode_data(self, first_year=2000, season_offset=1):
        '''
        first_year - početna godina - prva sezone 
        season_offset - koliko ih prvo preskočimo
        season_stride - koliko ih dalje preskačemo - nije baš najspretnije rečeno ....
        za cmplx način: season_offset=1, season_stride=2 !!! Ovo ovdje NIJE implementirano
        za simple način: season_offset=1, season_stride=1
        output: pandas.DataFrame
        '''
        season_stride=1
        nyears=(self.nyears-2*season_offset)//season_stride
        
        ndpt=365//self.nptperyear+1    # broj dana za svaku točku
        #out_data=np.zeros((nyears*self.nptperyear,4))
        sid=[]
        year=[]
        day=[]
        value=[]
        k=0 # row u izlaznom fileu
        nseries, npoints=self.data.shape[0:2]
        npt = self.nptperyear
        jstart = npt*season_offset
        jend = jstart + npt*nyears
        jlen = jend-jstart
        for i in range(nseries):            
            lvalue = self.data[i,jstart:jend]
            lsid = np.zeros_like(lvalue,dtype=np.int32) + self.rows[i]
            lday = (np.arange(jlen)%npt+1) * ndpt -ndpt//2
            lyear = np.arange(jlen)//npt + first_year

            sid.append(lsid)
            year.append(lyear)
            day.append(lday)
            value.append(lvalue)

        df = pandas.DataFrame(dict(
                NPOINT = np.concatenate(sid),
                YEAR = np.concatenate(year),
                DAY = np.concatenate(day),
                VALUE = np.concatenate(value)))

        return df
#%%
class Timesat_Tpa:
    '''
    Čitanje i dekodiranje .tpa file-ova (rezultati iz TIMESATa)
    '''
    data=None
    nseries=0
    nyears=0
    nptperyear=0
    rowstart=0
    rowstop=0
    colstart=0
    colstop=0
    
    def __init__(self, filename):    
        fn_ndx = Path(filename).with_suffix('.ndx')   
        index = np.fromfile(fn_ndx.open('rb'), dtype=np.int64)
        self.index = index.reshape(-1, 4) 
        self.nseries = self.index.shape[0]

        with open(filename,'rb') as fid:
            # fid = open(filename,'rb')
            d=np.fromfile(fid,dtype=np.int32,count=6)
            self.nyears,self.nptperyear,self.rowstart,self.rowstop,self.colstart,self.colstop=[int(i) for i in d]
            #self.nseries=(self.rowstop-self.rowstart+1)*(self.colstop-self.colstart+1)

            #self.npoints=self.nyears*self.nptperyear
            self.data=np.zeros((self.nseries,self.nyears,13),dtype=np.float32)
            self.rows=np.zeros(self.nseries,dtype=np.int32)
            self.cols=np.zeros(self.nseries,dtype=np.int32)
            self.nseasons=np.zeros(self.nseries,dtype=np.int32)
                        
            for i in range(self.nseries):
                #print i
                fid.seek(self.index[i,3]-1)
                rr=np.fromfile(fid,dtype=np.int32,count=3)
                #print rr
                #if len(rr)==0:
                #    break
                self.rows[i], self.cols[i], self.nseasons[i] = rr
                #if self.nseasons[i]>self.nyears:
                #    print(f'Series {i} has {self.nseasons[i]} seasons.')
                for j in range(self.nseasons[i]):
                    data = np.fromfile(fid,dtype=np.float32,count=13)
                    #if j<self.nyears:
                    self.data[i,j,:] = data
                    
    def decode_data(self,first_year=2000, season_offset=1, season_stride=2):
        '''
        first_year - početna godina - prva sezone 
        season_offset - koliko ih prvo preskočimo
        season_stride - koliko ih dalje preskačemo - nije baš najspretnije rečeno ....
        za cmplx način: season_offset=1, season_stride=2
        za simple način: season_offset=1, season_stride=1
        output: pandas.DataFrame
        '''
        ns=(self.nyears-season_offset)//season_stride
        ndpt=365//self.nptperyear+1    # broj dana za svaku točku
        out_data=np.zeros((self.nseries*ns,16))
        k=0 # row u izlaznom fileu
        nwrong=0 # broj loših pixel/sezona
        ni,nj=self.data.shape[0:2]
        for i in range(ni): #ni serije
            #print 'POINT %d:'%i,
            for j in range(nj):    #godine
                dd=self.data[i,j,:]    
                s=dd[[0,1,4]].astype(int)//self.nptperyear+1 # sezona u brojanju kao u ulaznom fileu                            
                #print '\n    sezona %d: (%d,%d)'%(s,dd[0],dd[1]),
                #print (dd[0]>0), (s>season_offset), ((s-season_offset-1)%season_stride==0)
                if (dd[0]>0) and (s[0]>season_offset) and ((s[0]-season_offset-1)%season_stride==0):                   
                    if s.min()==s.max(): 
                        valid=1
                    else:
                        valid=0
                        nwrong += 1
                    s=(s[0]-season_offset)//season_stride   # sezona u brojanju kao u izlaznom fileu
                    y=first_year+s-1  # godina                                 
                    #print ' (%d,%d)'%(s,y),
                    dd[[0,1,4]] = (dd[[0,1,4]] % self.nptperyear) * ndpt - 7 # U Julian day = - 8 + 1
                    dd[2] *= ndpt
                    npix=self.rows[i]   # Ovdje pretpostavljam da nemamo colone !!!!
                    dd=np.hstack((npix,y,valid,dd))
                    out_data[k,:]=dd
                    k += 1
                    
        out_data=out_data[0:k,:]            
        params=['SOS','EOS','LOS','BLV','MOS','LEF','SAM','ROI','ROD','LSI','SSI','SSV','ESV']
        out=pandas.DataFrame(data=out_data,columns=['NPOINT','YEAR','VALID']+params)
        #out=out.convert_objects()
        out.NPOINT=out.NPOINT.astype(int)
        out.YEAR=out.YEAR.astype(int)
        out.VALID=out.VALID.astype(int)
        print(f"Ukupno pixel/sezona: {k}, loših: {nwrong}, postotak loših: {100.0*nwrong/k:3.2f}")
        return out
        
        """
            'SOS' ... Start of season (Julian day)
            'EOS' ... End of season (Julian day)
            'LOS' ... Length of season (days)
            'BLV'  ... base level; given as the average of the left and right minimum values.
            'MOS' ... time for the mid of the season; computed as the mean value of the times for which,
                  ... respectively, the left edge has increased to the 80 % level and the right edge has decreased
                  ... to the 80 % level.
            'LEF' ... largest data value for the fitted function during the season; may occur at a
                  ... diferent time compared with 5
            'SAM' ... seasonal amplitude; diference between the maximum value and the base level.
            'ROI' ... rate of increase at the beginning of the season; calculated as the ratio of the
                  ... diference between the left 20 % and 80 % levels and the corresponding time diference.
            'ROD' ... rate of decrease at the end of the season; calculated as the absolute value of the
                  ... ratio of the diference between the right 20 % and 80 % levels and the corresponding time
                  ... di?erence. The rate of decrease is thus given as a positive quantity.
            'LSI' ... large seasonal integral; integral of the function describing the season from the season
                  ... start to the season end.
            'SSI' ... small seasonal integral; integral of the di?erence between the function describing the
                  ... season and the base level from season start to season end.
            'SSV' ... value for the start of the season
            'ESV' ... value for the end of the season
        """

#%%
class Timesat_Input:
    '''
    Klasa za izradu ulaznog filea za TIMESAT
    '''
    nptperyear = 23 # broj snimaka u godini
    skip_days = 16  # broj dana razmaka između dva snimka u godini
    # datumi koje zamijenjujem drugim datumima
    dates_replace = [('2000-01-01','2001-01-01'),
                     ('2000-01-17','2001-01-17'),
                     ('2000-02-02','2001-02-02')]
    #@classmethod
    #def from_txt(cls,filename):
    def __init__(self):
        pass
    
    def load_from_txt(self,filename,cols=None):
        '''        
        Učitavanje tekst filea sa modis podacima. 
        cols -- lista imena kolona u file-e, default=
            ['spatial_id','time_id','ndvi','evi','viq','c1','c2','c3','c4']
        '''
        if cols==None:
            cols=['spatial_id','time_id','ndvi','evi','viq','c1','c2','c3','c4']
        df=pandas.read_table(filename,names=cols)
        self.load_from_df(df)
        #self.unique_ids = pandas.Series(np.sort(df['spatial_id'].unique()))
        #self.data = df
        
    def load_from_df(self,df,calc_weight=None):
        '''
        Učitavanje modis podataka iz DataFrame-a
        '''
        # MODIS VIQ pretvorimo u weight-ove od 0 do 15 (0 - nema podatka, 15 - najbolji podatak)
        if calc_weight == 'MODIS_VIQ':
            df['weight'] = 15-np.right_shift(np.bitwise_and(df.viq.values,0b111100),2);
        self.time_index = pandas.DatetimeIndex(df.time_id)        
        self.data = df
        
    def get_dates_complex(self, end_year, start_year=2000):
        '''
        Vraća datume redom za kompleksni Zrinkin način
        '''        
        days = np.arange(1,366,self.skip_days) - 1
        ndays = len(days)
        hdays = ndays//2 + 1 

        NOY = 0
        god=np.array([],dtype=int); dan=np.array([],dtype=int);
        for g in range(start_year,end_year+1):
            if g==start_year:
                gg=np.tile(g,ndays*2) #repmat(g,1,23*2);
                dd=np.r_[days[::-1],days] #[dani(23:-1:1) dani(1:23)];        
            else:
                gg=np.r_[np.tile(g-1,hdays), np.tile(g,2*ndays-hdays)] #repmat(g-1,1,12) repmat(g,1,11+23)];
                dd=np.r_[days[:hdays-2:-1], days[hdays-2::-1], days]#[dani(23:-1:12) dani(11:-1:1) dani];        

            god=np.r_[god,gg]
            dan=np.r_[dan,dd]
            NOY=NOY+2;
        god=np.r_[god, np.tile(end_year,ndays)]#repmat(2012,1,23)];
        dan=np.r_[dan, days[::-1]]
        NOY=NOY+1;
        
        fyears=np.array([np.datetime64('{}-01-01'.format(y)) for y in god])
        dates = fyears + dan

        for d1,d2 in self.dates_replace:
            dates[dates==np.datetime64(d1)]=np.datetime64(d2)
        return dates, NOY
    
    def get_dates_simple(self, end_year, start_year=2000):
        '''
        Vraća datume redom - jedonstavnim načinom (samo se prva i zadnja godina dupliciraju)
        '''
        years = np.concatenate(([start_year],np.arange(start_year,end_year+1),[end_year]))
        days = np.arange(1,366,self.skip_days) - 1
        fyears=np.array([np.datetime64('{}-01-01'.format(y)) for y in years])
        dates = fyears.reshape((-1,1)) + days.reshape((1,-1))
        dates = dates.flatten()
        for d1,d2 in self.dates_replace:
            dates[dates==np.datetime64(d1)]=np.datetime64(d2)
        return dates, len(years)
        
    def write_timesat(self, filename, bands=['ndvi','evi'], kind='complex', spatial_ids=None):
        '''
        Ispisuje ASCII ulazne file-ove za TIMESAT
        Usput snima i poredak spatial_id-eva 
        '''
        import os  
        from datetime import datetime

        if spatial_ids is None:
            df = self.data
            end_year = self.time_index.year.max()
        else: #ostavimo samo one spatial_id-eve koji su u spatial_id ...
            ind = self.data.spatial_id.isin(spatial_ids)            
            df = self.data[ind]
            end_year = self.time_index[ind].year.max()

        fld,fname = os.path.split(filename)
        fbase,fext = os.path.splitext(fname)
        
        bands.append('weight')
        if kind=='complex':
            dates, NOY = self.get_dates_complex(end_year)
        elif kind=='simple':
            dates, NOY = self.get_dates_simple(end_year) 
        sdates =[d.astype(datetime).strftime('%Y-%m-%d') for d in dates]
        
        for b in bands:
            if b=='weight':
                mv=0
                frm='%d'
            else:
                mv=-9999.0
                frm='%.4f'
                
            dfp = df.pivot('spatial_id','time_id',b)
            
            ff = os.path.join(fld,'{}_{}.txt'.format(fbase,b))
            fid = open(ff,'wt')
            fid.write('{} {} {}\n'.format(NOY,self.nptperyear,len(dfp)))
            fid.flush()
            
            dfp[sdates].to_csv(fid, sep=' ', header=False, na_rep=mv, float_format=frm, index=False)
                
            fid.close()
        
        # Odmah snimim i unique_ids
        ff = os.path.join(fld,'{}_{}.txt'.format('spatial_ids',fbase))
        dfp.index.values.tofile(ff,sep='\n',format='%d')

# %%
