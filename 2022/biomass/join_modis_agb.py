#%%
import pandas
from pathlib import Path
import numpy as np
import geopandas, rasterio, rasterio.features
import pickle
import shapely.geometry

proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'
# Working folder
fld_timesat = Path(r'D:\DATA\ETHIOPIA2022\TIMESAT\work')
# Output data from TIMESAT
fn_timesat = fld_timesat/'eth2022_v2_fit.parquet'
# Calculated yearly EVI
fn_timesat_yearly = fld_timesat/'evi_yearly.parquet'
# AGB polygonds in MODIS projection
fn_agb_polys = Path(r'D:\DATA\ETHIOPIA2022\GLOBAL_AB\global_agb_modis.gpkg')
# georeference data for MODIS tiffs
fn_modis_georef = fld_timesat.parent/'modis4timesat_georef.pickle'
# pixel ids
fn_modis_sids = fld_timesat.parent/'modis4timesat_sids.pickle'
# output DataFrame with joined data
fn_modis_agb_df = fld_timesat/'modis_agb.df'
#%% Compute yearly EVI sum from smoothed EVI
def yearly_evi():
    df = pandas.read_parquet(fn_timesat)
    dfy = df.groupby(['spatial_id','object_id','YEAR'])['VALUE'].sum()/10000.0
    dfy = dfy.reset_index()
    dfy.to_parquet(fn_timesat_yearly)

#%%
def join_modis_agb():
    '''
    Joining of EVI yearly data with AGB data
    '''
    df = geopandas.read_file(fn_agb_polys)    
    modis_georef = pickle.load(open(fn_modis_georef,'rb'))
    out_shape = modis_georef['height'], modis_georef['width']
    transform = modis_georef['transform']

    # Rasterizing ids of AGB polygons 
    shapes = ((r.geometry, r.pid) for i,r in df.iterrows())
    pids = rasterio.features.rasterize(shapes, out_shape, transform=transform, dtype=np.int32)

    # Saving of rasterized polygons ids for check
    profile = modis_georef
    profile['count'] = 1
    profile['dtype'] = 'int32'
    profile['crs'] = proj4_modis
    with rasterio.open(fn_agb_polys.with_suffix('.tif'),'w',**profile) as dst:
        dst.write(pids,1)

    # Joining modis pixel ids with AGB polygon ids
    sids = pickle.load(open(fn_modis_sids,'rb'))
    agb_ids = pids.ravel()[sids.spatial_id.values]
    sids['agb_ids'] = agb_ids
    del sids['object_id']

    # Reading of yearly EVI values and joining with pixel ids and AGB polygon ids
    dfy = pandas.read_parquet(fn_timesat_yearly)
    dfy = dfy.join(sids.set_index('spatial_id'), on='spatial_id')
    dfy.to_parquet(fn_timesat_yearly.with_name(fn_timesat_yearly.stem+'_join.parquet'))

#%% Joining modelling ready data
def join_for_modelling():
   
    # read AGB data and MODIS data
    dfagb = geopandas.read_file(fn_agb_polys)
    dfmodis = pandas.read_parquet(fn_timesat_yearly.with_name(fn_timesat_yearly.stem+'_join.parquet'))    
    dfmodis = dfmodis.query('agb_ids>0')

    # yearly mean of EVI data per AGB polygon
    df = dfmodis.groupby(['agb_ids', 'YEAR'])['VALUE'].mean().reset_index()
    df.columns=['agb_id','year','evi']
    df['agb']=np.nan

    # Filling dataset with AGB data
    dfagb = dfagb.set_index('pid')
    for i,r in df.iterrows():
        if r.year>=1993 and r.year<=2012:        
            agb = dfagb.loc[r.agb_id][f'Y{int(r.year)}']
            df.loc[i,'agb']=agb

    # Exporting joined data
    df.to_pickle(fn_modis_agb_df)
    df.to_csv(fn_modis_agb_df.with_suffix('.csv'),sep='\t')
#%% Ploting for checks
def plots():
    import matplotlib.pyplot as plt

    df = pandas.read_pickle(fn_modis_agb_df)
    df.evi.hist()
    plt.close('all')
    df.agb.hist()
    plt.close('all')
    plt.hist(np.log(df.agb))
    plt.close('all')
    plt.plot(df.evi,df.agb,'r.')
    plt.close('all')
    plt.plot(df.evi,np.log(df.agb),'r.')
    plt.close('all')
    plt.plot(np.log(df.evi),np.log(df.agb),'r.')
# %%

if __name__=='__main__':
    yearly_evi()
    join_modis_agb()
    join_for_modelling()