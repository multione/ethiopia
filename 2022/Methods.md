
## 250m biomass
For biomass data dataset from this article is used: Liu, Y.Y., A.I.J.M. van Dijk, R.A.M. de Jeu, J.G. Canadell, M.F. McCabe, J.P. Evans and G. Wang (2015) Recent reversal in loss of global terrestrial biomass, Nature Climate Change 5, doi: 10.1038/NCLIMATE2581. Dataset can be found on this address: https://wald.anu.edu.au/data_services/data/global-above-ground-biomass-carbon-v1-0/. Data represents above-ground biomasss carbon mass (AGBc) in tons per hectare at 10km spatial resolution with assumption that AGB = 2*AGBc. 
EVI data are from MODIS dataset MOD13Q1, 16 days composits on 250m spatial resolution. EVI is first smoothed and filtered with TIMESAT application (https://web.nateko.lu.se/timesat/timesat.asp). Then it's merged with AGB data and yearly EVI mean of all modis pixel for each 10km AGB polygon is calculated.
This dataset is used to make relation between yearly EVI and AGB, according to literature relation between them can be modeled with this function: AGB = a*arctan(b*(EVIy-c))+d, where EVIy is yearly EVI mean. After fitting parameters this can be used to assess AGB for all MODIS pixels.

## 30m biomass
For 30m resolution biomass GLAD dataset is used (https://glad.umd.edu/dataset). It's 30m resolution, 16 days composits (compatible with MODIS MOD13Q1) of Landsat 5, Lansat 7 and Landsat 8 products ranging from 2000 to 2020. Smoothed MODIS EVI (smoothed with TIMESAT software) is resampled to GLAD 30m grid with cubic spline inrterpolation and in combination with EVI calculated from GLAD dataset a smoothed 30m, 16 days composites of EVI are obtained. Now using this EVI and modelled function AGB at 30m resolution for Ale and Sekela is calculated.

## Erosion
Erosion assesment is based on RUSLE equation. GEE engine aplication SoilWatch (https://github.com/SoilWatch/soil-erosion-watch) is modified to produce yearly erosion risk for years from 2016 to 2021. For this purpose RUSLE equation is rewriten as: A = (R * K * LS)/(V * L), where V is vegetation factor V=1/C, L is landscape factor L=1/P. 
### Vegetation Factor V
Vegetation factor V is calculated as Vm=(10 * (1-BSf) * FCover_m).exp(); V=Vm.mean(), where 10*(1-BSf) is vegetation cover frequency and FCover is fraction of vegetation cover.

### Landscape Factor L
Landscape factor is calculated as L = 1 + (Sf_B8/DNMax).sqrt() where Sf_B8 is the result of a convolution using 3x3 anisotropic edge detector Sobel filter on Sentinel-2's NIR band. The convolution output is normalized by dividing by the maximum potential reflectance value of the B8 band (10000).

### Rainfall erosivity factor R
Rainfall erosivity accounts for the combined effect of rainfall duration, magnitude and intensity, as well as taking into account the frequency of erosive events over a longer time period. The global rainfall erosivity dataset produced by Panagos et al., 2017 (https://esdac.jrc.ec.europa.eu/content/global-rainfall-erosivity) was used for this purpose.

### Soilerodibility factor K
The K-factor expresses the susceptibility of a soil to erode, is related to soil properties such as organic matter content, soil texture, soil structure and permeability. The global equation for the soil erodibility factor was taken from Renard et al., 1997: K=0.001317*[2.1 * 1E-4 * M^1.14 (12-OM) + 3.25(s-2) + 2.5(p-3)], where M=(Msilt + Mvfs)*(100-mc)  

### Slope length (L) and steepnes factor (S)  
The combined LS-factor describes the effect of topography on soil erosion. We applied Panagos et al. 2015's approach to derive it.