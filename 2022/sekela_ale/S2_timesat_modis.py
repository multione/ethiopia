#%%
from pathlib import Path

from timesat_util import TimesatTTS
import rasterio as rio
from rasterio.features import rasterize
import geopandas
import pandas
import numpy as np
#from timesat_util import Timesat_Input, Timesat_Tpa
import pickle
from netCDF4 import Dataset
from datetime import datetime, timedelta
import itertools, pyproj
from scipy.interpolate import griddata

from utils import params, create_copy_fusion_nc


#%% Preparation of EVI values for TIMESAT aplication
def prepare_timesat_raw(wsname='Ale'):

    fld_modis = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_ale_sekela')
    input_file = f'D:\DATA\ETHIOPIA2022\GLAD\glad_{wsname}.nc'
    output_folder = Path(r'D:\DATA\ETHIOPIA2022\TIMESAT\ale_sekela')

    with Dataset(input_file) as nc:
        year = nc['year'][:].data
        t16 = nc['t16'][:].data
        nyears = len(np.unique(year))
        nt16 = 23

    ntimes = len(year)
    modis_evi = None
    # For every date
    for i in range(ntimes):
        date = datetime(year[i],1,1) + timedelta(days=16*int(t16[i]))
        # Complete start of 2000 year with start of 2001
        if date<datetime(2000,2,18):
            date =datetime(2001,1,1) + timedelta(days=16*int(t16[i]))

        # Read MODIS EVI data
        modis_fn = fld_modis/f'modis_{wsname}_{date:%Y%m%d}.tif'
        print (modis_fn)
        with rio.open(modis_fn) as src:
            if modis_evi is None:
                height = src.profile['height']
                width = src.profile['width']
                modis_evi = np.zeros((height, width, ntimes), dtype=np.int16)
                modis_qa = np.zeros_like(modis_evi)

            modis_data = src.read(2)   
            modis_evi[:,:,i] = modis_data

            modis_data_qa = src.read(3)
            modis_qa[:,:,i] = modis_data_qa

    # Writing TIMESAT input file
    input_data = dict(modis_raw = modis_evi, modis_raw_qa=modis_qa)
    for var in ['modis_raw','modis_raw_qa']:
        # var = 'modis_raw'
        print(var)
        data = input_data[var]
        fid = open(output_folder/f"{wsname}_tsinput_{var}.txt", 'wt')        
        fid.write(f'{nyears+2} {nt16} {width*height}\n')
        for iy in range(height):
            print(f'{iy}/{height}')
            for ix in range(width):
                values = data[iy, ix, :] #nc[var][iy,ix,:].data
                # Copying of first year at begining and last year to the and
                values = np.r_[values[:nt16],values,values[-nt16:]] 
                fid.write(' '.join(values.astype(str)))            
                fid.write('\n')
                
        fid.close()    

#%% 
def timesat_modis_raw_to_nc(wsname='Ale'):
    
    fld_modis = Path(r'D:\DATA\ETHIOPIA\MODIS\ws_ale_sekela')
    input_file_tts = fr"D:\DATA\ETHIOPIA2022\TIMESAT\ale_sekela\{wsname}_raw_fit.tts"
    input_file_nc = fr'D:\DATA\ETHIOPIA2022\GLAD\glad_{wsname}.nc'
    output_file_nc = fr'D:\DATA\ETHIOPIA2022\FUSION\modis_timesat_{wsname}_raw.nc'
    ntpy = 23
    ns = 21

    proj4_modis = '+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +a=6371007.181 +b=6371007.181 +units=m +no_defs'

    # Reading properties of MODIS files
    fn_modis=next(fld_modis.glob(f'*_{wsname}_*.tif'))
    with rio.open(fn_modis) as src:
        width_raw = src.profile['width']
        height_raw = src.profile['height']
        transform_raw = src.profile['transform']

    # Read and decode TIMESAT output data
    tts = TimesatTTS(input_file_tts)
    tts_data = tts.decode_data()
    nt = tts_data.shape[1]

    # Reshaping evi data to height x width x ntimes
    modis_raw_evi = np.zeros((height_raw*width_raw,nt)) + np.nan
    modis_raw_evi[tts.rows-1,:] = tts_data
    modis_raw_evi = modis_raw_evi.reshape((height_raw,width_raw,nt))

    # Calculating x,y coordinates
    rows, cols= zip(*itertools.product(range(height_raw), range(width_raw)))
    modis_x, modis_y = rio.transform.xy(transform_raw, rows, cols)
    modis_xy = np.c_[modis_x, modis_y]

    # Copy data to new netCDF 
    create_copy_fusion_nc(input_file_nc, output_file_nc, ns, seasonal_params=False)
    # Append new data to it
    nc = Dataset(output_file_nc,'a')

    # Translate coordinates of MODIS to GLAD crs
    glad_lon = nc['lon'][:].data
    glad_lat = nc['lat'][:].data
    crs_glad = pyproj.crs.CRS.from_string(nc.crs)
    crs_modis = pyproj.crs.CRS.from_string(proj4_modis)
    tr = pyproj.Transformer.from_crs(crs_glad, crs_modis, always_xy=True)
    glad_x, glad_y = tr.transform(glad_lon, glad_lat)
    glad_xy = np.c_[glad_x.reshape(-1), glad_y.reshape(-1)]

    
    modis_qa = nc['modis_qa']
    modis_sids = nc['modis_sids']

    spatial_ids = np.arange(height_raw * width_raw).reshape((height_raw,width_raw))
    modis_sids[:,:] = griddata(modis_xy, spatial_ids.reshape(-1), glad_xy, 'nearest', -1).reshape((nc.height,nc.width))

    modis_evi_nn = np.full((nc.height,nc.width,nt),np.nan,dtype=np.float32)
    modis_evi_bl = np.full((nc.height,nc.width,nt),np.nan,dtype=np.float32)
    modis_evi_cb = np.full((nc.height,nc.width,nt),np.nan,dtype=np.float32) 

    # Interpolating smothed MODIS EVI data to GLAD grid
    for i in range(modis_raw_evi.shape[2]):
        # i=0
        print(i)
        mrn = modis_raw_evi[:,:,i]
        ind = np.isfinite(mrn)
        modis_xy_i = modis_xy[ind.reshape(-1),:]
        modis_evi_nn[:,:,i] = griddata(modis_xy_i, mrn[ind], glad_xy, 'nearest').reshape(nc.height,nc.width)
        modis_evi_bl[:,:,i] = griddata(modis_xy_i, mrn[ind], glad_xy, 'linear').reshape(nc.height,nc.width)
        modis_evi_cb[:,:,i] = griddata(modis_xy_i, mrn[ind], glad_xy, 'cubic').reshape(nc.height,nc.width)
        
    nc['modis_evi_nn'][:] = modis_evi_nn
    nc['modis_evi_bl'][:] = modis_evi_bl
    nc['modis_evi_cb'][:] = modis_evi_cb

    nc.close()

if __name__=='__main__':
    # 1. Prepare timesat files from EVI values
    prepare_timesat_raw('Ale')
    prepare_timesat_raw('Sekela')

    # 2. run timesat
    #>CD D:\DATA\ETHIOPIA2022\TIMESAT\ale_sekela
    #>C:\Utils\timesat33\timesat_fortran\main\TSF_process.exe ale_raw.set
    #>C:\Utils\timesat33\timesat_fortran\main\TSF_process.exe sekela_raw.set

    # 3. Read data from .tts and save to .nc
    timesat_modis_raw_to_nc('Ale')
    timesat_modis_raw_to_nc('Sekela')