#%%
import pandas
import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
import rasterio
from pathlib import Path

ver='v8'    # version of 'fusion' algorithm
fld_output = Path(r'D:\DATA\ETHIOPIA2022\BIOMASS\maps')
fld_output.mkdir(parents=True, exist_ok=True)

# parametri i funkcija izjednačenja sume godišnjeg EVIja i biomase u Mg/ha
# parameters and fited function of yearly EVI and AGB [Mg/ha] (from biomass/model_fit.py)
popt = [75.65578398, 1.76208867, 9.95634832, 132.04593876]
def f(x, a,b,c,d):
    return a*np.arctan(b*(x-c))+d

#%%
def biomass(wsname):
    # Input netCDF file
    fn_input = fr'D:\DATA\ETHIOPIA2022\FUSION\fusion_{wsname}_{ver}.nc'
    evi_var='evi_cb'    # which EVI variable to use (interpolation)

    nc = Dataset(fn_input)

    profile = dict(driver='COG', nodata=-9999.0, dtype=rasterio.float32, crs='EPSG:4326', count=1)
    width = nc.width; height=nc.height
    transform = rasterio.transform.Affine.from_gdal(*nc.transform_gdal)
    profile['transform'] = transform
    profile['width'] = width; profile['height'] = height
    profile['compress'] = 'DEFLATE'

    years = np.unique(nc['year'][:])
    ncyears = nc['year'][:].data
    for year in years:
        # year=years[0]
        ind = ncyears==year
        evi = nc[evi_var][:,:,ind]
        evi = np.ma.filled(evi,np.nan).sum(axis=2)/10000    # calculate yearly EVI
        bm = np.ma.filled(f(evi,*popt), profile['nodata'])  # calculate biomass
        fn = fld_output/f'biomass_{wsname}_{year}.tif'
        print(fn)
        with rasterio.open(fn,'w', **profile) as dst:
            dst.write(bm,1)
    
    nc.close()

if __name__=='__main__':
    biomass('Ale')
    biomass('Sekela')