from netCDF4 import Dataset
params=['SOS','EOS','LOS','BLV','MOS','LEF','SAM','ROI','ROD','LSI','SSI','SSV','ESV']

def create_copy_fusion_nc(input_file, output_file, nseasons, seasonal_params=True):

    with Dataset(input_file) as ncin, Dataset(output_file,'w', format='NETCDF4') as nc:
        # copy global attributes all at once via dictionary
        nc.setncatts(ncin.__dict__)
        # copy dimensions
        for name, dimension in ncin.dimensions.items():
            nc.createDimension(
                name, (len(dimension) if not dimension.isunlimited() else None))
        # copy all file data except for the excluded
        for name, variable in ncin.variables.items():
            if (not seasonal_params) or  (seasonal_params and (name in ['objectid','modis_sids'])):
                v = nc.createVariable(name, variable.datatype, variable.dimensions)
                # copy variable attributes all at once via dictionary
                nc[name].setncatts(ncin[name].__dict__)
                nc[name][:] = ncin[name][:]
        
        if seasonal_params:
            nc.createDimension('season', nseasons)
            for p in params:
                nc.createVariable(p, 'f4', ('y','x','season'))
            nc.createVariable('YEAR','i2', ('y','x','season'))


class SpecialLinReg():
    
    def __init__(self) -> None:
        pass


    def fit(self, XX, yy, sample_weight=None):
        from scipy.optimize import minimize
        import numpy as np
        
        model = lambda b, X: np.dot(X,b)
        if sample_weight is None:
            obj = lambda b, Y, X: np.sum(np.abs(Y-model(b,X))**2)
        else:
            obj = lambda b, Y, X: np.dot(np.abs(Y-model(b,X))**2, sample_weight)

        nfeatures = XX.shape[1]
        bnds= [(None,None)]*nfeatures
        bnds[0] = (0.1, None)

        xinit = [0.1]*nfeatures

        res = minimize(obj, args=(yy,XX), x0=xinit, bounds=bnds)
        self.res = res
        self.model = model

        return self

    def predict(self,X):
        return self.model(self.res.x, X)



def savitzky_golay_filtering(timeseries, wnds=[11, 7], orders=[2, 4], debug=True):       
    import pandas as pd
    import numpy as np
    from scipy.signal import savgol_filter                              
    interp_ts = pd.Series(timeseries).interpolate(method='linear', limit=9999)
    smooth_ts = interp_ts                                                                                              
    wnd, order = wnds[0], orders[0]
    F = 1e8 
    it = 0                                                                                                             
    while True:
        smoother_ts = savgol_filter(smooth_ts, window_length=wnd, polyorder=order, mode='constant', cval=0)                                     
        diff = (smoother_ts - interp_ts).clip(0)
        sign = diff > 0                                                                                                                       
        W = 1 - diff / np.max(diff)                                                         
        wnd, order = wnds[1], orders[1]                                                                            
        fitting_score = np.sum(diff * W)                                                                       
        print (it, ' : ', fitting_score)
        if fitting_score > F:
            break
        else:
            F = fitting_score
            it += 1        
        smooth_ts = smoother_ts #* sign + interp_ts * (1 - sign)
    if debug:
        return smooth_ts, interp_ts
    return smooth_ts