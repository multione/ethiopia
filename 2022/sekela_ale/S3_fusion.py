#%%
from os import replace
from netCDF4 import Dataset
from timesat_util import TimesatTTS
import numpy as np
from pathlib import Path
from sklearn import linear_model
from sklearn import metrics
from utils import SpecialLinReg, savitzky_golay_filtering
from scipy.signal import savgol_filter
from matplotlib import pyplot as plt

# Copy netCDF file and create needed variables
def create_copy_fusion_nc(input_file, output_file):

    with Dataset(input_file) as ncin, Dataset(output_file,'w', format='NETCDF4') as nc:
        # copy global attributes all at once via dictionary
        nc.setncatts(ncin.__dict__)
        # copy dimensions
        for name, dimension in ncin.dimensions.items():
            nc.createDimension(
                name, (len(dimension) if not dimension.isunlimited() else None))
        # copy all file data except for the excluded
        for name, variable in ncin.variables.items():
            if name in ['lon','lat','year','t16', 'objectid','modis_sids']:
                v = nc.createVariable(name, variable.datatype, variable.dimensions)
                # copy variable attributes all at once via dictionary
                nc[name].setncatts(ncin[name].__dict__)
                nc[name][:] = ncin[name][:]
                
        
        ndvi_nn = nc.createVariable("evi_nn", "i2", ("y", "x", "time"), zlib=True)
        ndvi_bl = nc.createVariable("evi_bl", "i2", ("y", "x", "time"), zlib=True)
        ndvi_cb = nc.createVariable("evi_cb", "i2", ("y", "x", "time"), zlib=True)

        nc.createVariable("weights_nn", "i2", ("y", "x", "time"), zlib=True)
        nc.createVariable("weights_bl", "i2", ("y", "x", "time"), zlib=True)
        nc.createVariable("weights_cb", "i2", ("y", "x", "time"), zlib=True)

        nc.createVariable("evi_score_nn", "i2", ("y", "x"), zlib=True)
        nc.createVariable("evi_score_bl", "i2", ("y", "x"), zlib=True)
        nc.createVariable("evi_score_cb", "i2", ("y", "x"), zlib=True)

#%% Fusion of MODIS and GLAD EVI
def fusion_ndvi_v8(wsname):
    '''
    Fusion of modis and glad evi timeseries
    Modis is now interpolated after TIMESAT smoothing
    For every pixel make linear regression from smothed modis evi    
    '''    
    ver = 'v8'
    input_file = fr'D:\DATA\ETHIOPIA2022\GLAD\glad_{wsname}.nc'
    input_modis = fr'D:\DATA\ETHIOPIA2022\FUSION\modis_timesat_{wsname}_raw.nc'
    output_file = fr'D:\DATA\ETHIOPIA2022\FUSION\fusion_{wsname}_{ver}.nc'

    create_copy_fusion_nc(input_file, output_file)

    nc = Dataset(output_file,'a')

    # For every interpolation type
    for interp in ['nn','bl','cb']:     
        print(interp)
       
        with Dataset(input_file,'r') as ncin:
            width = ncin.width; height=ncin.height
            input_evi = ncin['evi'][:].data
            input_evi = np.ma.masked_less_equal(input_evi, -5000)   # mask invalid values of EVI
            qf = ncin['qf'][:]
            weight_evi = np.ones_like(qf, dtype=float)
            # weights for TIMESAT. 1-use value, 0.5-use pixel with half weight, 0-don't use value
            # qf==15 -> clear sky
            weight_evi[np.isin(qf,[11,12,13,14,16,17])] = 0.5  #clear-sky data with indication of cloud/shadow proyximity
            weight_evi[np.isin(qf,[5,6])] = 0  # seasonal data quality issues (topographic shadows and snow cover)
            weight_evi[np.isin(qf,[3,4,7,10])] = 0 #contaminated by clouds and shadows          
            nt = input_evi.shape[2]
                        
        # Read smoothed MODIS EVI
        with Dataset(input_modis) as ncmodis:
            modis_evi = ncmodis[f'modis_evi_{interp}'][:].reshape((-1,nt))

        # Prepare arrays for data
        glad_evi = input_evi.astype(np.float32).filled(np.nan).reshape((-1,input_evi.shape[2]))
        weight_evi = weight_evi.filled(0).reshape((-1,input_evi.shape[2]))
        
        # LinearRegression specialized for this task, taking in account weights
        clf = SpecialLinReg()
        res = np.zeros(nt)
        fusion_evi = np.empty_like(input_evi)
        fusion_evi_weights = np.empty_like(input_evi)
        fusion_score = np.zeros((height, width))
        # For every GLAD pixel
        for i in range(width*height):
            # row, col
            r = i//width; c=i%width
            if (i%1000==0):
                print(f'{i+1}/{width*height}')            

            y = glad_evi[i,:]

            modis_evi_i = modis_evi[i,:]
            modis_evi_i_min = modis_evi_i.min() # minimum modis evi value
            modis_evi_i_max = modis_evi_i.max() # maximum modis evi value

            # Remove all glad evi values that are lower then 0.9*modis_evi_min or higher then 1.1*modis_evi_max
            y[(y<modis_evi_i_min*0.9) | (y>modis_evi_i_max*1.1)] = np.nan
            X = np.c_[modis_evi_i] #, d1, d1**2, d2, d2**2] # , dist_ms**2, dist_ms*modis_ndvi[c,:]] #modis_ndvi[c,:].reshape(-1,1) #np.c_[modis_ndvi[c,:], dist_ms] #, dist_ms**2, dist_ms*modis_ndvi[c,:]]
            
            ind = np.isfinite(y)# & (y>modis_ndvi[i,:].min())            
            XX = X[ind,:]; yy = y[ind]

            # Fitting linear regression, x=modis_evi, y=glad_evi
            clf.fit(XX,yy, sample_weight=weight_evi[i,ind])
            
            # Fusion evi are predicted values of this linear regression
            Y = clf.predict(X)
            fusion_evi[r,c,:] = Y
            
            # Calculate R^2 of this regression
            prd = clf.predict(XX)            
            fusion_score[r,c] = metrics.r2_score(yy,prd)            

            #sres = savitzky_golay_filtering(res, wnds=[23*5, 23*3], orders=[2,4], debug=False)
            #plt.close('all')
            #plt.figure(figsize=(12,5)); plt.plot(X[:,0],'k'); plt.plot(Y,'r'); plt.plot(sres+X[:,0].mean(),'.'); plt.plot(sres+Y,'g--'); plt.plot(y,'k.'); plt.show()
            #plt.savefig(fr'D:\DATA\ETHIOPIA\FUSION\{ver}\sg_{interp}_{i}.png',dpi=200,bbox_inches='tight')
        
        # save resulting evi and r^2 score
        nc[f'evi_{interp}'][:] = fusion_evi.astype(np.int16)
        nc[f'evi_score_{interp}'][:] = fusion_score
        print(' FINISHED.')

    nc.close()

if __name__=='__main__':
    fusion_ndvi_v8('Ale')
    fusion_ndvi_v8('Sekela')