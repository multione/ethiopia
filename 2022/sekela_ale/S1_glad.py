#%%
from datetime import timedelta
import geopandas
import requests
from pathlib import Path
from shapely import geometry

from shapely.ops import transform

# Folder with input GLAD tiles
fld = Path(r'D:\DATA\ETHIOPIA\GLAD\GLAD_TILES')

#%% Convert GLAD data to netCDF format for Ale watershed
def glad2nc_Ale():
    # ROI 
    roi_pilot = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    buffer_m = 500 # buffer around ROI in meters
    # Output file
    outfile = r'D:\DATA\ETHIOPIA2022\GLAD\glad_Ale.nc'

    import geopandas
    from netCDF4 import Dataset
    import rasterio as rio
    import rasterio.features, rasterio.transform
    import itertools
    import numpy as np

    # Read ROI for Ale and add buffer
    shp = geopandas.read_file(roi_pilot)
    shp = shp.query("W_NAME=='Ale'")    
    shp_buffer = shp.buffer(buffer_m)

    # Files with GLAD data for this ROI (only one GLAD tile is needed)
    glad_files = list(fld.glob("glad_035E_08N*.tif"))
    nfiles = len(glad_files)

    with rio.open(glad_files[0]) as src:
        profile = src.profile

    glad_crs = profile['crs']
    
    shp_buffer = shp_buffer.to_crs(glad_crs)
    roi = shp_buffer.unary_union   # buffer in one feature
    roi_bnd = roi.bounds    # boundary

    # Rasterio window for ROI boundary
    window = rio.windows.from_bounds(*roi_bnd, 
            transform=profile['transform'], width=profile['width'], height=profile['height'])
    window = window.round_shape(op='ceil').round_offsets()
    window_transform = rio.windows.transform(window, profile['transform'])

    # Rasterization of microwatershed ids on ROI window
    shp = shp.to_crs(glad_crs)
    shapes = zip(shp.geometry, shp['OBJECTID'])
    objectids = rio.features.rasterize(shapes, 
                        out_shape=(window.height, window.width), 
                        fill=0, 
                        transform=window_transform, 
                        dtype=rio.int16)
    # Output file
    nc = Dataset(outfile, 'w', format="NETCDF4")

    # Create dimensions
    x = nc.createDimension("x", window.width)
    y = nc.createDimension("y", window.height)
    time = nc.createDimension("time", nfiles)

    # Create output variables
    lon = nc.createVariable("lon", "f4", ("y","x"))
    lat = nc.createVariable("lat", "f4", ("y","x"))
    year = nc.createVariable("year", "i4", ("time",))
    t16 = nc.createVariable("t16", "i4", ("time",))
    glad_interval = nc.createVariable("glad_interval", "i4", ("time",))

    objectid = nc.createVariable("objectid", "i4", ("y", "x"), zlib=True)
    evi = nc.createVariable("evi", "i2", ("y", "x", "time"), zlib=True)
    qf = nc.createVariable("qf", "i1", ("y", "x", "time"), zlib=True)

    # MODIS EVI interpolated to GLAD pixels with nearest neighbourhood interpolation
    modis_ndvi_nn = nc.createVariable("modis_evi_nn", "i2", ("y", "x", "time"), zlib=True)  
    # MODIS EVI interpolated to GLAD pixels with bilinear interpolation
    modis_ndvi_bl = nc.createVariable("modis_evi_bl", "i2", ("y", "x", "time"), zlib=True)
    # MODIS EVI interpolated to GLAD pixels with cubic interpolation
    modis_ndvi_cb = nc.createVariable("modis_evi_cb", "i2", ("y", "x", "time"), zlib=True)
    # Quality flags for modis pixels
    modis_qa = nc.createVariable("modis_qa", "i1", ("y", "x", "time"), zlib=True)
    # MIDIS pixels id's
    modis_sids = nc.createVariable("modis_sids", "i4", ("y", "x"), zlib=True)

    nc.crs = profile['crs'].to_string()
    nc.transform_gdal = window_transform.to_gdal()
    nc.width = window.width
    nc.height = window.height
    
    # Coordinates
    rows, cols= zip(*itertools.product(range(window.height), range(window.width)))
    dlons, dlats = rio.transform.xy(window_transform, rows, cols)
    '''
    # Testing 
    tst = geopandas.GeoDataFrame(data={'row':rows, 'col':cols}, geometry=geopandas.points_from_xy(dlons, dlats))
    tst.to_file(r'D:\DATA\ETHIOPIA\GLAD\glad_Ale_coords.shp')
    '''
    # exporting lon, lat
    lon[:] = np.array(dlons).reshape((window.height, window.width))
    lat[:] = np.array(dlats).reshape((window.height, window.width))

    # exporting mws ids
    objectid[:] = objectids

    # Iteration through all GLAD files (every file is one 16day interval)
    for fn in glad_files:        
        print(fn)
        # extractiong of year and period within yeare
        (_,_,_,yy,tt)=fn.with_suffix('').name.split('_')
        # Calculating date, year, day of year ...
        dyear = int(yy); dt16 = int(tt)
        interval = (dyear-1980)*23 + dt16 +1
        t = (dyear - 2000) * 23 + dt16
        year[t] = dyear
        t16[t] = dt16
        glad_interval[t] = interval
        # Reading GLAD data from TIFF file
        with rasterio.open(fn) as src:
            nir = src.read(4, window = window)/40000.0
            red = src.read(3, window = window)/40000.0
            blue = src.read(1, window = window)/40000.0
            
            dqf = src.read(8, window = window)

            # Calculating EVI from GLAD data
            # EVI = 2.5*((NIR - R)/(NIR + 6*R - 7.5*B + 1))
            eviup = 2.5 * (nir - red)
            evidown = nir + 6*red -7.5*blue + 1
            ind = evidown != 0
            devi = np.zeros_like(eviup, dtype=np.int16)-9999
            devi[ind] = eviup[ind]/evidown[ind]*10000

            evi[:,:,t]=devi
            qf[:,:,t]=dqf


    nc.close()   

#%% Convert GLAD data to netCDF for Sekela WS
def glad2nc_Sekela():    
    roi_pilot = r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\MWS_pilot.shp'
    buffer_m = 500
    outfile = r'D:\DATA\ETHIOPIA2022\GLAD\glad_Sekela.nc'

    import geopandas
    from netCDF4 import Dataset
    import rasterio as rio
    import rasterio.features, rasterio.transform
    from rasterio.merge import merge
    import itertools
    import numpy as np

    shp = geopandas.read_file(roi_pilot)
    shp = shp.query("W_NAME=='Sekela'")    
    shp_buffer = shp.buffer(buffer_m)

    glad_files = list(fld.glob("glad_037E*.tif"))
    glad_dates = np.unique(['_'.join(f.with_suffix('').name.split('_')[-2:]) for f in glad_files])
    nfiles = len(glad_files)
    ndates = len(glad_dates)

    with rio.open(fld/'glad_037E_11N_2000_00.tif') as src:
        profile = src.profile

    glad_crs = profile['crs']
    
    shp_buffer = shp_buffer.to_crs(glad_crs)
    roi = shp_buffer.unary_union   #buffer od pola km oko WS-a
    roi_bnd = roi.bounds

    window = rio.windows.from_bounds(*roi_bnd, 
            transform=profile['transform'], width=profile['width'], height=profile['height'])
    window = window.round_shape(op='ceil').round_offsets()
    window_slices = window.toslices()
    window_transform = rio.windows.transform(window, profile['transform'])

    shp = shp.to_crs(glad_crs)
    shapes = zip(shp.geometry, shp['OBJECTID'])
    objectids = rio.features.rasterize(shapes, 
                        out_shape=(window.height, window.width), 
                        fill=0, 
                        transform=window_transform, 
                        dtype=rio.int16)
#%%
    nc = Dataset(outfile, 'w', format="NETCDF4")

    x = nc.createDimension("x", window.width)
    y = nc.createDimension("y", window.height)
    time = nc.createDimension("time", ndates)

    lon = nc.createVariable("lon", "f4", ("y","x"))
    lat = nc.createVariable("lat", "f4", ("y","x"))
    year = nc.createVariable("year", "i4", ("time",))
    t16 = nc.createVariable("t16", "i4", ("time",))
    glad_interval = nc.createVariable("glad_interval", "i4", ("time",))

    objectid = nc.createVariable("objectid", "i4", ("y", "x"), zlib=True)
    evi = nc.createVariable("evi", "i2", ("y", "x", "time"), zlib=True)
    qf = nc.createVariable("qf", "i1", ("y", "x", "time"), zlib=True)

    modis_ndvi_nn = nc.createVariable("modis_evi_nn", "i2", ("y", "x", "time"), zlib=True)
    modis_ndvi_bl = nc.createVariable("modis_evi_bl", "i2", ("y", "x", "time"), zlib=True)
    modis_ndvi_cb = nc.createVariable("modis_evi_cb", "i2", ("y", "x", "time"), zlib=True)
    modis_qa = nc.createVariable("modis_qa", "i1", ("y", "x", "time"), zlib=True)
    modis_sids = nc.createVariable("modis_sids", "i4", ("y", "x"), zlib=True)

    nc.crs = profile['crs'].to_string()
    nc.transform_gdal = window_transform.to_gdal()
    nc.width = window.width
    nc.height = window.height
    # coords
    #row_range, col_range = window.toranges()
    rows, cols= zip(*itertools.product(range(window.height), range(window.width)))
    dlons, dlats = rio.transform.xy(window_transform, rows, cols)
    '''
    tst = geopandas.GeoDataFrame(data={'row':rows, 'col':cols}, geometry=geopandas.points_from_xy(dlons, dlats))
    tst.to_file(r'D:\DATA\ETHIOPIA\GLAD\glad_Ale_coords.shp')
    '''
    lon[:] = np.array(dlons).reshape(window.height, window.width)
    lat[:] = np.array(dlats).reshape(window.height, window.width)

    objectid[:] = objectids

    for date in glad_dates:

        (yy,tt)=date.split('_')
        # Sekela if divided on two GLAD tiles
        fn1 = fld/f'glad_037E_11N_{date}.tif'
        fn2 = fld/f'glad_037E_10N_{date}.tif'
        print(fn1)
        fns=[fn1, fn2]

        # Calculating date, year, ....        
        dyear = int(yy); dt16 = int(tt)
        interval = (dyear-1980)*23 + dt16 +1
        t = (dyear - 2000) * 23 + dt16
        year[t] = dyear
        t16[t] = dt16
        glad_interval[t] = interval

        srcs = [rio.open(fn) for fn in fns]
        data, transform = merge(srcs)
        for src in srcs:
            src.close()
        nir = data[3,window_slices[0],window_slices[1]]/40000.0
        red = data[2,window_slices[0],window_slices[1]]/40000.0
        blue = data[1,window_slices[0],window_slices[1]]/40000.0
        dqf = data[7,window_slices[0],window_slices[1]]

        # Calculating EVI
        eviup = 2.5 * (nir - red)
        evidown = nir + 6*red -7.5*blue + 1
        ind = evidown != 0
        devi = np.zeros_like(eviup, dtype=np.int16)-9999
        devi[ind] = eviup[ind]/evidown[ind]*10000

        evi[:,:,t] = devi
        qf[:,:,t] = dqf


    nc.close()    

if __name__=='__main__':
    glad2nc_Ale()
    glad2nc_Sekela()