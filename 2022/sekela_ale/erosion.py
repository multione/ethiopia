#%%
import numpy as np
from pathlib import Path
import geopandas, pandas
import  rasterio, rasterio.features
from sklearn.linear_model import LinearRegression
#%%

fld_input = Path(r'D:\DATA\ETHIOPIA2022\EROSION\RUSLE\results')
shp_file = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\ethiopia_MWS_wgs84.shp')

#%%
shp = geopandas.read_file(shp_file)
shp = shp.to_crs('EPSG:4326')
res=[]
for wsname in ['Ale','Sekela']:
    #wsname='Ale'
    print(wsname)
    ws = shp.query("W_NAME==@wsname").copy()
    geoms = zip(ws.geometry, ws.OBJECTID)

    files = list(fld_input.glob(f'v2-{wsname}-*.tif'))

    with rasterio.open(files[0]) as src:
        profile=src.profile
        out_shape=(profile['height'], profile['width'])

    objids = rasterio.features.rasterize(geoms, out_shape, transform=profile['transform'], dtype=np.int32)

    for fn in files:
        # fn = files[0]
        year = int(fn.stem.split('-')[-1])
        print(year)
        with rasterio.open(fn) as src:
            data = src.read(1)
        data0 = np.nan_to_num(data)

        for ir, r in ws.iterrows():
            #print(r.OBJECTID)
            ind = objids==r.OBJECTID            
            risk_cnt = np.isfinite(data[ind]).sum()
            if risk_cnt==0:
                risk_avg=0
            else:
                risk_avg = np.nanmean(data[ind])
            all_mean = data0[ind].mean()

            ws.at[ir, f'risk_avg_{year}'] = risk_avg
            ws.at[ir, f'risk_area_{year}'] = risk_cnt*30*30/10000.0
            ws.at[ir, f'all_avg_{year}'] = all_mean


    res.append(ws)

res = geopandas.GeoDataFrame(pandas.concat(res))

#%% Slopes for risk_avg, risk_area and all_avg, without 2016
lm=LinearRegression()
years = np.arange(2017,2022).reshape(-1,1)
for ir,r in res.iterrows():

    for stat in ['risk_avg','risk_area','all_avg']:
        y = np.array([r[f'{stat}_{year}'] for year in years[:,0]])
        lm = lm.fit(years,y)
        res.at[ir,f'{stat}_slope'] = lm.coef_[0]

res.to_file(fld_input/'erosion_Ale_Sekela_wgs84.gpkg',driver='GPKG')
#%%

    

# %%
