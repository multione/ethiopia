#%%
from pathlib import Path
import pandas, geopandas


#%%

def update_names():
    '''
    Update imaena MWSova koje mi je Teddy poslao
    '''
    fn_original = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\ethiopia_MWS_wgs84.gpkg')
    fn_names = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\2022\20221101 MWS_fixed_names_and_chosen_ids.xlsx')
    fn_out = Path(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\ethiopia_MWS_20221103_wgs84.gpkg')

    dfo = geopandas.read_file(fn_original)
    dfn = pandas.read_excel(fn_names, sheet_name='data')

    for c in ['cws_nm','mws_nm','R_NAME','Z_NAME','W_NAME']:
        ind1 = dfo[c].isna()
        ind2 = dfn[c].isna()
        ind = ind1 & (~ind2)
        print(c, ind.sum())
        dfo.loc[ind,c] = dfn.loc[ind,c]

    dfid = pandas.read_excel(fn_names, sheet_name='chosen_ids')
    dfo['chosen']=0
    dfo.loc[dfo.OBJECTID.isin(dfid.OBJECT_ID),'chosen']=1
    
    dfo.to_file(fn_out,driver='GPKG')


def spoji_iz_gee():

    df1 = geopandas.read_file(r'D:\DATA\ETHIOPIA2022\EROSION\chosen-2016.geojson')
    df1['year'] = 2016
    df1 = df1[['OBJECTID','year','mean']]
    df1.columns=['oid','year','soil_erosion_hazard' ]

    df2 = geopandas.read_file(r'D:\DATA\ETHIOPIA2022\EROSION\chosen-2017.geojson')
    df2['year'] = 2017
    df2 = df2[['OBJECTID','year','mean']]
    df2.columns = ['oid','year','soil_erosion_hazard']

    df3 = geopandas.read_file(r'D:\DATA\ETHIOPIA2022\EROSION\erosion_chosen_2018.geojson')
    df3 = df3[['oid','year','soil_erosion_hazard']]
    df3['oid'] = df3.oid.apply(int)

    df4 = geopandas.read_file(r'D:\DATA\ETHIOPIA2022\EROSION\erosion_chosen_192021.geojson')
    df4 = df4[['oid','year','soil_erosion_hazard']]
    df4['oid'] = df4.oid.apply(int)

    df = pandas.concat((df1,df2,df3,df4))

    dfp = pandas.pivot(df, index='oid',columns='year',values='soil_erosion_hazard')

    dfo = geopandas.read_file(r'C:\CLOUDS\MultiOne j.d.o.o\PROJEKTI - Documents\ETHIOPIA\GIS\ethiopia_MWS_20221103_wgs84.gpkg')

    df = dfo.join(dfp, on='OBJECTID')

    df.to_file(r'D:\DATA\ETHIOPIA2022\EROSION\erosion_chosen_all.gpkg', driver='GPKG')
    df.to_excel(r'D:\DATA\ETHIOPIA2022\EROSION\erosion_chosen_all.xlsx')