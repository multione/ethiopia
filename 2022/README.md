# Overview of repository (folder "2022")
 - biomass  
    Scripts for calculating biomass from modis EVI indices at 250 meter resolution  
    1. global_biomass.py  
      Importing global ABC data from netCDF file to global_agb.gpkg   
    2. timesat_util.py  
      Readers and writers for TIMESAT input and output files. Used in next script.  
    3. timesat.py  
      Smoothing of modis values through TIMESAT application.  
    4. join_modis_agb.py  
      Joining of smoothed MODIS data and AGB data
    5. model_fit.py  
      Fitting model
    6. savgol.py   
      Attempt to avoid using TIMESAT using savitzky-golay filtering, didn't used it.
 - sekela-ale  
    Scripts for downscaling biomass to 30m resolution
    1. S1_glad.py  
      Converting GLAD data to netCDF format. It's separate code for Ale and Sekela becouse Ale is on only one GLAD tile and Sekela is divided on two tiles.
    2. S2_timesat_modis  
      Smothing of MODIS EVI values with TIMESAT for Ale and Sekela watersheds. Interpolating that data to GLAD grid.
    3. S3_fusion.py  
      For every pixel of GLAD dataset we calculate linear regression (with weights) through all times with x as smoothed MODIS EVI and Y is raw GLAD EVI, predicted values are named fusion EVI.
    4. S4_biomass.py  
      Applying of fitted model from biomass/model_fit.py to fusion EVI
    5. timesat_util.py   
      Same file as in biomass, used to decode output of TIMESAT
    6. utils.py  
      Some utiils functions and SpecialLinReg class that represents linear regression with weights