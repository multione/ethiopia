# -*- coding: utf-8 -*- 
# Manipulacija sa .tts fileovima od TIMESAT paketa
#

import numpy as np
import pandas 

#def encode_cmplx(data,)
'''
def decode_cmplx(data,nptperyear):

    # dekodiranje kompleksnog načina 
    # data - 2d numpy array (nserija x nsnimaka)
    # first_year - prva godina
    # nptperyear - broj točaka u godini (23)
    
    nseries,npoints=data.shape
    nyears=(npoints/nptperyear-1)/2
    #out_years=nd.zeros((nseries, nyears*nptperyear))
    #out_points=nd.zeros((nseries, nyears*nptperyear))
    out_data=np.zeros((nseries, nyears*nptperyear))
    for y in range(nyears):
        out_data[:,y*nptperyear:(y+1)*nptperyear] = data[:,(2*y+1)*nptperyear:(2*y+2)*nptperyear]
    
    return out_data
    '''
## tts class
    
class tts:
    ''' 
    Za sada služi samo za čitanje .tts file-ova 
    '''
    nyears=0
    nptperyear=0
    rowstart=0
    rowstop=0
    colstart=0
    colstop=0
    nseries=0
    npoints=0
    data=[]
    rows=[]
    cols=[]
    
    def __init__(self, filename):
        with open(filename,'rb') as fid:
            d=np.fromfile(fid,dtype=np.int32,count=6)
            self.nyears,self.nptperyear,self.rowstart,self.rowstop,self.colstart,self.colstop=[int(i) for i in d]
            self.nseries=(self.rowstop-self.rowstart+1)*(self.colstop-self.colstart+1)            
            self.npoints=self.nyears*self.nptperyear
            self.data=np.zeros((self.nseries,self.npoints),dtype=np.float32)
            self.rows=np.zeros(self.nseries,dtype=np.int32)
            self.cols=np.zeros(self.nseries,dtype=np.int32)
            
            for i in xrange(self.nseries):
                self.rows[i],self.cols[i]=np.fromfile(fid,dtype=np.int32,count=2)
                self.data[i,:]=np.fromfile(fid,dtype=np.float32,count=self.npoints)
                
    
##
#t=tts(r'E:\DATA\KDS\TIMESAT\1_fit.tts')    
#t=tts(r'E:\DATA\KDS\MODIS\tts\1_fit.tts')
#t=tts(r'E:\DATA\KDS\MODIS\tts\1_fit.tts')
##
class tpa:
    '''
    Čitanje i dekodiranje .tpa file-ova (rezultati iz TIMESATa)
    '''
    data=None
    nseries=0
    nyears=0
    nptperyear=0
    rowstart=0
    rowstop=0
    colstart=0
    colstop=0
    
    def __init__(self, filename):                        
        with open(filename,'rb') as fid:
            d=np.fromfile(fid,dtype=np.int32,count=6)           
            self.nyears,self.nptperyear,self.rowstart,self.rowstop,self.colstart,self.colstop=[int(i) for i in d]
            self.nseries=(self.rowstop-self.rowstart+1)*(self.colstop-self.colstart+1)            
            #self.npoints=self.nyears*self.nptperyear
            self.data=np.zeros((self.nseries,self.nyears,11),dtype=np.float32)
            self.rows=np.zeros(self.nseries,dtype=np.int32)
            self.cols=np.zeros(self.nseries,dtype=np.int32)
            self.nseasons=np.zeros(self.nseries,dtype=np.int32)
                        
            for i in xrange(self.nseries):
                #print i
                rr=np.fromfile(fid,dtype=np.int32,count=3)
                #print rr
                if len(rr)==0:
                    break
                self.rows[i],self.cols[i],self.nseasons[i]=rr
                for j in xrange(self.nseasons[i]):
                    self.data[i,j,:]=np.fromfile(fid,dtype=np.float32,count=11)
                    
    def decode_data(self,first_year=2000, season_offset=1, season_stride=2):
        '''
        first_year - početna godina - prva sezone 
        season_offset - koliko ih prvo preskočimo
        season_stride - koliko ih dalje preskačemo - nije baš najspretnije rečeno ....
        za cmplx način: season_offset=1, season_stride=2
        za simple način: season_offset=1, season_stride=1
        output: pandas.DataFrame
        '''
        ns=(self.nyears-season_offset)/season_stride
        ndpt=365//self.nptperyear+1    # broj dana za svaku točku
        out_data=np.zeros((self.nseries*ns,14))
        k=0 # row u izlaznom fileu
        nwrong=0 # broj loših pixel/sezona
        ni,nj=self.data.shape[0:2]
        for i in xrange(ni): #ni serije
            #print 'POINT %d:'%i,
            for j in xrange(nj):    #godine
                dd=self.data[i,j,:]    
                s=dd[[0,1,4]].astype(int)/self.nptperyear+1 # sezona u brojanju kao u ulaznom fileu                            
                #print '\n    sezona %d: (%d,%d)'%(s,dd[0],dd[1]),
                #print (dd[0]>0), (s>season_offset), ((s-season_offset-1)%season_stride==0)
                if (dd[0]>0) and (s[0]>season_offset) and ((s[0]-season_offset-1)%season_stride==0):                   
                    if s.min()==s.max(): 
                        valid=1
                    else:
                        valid=0
                        nwrong += 1
                    s=(s[0]-season_offset)//season_stride   # sezona u brojanju kao u izlaznom fileu
                    y=first_year+s  # godina                                 
                    #print ' (%d,%d)'%(s,y),
                    dd[[0,1,4]] = (dd[[0,1,4]] % self.nptperyear) * ndpt - 7 # U Julian day = - 8 + 1
                    dd[2] *= ndpt
                    npix=self.rows[i]   # Ovdje pretpostavljam da nemamo colone !!!!
                    dd=np.hstack((npix,y,valid,dd))
                    out_data[k,:]=dd
                    k += 1
                    
        out_data=out_data[0:k,:]            
        params=['SOS','EOS','LOS','BLV','MOS','LEF','SAM','ROI','ROD','LSI','SSI']
        out=pandas.DataFrame(data=out_data,columns=['NPOINT','YEAR','VALID']+params)
        #out=out.convert_objects()
        out.NPOINT=out.NPOINT.astype(int)
        out.YEAR=out.YEAR.astype(int)
        out.VALID=out.VALID.astype(int)
        print "Ukupno pixel/sezona: %d, loših: %d, postotak loših: %3.2f"%(k,nwrong,100.0*nwrong/k)
        return out
        
        """
            'SOS' ... Start of season (Julian day)
            'EOS' ... End of season (Julian day)
            'LOS' ... Length of season (days)
            'BLV'  ... base level; given as the average of the left and right minimum values.
            'MOS' ... time for the mid of the season; computed as the mean value of the times for which,
                  ... respectively, the left edge has increased to the 80 % level and the right edge has decreased
                  ... to the 80 % level.
            'LFF' ... largest data value for the fitted function during the season; may occur at a
                  ... diferent time compared with 5
            'SAM' ... seasonal amplitude; diference between the maximum value and the base level.
            'ROI' ... rate of increase at the beginning of the season; calculated as the ratio of the
                  ... diference between the left 20 % and 80 % levels and the corresponding time diference.
            'ROD' ... rate of decrease at the end of the season; calculated as the absolute value of the
                  ... ratio of the diference between the right 20 % and 80 % levels and the corresponding time
                  ... di?erence. The rate of decrease is thus given as a positive quantity.
            'LSI' ... large seasonal integral; integral of the function describing the season from the season
                  ... start to the season end.
            'SSI' ... small seasonal integral; integral of the di?erence between the function describing the
                  ... season and the base level from season start to season end.
        """
                
##
"""
t=tpa(r'E:\DATA\KDS\TIMESAT\sve\122_TS.tpa')
d=t.decode_data()
d.to_csv(r'E:\DATA\KDS\TIMESAT\sve\122_TS.csv', index=False, sep='\t')
d.save(r'E:\DATA\KDS\TIMESAT\sve\122_TS.df')
"""
##
class ts_input_ascii(object):
    '''
    Učitavanje ulaznog filea za TIMESAT (ascii)
    '''
    nyears=0
    nptperyear=0
    nseries=0
    npoints=0
    data=[]
    
    def __init__(self, filename):
        with open(filename,'r') as fid:
            l=fid.readline()
            d=np.fromstring(l,dtype=int,count=3,sep=' ')
            self.nyears,self.nptperyear,self.nseries=[int(dd) for dd in d]
            self.npoints=self.nyears*self.nptperyear
            self.data=np.zeros((self.nseries,self.npoints),dtype=np.float32)
            
            for i in xrange(self.nseries):
                    l=fid.readline()
                    self.data[i,:]=np.fromstring(l,dtype=np.float32,count=self.npoints,sep=' ')
                    
#%%
class timesat_input(object):
    '''
    Klasa za izradu ulaznog filea za TIMESAT
    '''
    nptperyear = 23 # broj snimaka u godini
    skip_days = 16  # broj dana razmaka između dva snimka u godini
    # datumi koje zamijenjujem drugim datumima
    dates_replace = [('2000-01-01','2001-01-01'),
                     ('2000-01-17','2001-01-17'),
                     ('2000-02-02','2001-02-02')]
    #@classmethod
    #def from_txt(cls,filename):
    def __init__(self):
        pass
    
    def load_from_txt(self,filename,cols=None):
        '''        
        Učitavanje tekst filea sa modis podacima. 
        cols -- lista imena kolona u file-e, default=
            ['spatial_id','time_id','ndvi','evi','viq','c1','c2','c3','c4']
        '''
        if cols==None:
            cols=['spatial_id','time_id','ndvi','evi','viq','c1','c2','c3','c4']
        df=pandas.read_table(filename,names=cols)
        self.load_from_df(df)
        #self.unique_ids = pandas.Series(np.sort(df['spatial_id'].unique()))
        #self.data = df
        
    def load_from_df(self,df):
        '''
        Učitavanje modis podataka iz DataFrame-a
        '''
        # MODIS VIQ pretvorimo u weight-ove od 0 do 15 (0 - nema podatka, 15 - najbolji podatak)
        df['weight'] = 15-np.right_shift(np.bitwise_and(df.viq.values,0b111100),2);
        self.time_index = pandas.DatetimeIndex(df.time_id)        
        self.data = df
        
    def get_dates_complex(self, end_year, start_year=2000):
        '''
        Vraća datume redom za kompleksni Zrinkin način
        '''        
        days = np.arange(1,366,self.skip_days) - 1
        ndays = len(days)
        hdays = ndays//2 + 1 

        NOY = 0
        god=np.array([],dtype=int); dan=np.array([],dtype=int);
        for g in range(start_year,end_year+1):
            if g==start_year:
                gg=np.tile(g,ndays*2) #repmat(g,1,23*2);
                dd=np.r_[days[::-1],days] #[dani(23:-1:1) dani(1:23)];        
            else:
                gg=np.r_[np.tile(g-1,hdays), np.tile(g,2*ndays-hdays)] #repmat(g-1,1,12) repmat(g,1,11+23)];
                dd=np.r_[days[:hdays-2:-1], days[hdays-2::-1], days]#[dani(23:-1:12) dani(11:-1:1) dani];        

            god=np.r_[god,gg]
            dan=np.r_[dan,dd]
            NOY=NOY+2;
        god=np.r_[god, np.tile(end_year,ndays)]#repmat(2012,1,23)];
        dan=np.r_[dan, days[::-1]]
        NOY=NOY+1;
        
        fyears=np.array([np.datetime64('{}-01-01'.format(y)) for y in god])
        dates = fyears + dan

        for d1,d2 in self.dates_replace:
            dates[dates==np.datetime64(d1)]=np.datetime64(d2)
        return dates, NOY
    
    def get_dates_simple(self, end_year, start_year=2000):
        '''
        Vraća datume redom - jedonstavnim načinom (samo se prva i zadnja godina dupliciraju)
        '''
        years = np.concatenate(([start_year],np.arange(start_year,end_year),[end_year]))
        days = np.arange(1,366,self.skip_days) - 1
        fyears=np.array([np.datetime64('{}-01-01'.format(y)) for y in years])
        dates = fyears.reshape((-1,1)) + days.reshape((1,-1))
        dates = dates.flatten()
        for d1,d2 in self.dates_replace:
            dates[dates==np.datetime64(d1)]=np.datetime64(d2)
        return dates, len(years)
        
    def write_timesat(self, filename, bands=['ndvi','evi'], kind='complex', spatial_ids=None):
        '''
        Ispisuje ASCII ulazne file-ove za TIMESAT
        Usput snima i poredak spatial_id-eva 
        '''
        import os  
        from datetime import datetime

        if spatial_ids is None:
            df = self.data
            end_year = self.time_index.year.max()
        else: #ostavimo samo one spatial_id-eve koji su u spatial_id ...
            ind = self.data.spatial_id.isin(spatial_ids)            
            df = self.data[ind]
            end_year = self.time_index[ind].year.max()

        fld,fname = os.path.split(filename)
        fbase,fext = os.path.splitext(fname)
        
        bands.append('weight')
        if kind=='complex':
            dates, NOY = self.get_dates_complex(end_year)
        elif kind=='simple':
            dates, NOY = self.get_dates_simple(end_year) 
        sdates =[d.astype(datetime).strftime('%Y-%m-%d') for d in dates]
        
        for b in bands:
            if b=='weight':
                mv=0
                frm='%d'
            else:
                mv=-9999.0
                frm='%.4f'
                
            dfp = df.pivot('spatial_id','time_id',b)
            
            ff = os.path.join(fld,'{}_{}.txt'.format(fbase,b))
            fid = open(ff,'wt')
            fid.write('{} {} {}\n'.format(NOY,self.nptperyear,len(dfp)))
            fid.flush()
            
            dfp[sdates].to_csv(fid, sep=' ', header=False, na_rep=mv, float_format=frm, index=False)
                
            fid.close()
        
        # Odmah snimim i unique_ids
        ff = os.path.join(fld,'{}_{}.txt'.format('spatial_ids',fbase))
        dfp.index.values.tofile(ff,sep='\n',format='%d')

#%%                
   
def _test_timesat_input():
    #%%
    
    import jkUtils
    
    filename = 'E:\\WORK\\RAZNO\\UCKA\\modis\\v_data_ucka.txt'
    ti = timesat_input()
    ti.load_from_txt(filename)
    
    # Simple
    filename = 'E:\\WORK\\RAZNO\\UCKA\\modis\\test\\ts.txt'
    ti.write_timesat(filename,kind='simple')
    

    kp = jkUtils.VectorToDataFrame('E:\\WORK\\RAZNO\\UCKA\\modis\\klase_postoci.shp')
    for k in range(1,7):
        col='k%d'%k
        ind=kp[col]>=0.5
        spatial_ids=kp.spatial_id[ind]
        ti.write_timesat(r'E:\WORK\RAZNO\UCKA\modis\test\ts_%s'%col, 
                         kind='simple', spatial_ids= spatial_ids)

    # Complex
    fld=r'E:\WORK\RAZNO\UCKA\modis\test_complex'
    ti.write_timesat(os.path.join(fld,'ts'),kind='complex')
    
    kp = jkUtils.VectorToDataFrame('E:\\WORK\\RAZNO\\UCKA\\modis\\klase_postoci.shp')
    for k in range(1,7):
        col='k%d'%k
        ind=kp[col]>=0.5
        spatial_ids=kp.spatial_id[ind]
        ti.write_timesat(os.path.join(fld,'ts_%s'%col), kind='complex', spatial_ids = spatial_ids)
        
     # Complex, pixel > 90% klase
    fld=r'E:\WORK\RAZNO\UCKA\modis\test_complex_90'    
    
    kp = jkUtils.VectorToDataFrame('E:\\WORK\\RAZNO\\UCKA\\modis\\klase_postoci.shp')
    for k in range(1,7):
        col='k%d'%k
        ind=kp[col]>=0.9
        if ind.any():
            spatial_ids=kp.spatial_id[ind]
            ti.write_timesat(os.path.join(fld,'ts_%s'%col), kind='complex', spatial_ids = spatial_ids)

def _test_tts_1():
    in_txt=r'E:\WORK\VHS\MODIS\_plohe_EVI_TIMESAT.TXT'
    in_weight=r'E:\WORK\KDS\MONITORING\modis\cmplx_modis_kds_plohe_WEIGHT.TXT'
    in_tts_folder=r'E:\DATA\KDS\MODIS\tts'
    out_res_file=r'E:\DATA\KDS\MODIS\tts\res.txt'
    first_year=2000
    
    
    #fid=open(out_res_file,'wt')
    indata=ts_input_ascii(in_txt)
    inweight=ts_input_ascii(in_weight)
    
    data_source=decode_cmplx(indata.data,indata.nptperyear)
    data_weight=decode_cmplx(inweight.data,inweight.nptperyear)
    out_nseries,out_points = data_source.shape
    
    tts_files=[f for f in os.listdir(in_tts_folder) if f.endswith('.tts')]
    out_nres=len(tts_files)
    
    out_res=np.zeros((out_nseries*out_points, out_nres),dtype=np.float32)
                #dtype={'names':['res_%d'%i for i in range(1,out_nres+1)],'formats':[np.float32]*out_nres})
    out_data=np.zeros((out_nseries*out_points, 4),dtype=int)
                #dtype=[('point',int),('year',int),('pixel',int),('weight',int)])
    
    out_data[:,0]=np.tile(np.arange(out_points)/indata.nptperyear+first_year,out_nseries)
    out_data[:,1]=np.tile(np.arange(indata.nptperyear)+1,out_points/indata.nptperyear*out_nseries)
    out_data[:,2]=np.arange(out_points*out_nseries)/out_points+1
    out_data[:,3]=data_weight.flat
    
    for tts_file in tts_files:
        n=int(re.search('(\d+)_fit',tts_file).group(1))-1
        print n
        in_tts_file=os.path.join(in_tts_folder,tts_file)
        t=tts(in_tts_file)
        data_tts=decode_cmplx(t.data,t.nptperyear)
        out_res[:,n]=(data_source-data_tts).flat

    
    out=np.hstack((out_data,out_res))
    np.savetxt(out_res_file, out,fmt='%d %d %d %d'+' %.6f'*out_nres, delimiter='\t')
    
def _tpa_1():
    t=tpa(r'E:\DATA\KDS\TIMESAT\sve\122_TS.tpa')
    d=t.decode_data()
    d.to_csv(r'E:\DATA\KDS\TIMESAT\sve\122_TS.csv', index=False, sep='\t')
    d.save(r'E:\DATA\KDS\TIMESAT\sve\122_TS.df')

def _tpa_2():
    t=tpa(r'E:\WORK\VHS\MODIS\595_TS.tpa')
    d=t.decode_data()
    d.to_csv(r'E:\WORK\VHS\MODIS\595_TS.csv', index=False, sep='\t')
    d.save(r'E:\WORK\VHS\MODIS\595_TS.df')
    
def _test_tpa_test():
    #df=pandas.load((r'E:\DATA\KDS\TIMESAT\sve\122_TS.df')
    # PROVJERA ZADNJEG PIXELA !!!!
    traw=tts(r'E:\DATA\KDS\TIMESAT\sve\122_raw.tts')
    traw=decode_cmplx(traw.data,23)
    tfit=tts(r'E:\DATA\KDS\TIMESAT\sve\122_fit.tts')
    tfit=decode_cmplx(tfit.data,23)
    
    import matplotlib.pylab as plt
    
    plt.plot(traw[-1,:],'r')
    plt.plot(tfit[-1,:],'b')
    plt.show()
    
    
def _test_tpa_upload():
    #import pandas_dbms
    import jkUtils
    
    infile_tpa=r'E:\DATA\KDS\TIMESAT\sve\595_TS.df'
    infile=r'E:\DATA\KDS\modis\v_kds_data.txt'
    
    df=pandas.load(infile_tpa)
    # Uzmem samo VALID=1
    df=df.ix[df.VALID==1,:]
    del df['VALID']
    #Učitam samo prvu kolonu iz ulaznog file-a
    data=pandas.read_table(infile,header=None,usecols=[0])
    #pokupim jedinstvene spatial_id-eve
    sids=pandas.DataFrame(data[0].unique(),columns=['spatial_id'])
    sids.sort(columns='spatial_id',inplace=True)
    sids.reset_index(drop=True,inplace=True)
    del data
    # NPOINT broji od 1 pa dodam 1 indeksu
    sids.index=sids.index+1
    # sačuvam imena kolona
    df_cols=[n for n in df.columns]
    # spojim spatial_id-eve na podatke
    df=df.join(sids,on='NPOINT')
    # maknem kolonu NPOINT
    # del df['NPOINT']
    # preuredim kolone da spatial_id bude prvi
    df=df.ix[:,['spatial_id']+df_cols]
    jkUtils.PandasUploadToPG(df,'timesat_suma_595','localhost','kds','postgres','anscd11')#,if_exists='truncate')
    
if __name__=='__main__':
    #tts_1()
    #tpa_2()
    #tpa_upload()
    _test_timesat_input()
    